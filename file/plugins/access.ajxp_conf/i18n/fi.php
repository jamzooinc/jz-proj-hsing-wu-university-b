<?php
//---------------------------------------------------------------------------------------------------
//							
//	AjaXplorer
//	
//	Copyright 2007-2010 Charles du Jeu - LGPL
//  www.Ajaxplorer.info
//
//	Finnish translation by Aleksi Postari
//	aleksi (at) postari.net
//	Last update: 10.09.2010
// 
//---------------------------------------------------------------------------------------------------

$mess=array(
"1" => "Asetustiedot",
"2" => "Käyttäjät",
"3" => "Käyttäjien kansiot",
"4" => "Historia",
"5" => "Vianmääritys",
"6" => "Tunnus",
"7" => "Ylläpitäjä", 
"8" => "Kansion nimi",
"9" => "Kansion tyyppi",
"10" => "Metalähteet",
"11" => "Lisää lähde",
"12" => "Meta lisäosa",
"13" => "Oletko varma, että haluat poistaa tämän lähteen?",
"14" => "Kyllä",
"15" => "Ei",
"16" => "Tiedoston päiväys",
"17" => "Päiväys",
"18" => "IP", 
"19" => "Tyyppi",
"20" => "Käyttäjä", 
"21" => "Toiminto", 
"22" => "Parametrit",
"23" => "Testin nimi",
"24" => "Sisältö",
"25" => "Oikeudet kansioihin", 
"26" => "Muuta salasana",
"27" => "Ylläpitäjä",
"28" => "Tämä käyttäjä on ylläpitäjä?", 
"29" => "Luku", 
"30" => "Kirjoitus",
"32" => "Kansion tyyppi",
"33" => "Ladataan...",
"34" => "Oletko varma, että haluat poistaa tämän käyttäjän? Tätä toimea ei voida perua!",
"35" => "Oletko varma, että haluat poistaa tämän kansion? Tätä toimea ei voida perua!",
"36" => "Täytä kaikki vaaditut kentät!",
"37" => "Salasana ja varmistus-salasana eivät täsmää!",
"38" => "Täytä kirjautumisruutu!",
"39" => "Täytä kummatkin salasanakentät!", 
"40" => "Rastita ruutu hyväksyäksesi!",
"41" => "ajurin asetukset",
"42" => "Valitse ajuri!",
"43" => "Käyttäjä on jo olemassa, valitse toinen nimi!",
"44" => "Käyttäjä luotu onnistuneesti",
"45" => "Myönnetty pääylläpitäjän oikeudet käyttäjälle ",
"46" => "Oikeudet muutettu käyttäjälle ",
"47" => "Tiedot muutettu käyttäjälle ",
"48" => "Salasana muutettu käyttäjälle ",
"49" => "Salasanaa ei onnistuttu muuttamaan",
"50" => "Virhe: Tämän niminen kansio on jo olemassa",
"51" => "Kansio conf ei ole kirjoitettavissa",
"52" => "Käyttäjien kansio luotu onnistuneesti",
"53" => "Virhe muutettaessa käyttäjien kansion tietoja", 
"54" => "Käyttäjien kansion tietojen muuttaminen onnistui", 
"55" => "Varoitus, Tällä hetkellä voit luoda ainoastaan yhden meta lisäosan kerrallaan.", 
"56" => "Metalähde lisätty onnistuneesti", 
"57" => "Metalähde poistettu onnistuneesti",
"58" => "Metalähteen tiedot muutettu onnistuneesti",
"59" => "Käyttäjän kansio poistettu onnistuneesti",
"60" => "Käyttäjä poistettu onnistuneesti",
"61" => "Virheelliset syötteet!",
); 
?>
