<?php
//---------------------------------------------------------------------------------------------------
//							
//	AjaXplorer
//	
//	Copyright 2007-2010 Charles du Jeu - LGPL
//  www.Ajaxplorer.info
//
//	german translation: Axel Otterstätter <axel.otterstaetter@googlemail.com>
// 
//---------------------------------------------------------------------------------------------------

$mess=array(
"1" => "Konfigurationsdaten",
"2" => "Benutzer",
"3" => "Repositories",
"4" => "Logs",
"5" => "Diagnose",
"6" => "Benutzername",
"7" => "Ist Admin", 
"8" => "Repository Label",
"9" => "Zugriffstyp",
"10" => "Meta Quelle",
"11" => "Quelle hinzufügen",
"12" => "Meta Plugin",
"13" => "Wollen Sie die Quelle wirklich löschen?",
"14" => "Wahr",
"15" => "Falsch",
"16" => "Datei Datum",
"17" => "Datum",
"18" => "I.P.", 
"19" => "Level",
"20" => "Benutzer", 
"21" => "Aktion", 
"22" => "Param.",
"23" => "Test Name",
"24" => "Test Daten",
"25" => "Repositories Zugriffe", 
"26" => "Passwort ändern",
"27" => "Adminrechte",
"28" => "Benutzer hat Adminrechte?", 
"29" => "Lesen", 
"30" => "Schreiben",
"32" => "Repository-Treiber",
"33" => "Lade...",
"34" => "Soll der Benutzer wirklich gelöscht werden? Dies kann nicht rückgängig gemacht werden!",
"35" => "Soll das Repository wirklich gelöscht werden? Dies kann nicht rückgängig gemacht werden!",
"36" => "Erforderliche Felder fehlen!",
"37" => "Achtung, Passwort und die Wiederholung sind unterschiedlich!",
"38" => "Bitte das Benutzernamefeld ausfüllen!",
"39" => "Bitte beide Passwortfelder ausfüllen!", 
"40" => "Bitte zum Zustimmen einen Haken setzen!",
"41" => "Treiberoptionen",
"42" => "Bitte einen Treiber wählen!",
"43" => "Benutzer existiert bereits, bitte einen anderen Anmeldenamen wählen!",
"44" => "Benutzer erfolgreich angelegt",
"45" => "Adminrechte geändert von Benutzer ",
"46" => "Rechte geändert von Benutzer ",
"47" => "Daten gespeichert von Benutzer ",
"48" => "Passwort erfolgreich geändert für Benutzer ",
"49" => "Passwort kann nicht geändert werden",
"50" => "Fehler: Ein Repository mit gleichen Namen existiert bereits",
"51" => "Das conf Verzeichnis ist nicht beschreibbar",
"52" => "Repository erfolgreich erstellt",
"53" => "Fehler beim Ändern des Repository", 
"54" => "Repository erfolgreich geändert", 
"55" => "Achtung, zur Zeit kann nur eine Instanz von jedem Meta Plugin hinzugefügt werden.", 
"56" => "Meta Quelle erfolgreich hinzugefügt", 
"57" => "Meta Quelle erfolgreich gelöscht",
"58" => "Meta Quelle erfolgreich geändert",
"59" => "Repository erfolgreich gelöscht",
"60" => "Benutzer erfolgreich entfernt",
"61" => "Fehlerhafte Parameter!",
); 
?>
