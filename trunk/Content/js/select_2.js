function tri(parent, child, jsonUrl)
{
    $(parent).bind('change', function() {
        _e();
    });
    
    var _e = function()
    {
        $.ajax({
            url:jsonUrl + $(parent).val(),
            type:'GET',
            dataType:'json',
            success:function(json) {
                //[{"value":"","text":""},......]
                _render($(child), json);
            }
        });
    }
    
    var _render = function(target, json) {
        
        $(target).empty();
        for (var i = 0; i < json.length; i ++) {
            var _opt = $('<option />').val(json[i].value).text(json[i].text);
            //if (json[i].checked) _opt.attr('checked', 'checked');
            $(target).append(_opt);
        }
    }
}