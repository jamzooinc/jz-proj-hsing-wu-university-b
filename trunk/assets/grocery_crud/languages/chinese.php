<?php
/* Translated by @heenji */
	$lang['list_add'] 				= '新增';
	$lang['list_actions'] 			= '操作';
	$lang['list_page'] 				= '頁';
	$lang['list_paging_of'] 		= '/';
	$lang['list_displaying']		= '顯示 {start} 至 {end} /總計 {results} 紀錄';
	$lang['list_filtered_from']		= '(塞選於 {total_results} 總紀錄)';
	$lang['list_show_entries']		= '顯示 {paging} 頁紀錄';
	$lang['list_no_items']			= '沒有紀錄';
	$lang['list_zero_entries']		= '顯示 0 - 0 / 0 條紀錄';
	$lang['list_search'] 			= '查詢';
	$lang['list_search_all'] 		= '查詢所有';
	$lang['list_clear_filtering'] 	= '删除篩選條件';
	$lang['list_delete'] 			= '删除';
	$lang['list_edit'] 				= '編輯';
	$lang['list_paging_first'] 		= '首頁';
	$lang['list_paging_previous'] 	= '前一頁';
	$lang['list_paging_next'] 		= '後一頁';
	$lang['list_paging_last'] 		= '最後';
	$lang['list_loading'] 			= '載入...';

	$lang['form_edit'] 				= '編輯';
	$lang['form_back_to_list'] 		= '返回列表';
	$lang['form_update_changes'] 	= '修改';
	$lang['form_cancel'] 			= '取消';
	$lang['form_update_loading'] 	= '載入, 並更新修改...';
	$lang['update_success_message'] = '修改成功！';
	$lang['form_go_back_to_list'] 	= '返回列表';

	$lang['form_add'] 				= '新增';
	$lang['insert_success_message'] = '新增紀錄成功保存到資料庫';
	$lang['form_or']				= '或者';
	$lang['form_save'] 				= '儲存';
	$lang['form_insert_loading'] 	= '載入,並儲存...';

	$lang['form_upload_a_file'] 	= '上傳檔案';
	$lang['form_upload_delete'] 	= '刪除';
	$lang['form_button_clear'] 		= '清除';

	$lang['delete_success_message'] = '刪除資料成功.';
	$lang['delete_error_message'] 	= '找不到該筆資料.';

	/* Javascript messages */
	$lang['alert_add_form']			= '你新增的資料可能沒有儲存.\\n確定要返回嗎?';
	$lang['alert_edit_form']		= '你新增的資料可能沒有儲存.\\n確定要返回嗎??';
	$lang['alert_delete']			= '確定要刪除?';

	$lang['insert_error']			= '新增資料錯誤.';
	$lang['update_error']			= '更新資料錯誤.';

	/* Added in version 1.2.1 */
	$lang['set_relation_title']		= 'Select {field_display_as}';
	$lang['list_record']			= 'Record';
	$lang['form_inactive']			= 'inactive';
	$lang['form_active']			= 'active';

	/* Added in version 1.2.2 */
	$lang['form_save_and_go_back']	= '新增並且返回';
	$lang['form_update_and_go_back']= '更新並且返回';

	/* Upload functionality */
	$lang['string_delete_file'] 	= "Deleting file";
	$lang['string_progress'] 		= "Progress: ";
	$lang['error_on_uploading'] 	= "An error has occurred on uploading.";
	$lang['message_prompt_delete_file'] 	= "Are you sure that you want to delete this file?";

	$lang['error_max_number_of_files'] 	= "You can only upload one file each time.";
	$lang['error_accept_file_types'] 	= "You are not allow to upload this kind of extension.";
	$lang['error_max_file_size'] 		= "The uploaded file exceeds the {max_file_size} directive that was specified.";
	$lang['error_min_file_size'] 		= "You cannot upload an empty file.";

	/* Added in version 1.3.1 */
	$lang['list_export'] 	= "匯出";
	$lang['list_print'] 	= "列印";
	$lang['minimize_maximize'] = 'Minimize/Maximize';