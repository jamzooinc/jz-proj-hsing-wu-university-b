﻿<?php
include "head.php";
?>
<script >
$(document).ready(function(){
	$(document).keydown(function(e){
		if(e.keyCode==13)
		{
			$("form").submit();
		}
	});
})
</script>
<body>
<body >
    <div id="wrap">
        <div id="top">
            <div id="logo">
                <img src="<?= base_url(); ?>Content/Images/LOGO.gif" alt="" style="height: 50px;"></div>

        </div>

        <?php echo form_open(base_url() . 'manager/login'); ?>
        <div id="index">
            <div id="login">
                <table border="0" cellpadding="0" cellspacing="0" class="loginTable">
                    <tbody>
                        <tr>
                            <td colspan="2" class="title">
                                <img src="<?= base_url(); ?>Content/Images/login_t1.gif" alt="請輸入您的帳號與密碼"></td>
                        </tr>
                        <tr>
                            <td class="item">
                                <img src="<?= base_url(); ?>Content/Images/login_t1_1.gif" alt="帳號"></td>
                            <td>
                                <?php echo form_input('username', ''); ?>
                                <?php echo form_error('username'); ?>

                            </td>
                        </tr>
                        <tr>
                            <td class="item">
                                <img src="<?= base_url(); ?>Content/Images/login_t1_2.gif" alt="密碼"></td>
                            <td>
                                <?php echo form_password('password', ''); ?>
                                <?php echo form_error('password'); ?>
                            </td>
                        </tr>



                    </tbody>
                </table>
            </div>
        </div>

        <div id="loginBTN">
            <img src="<?= base_url(); ?>Content/Images/btn_login.gif" width="100" height="103" border="0" id="Image8">
        </div>
        <?php echo form_close(); ?>
    </div>
    <script>
        $("#loginBTN").click(function() {
            $("form").submit();
        });
    </script>
    <div id="footer">
        <div id="copyright">© 2010 Jamzoo Inc. 醬子科技股份有限公司 <a href="http://www.jamzoo.com.tw/" target="_blank">www.jamzoo.com.tw</a></div>
    </div>
</body>
</html>
