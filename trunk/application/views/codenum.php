<?php
        $RestCode = jampush_resCode;
        $AppKey = jampush_AppKey;
        $parameterArray["ctrl"] = "point_summary";
        $parameterArray["rest_code"] = "$RestCode";
        $parameterArray["appkey"] = $AppKey;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://api.jampush.com.tw/rest-api.php");
        curl_setopt($ch, CURLOPT_POST, true); // 啟用POST
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($parameterArray));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); //只處理不顯示
        $r = curl_exec($ch);
	$json = json_decode($r);
	//print_r($json);
        curl_close($ch);
?>
﻿<?php
include("head.php");
?>
<script>
  $(function() {
	$(".datepicker").datepicker();
    $(".datepicker").datepicker(
		"option", "dateFormat",'yy-mm-dd'
	);
  });
 </script>
<body>

<div id="wrap">
<?php
include("top.php");	
?>



  <div id="left">
<?php include("navigation.php");?>
  </div>

  <div id="main">
    <div class="secondaryMenu">
      <h1 class="float"></h1>
    </div>
    <div id="content">
		<h2>Jampush 剩餘點數查詢</h2>
		
		<div class="editArea">
			剩餘點數：<?php echo $json->points;?>
		</div>
		
    </div>
      <p>&nbsp;</p>
    </div>
  </div>
</form>
</div>
<div id="footer">
  <div id="copyright">© 2010 Jamzoo Inc. 醬子科技股份有限公司 <a href="http://www.jamzoo.com.tw/" target="_blank">www.jamzoo.com.tw</a></div> 
</div>

<script src="<?php echo base_url();?>Content/js/bootstrap-modal.js"></script>

</body></html>
