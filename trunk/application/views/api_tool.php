<!DOCTYPE html>
<html >
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>API TOOL</title>
    </head>
    <body>
        <link rel="stylesheet/less" type="text/css" href="<?= base_url('./less/style.less') ?>">
        <link href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.min.css" rel="stylesheet">
        <link href="<?= base_url('./css/api_tool.css') ?>" rel="stylesheet">
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
        <script src="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/js/bootstrap.min.js"></script>
        <script src="<?= base_url('./js/less-1.4.1.min.js') ?>" ></script>



        <script>
            //for ie8 hide log.
            function log($msg){
                //                console.log($msg);
            }
            
            
            $(function(){
                log('READY');
                $.post('http://hwu.ladesign.tw//api_tool/api',{},function(data){
                    log('LOAD API DATA FINISH');
                    //compile string to object.
                    json_obj = jQuery.parseJSON(data);
                    
                    
                    window.API_LENGTH = json_obj.content.length;  //the total number of API
                    window.API_ARRAY  = json_obj.content;
                    window.CURRENT_URL = "";  //url means the url of this API.
                
                    //fill left_side navigation.
                    for(i = 0; i < API_LENGTH; i++){
                        log(API_ARRAY[i].URL);
                        if(API_ARRAY[i].CLASS == null){
                           $('#api_link').append("<li id='"+i+"'>"+ "<a>"+API_ARRAY[i].TITLE+"</a>"+"</li>");
                        }else if(API_ARRAY[i].CLASS == "HEADER"){
                           $('#api_link').append("<li class='nav-header'>"+API_ARRAY[i].HEADER_NAME+"</li>");
                        }
                    }
                
                
                    $('#api_link li').click(function(){
                        var id=this.id;
                        $('#comment').text(API_ARRAY[id].COMMENT);
                        //$('#comment').text(API_ARRAY[id].COMMENT + "\t" + API_ARRAY[id].EXAMPLE);
                        CURRENT_URL = API_ARRAY[id].URL;
                        $('#link_url').text(CURRENT_URL);
                        
                        //array of parameters    
                        var get_arr = (API_ARRAY[id].GET).split('|');
                        var post_arr = (API_ARRAY[id].POST).split('|');
                    
                    
                        //the last row of table is the button
                        //so every time click clean the rows before the last row.
                        if($('#json_table tr').length > 1)
                            $("#json_table").find("tr:lt(-1)").remove();
                    
                        
                    
                        $.each(get_arr,function(key,val){
                            if(val != ''){
                                $('#btn_row').before("<tr><td>"+val+"</td><td><input name='"+val+"'/></td></tr>");
                            }
                        });
                        
                        $.each(post_arr,function(key,val){
                            if(val != ''){
                                $('#btn_row').before("<tr><td>"+val+"</td><td><input name='"+val+"'/></td></tr>");
                            }
                        });
                        
                        return false;
                    });
                    
                    
                    
                    //if submit then get the button val to determin get or post then get json.        
                    $('#json_form').submit(function(e,obj){
                        var val = $("input[type=submit][clicked=true]").val();;
                        $.ajax({
                            data: $(this).serialize(),
                            type: val,
                            url : CURRENT_URL,
                            success: function(data){
                                json = $.parseJSON(data);
                                $('#output').html(JSON.stringify(json,null,'\t'));
                            }
                        });
                        return false;
                    });
                    
                    
            
                    //Add clicked to the button for latter use.
                    $("#json_form input[type=submit]").click(function() {
                        $("input[type=submit]", $(this).parents("form")).removeAttr("clicked");
                        $(this).attr("clicked", "true");
                    });
                });
            });
            
           
            
            
            
        </script>





        <div class="row-fluid">
            <div id="header" class="span12">
                <h1 style="display: inline-block;margin:10px 10px">API TOOL</h1>
                <a style="float:right;margin:15px 10px 15px 5px" href="#manual" role="button" class="btn mike btn-small" data-toggle="modal">使用說明</a>
                <!--<a style="float:right;margin:15px 5px" href="#api_load" role="button" class="btn mike btn-small" data-toggle="modal">載入 API</a>-->
            </div> 
        </div>


        <div id="api_load"class="modal hide fade">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h3>API URL:</h3>
            </div>
            <div class="modal-body">
                <div class="input-append">
                    <!--<span class="add-on">http://</span>-->
                    <input class="span5" id="appendedInputButton" type="url">
                    <button class="btn" type="button">Go!</button>
                </div>
            </div>
        </div>



        <div id="manual"class="modal hide fade">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h3>使用說明</h3>
            </div>
            <div class="modal-body">
                <p>就用啊</p>
            </div>
        </div>



        <div class="container-fluid">

            <div class="span12"><br /></div>
            <div id="left_side" class="span3" style="height:350px;overflow-y: auto">
                <!--<ul id="api_link" class="unstyled">-->
                <ul id="api_link" class="nav nav-list">
                </ul>
            </div>

            <div id="main" class="span8" style="height:350px;overflow-y: auto">
                <p id="link_url"></p>
                <form id="json_form">
                    <table id="json_table">
                        <tr id="btn_row"><td>
                                <input id="post_btn" type="submit" class="btn mike" value="POST"/>
                                <input id="get_btn" type="submit" class="btn mike" value="GET"/>
                            </td></tr>
                    </table>
                </form>
            </div>
            <div class="span12"><br /></div>
            <div id="comment" class="span3" style="height:350px;overflow-y: auto">COMMENT:</div>
            <div id="output" class="span8" style="height:350px;overflow-y: auto">OUTPUT:</div>
        </div>
        <!--<footer>ccc</footer>-->
    </body>
</html>