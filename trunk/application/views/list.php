﻿<?php
include("head.php");
?>
<script>
  $(function() {
	$(".datepicker").datepicker();
    $(".datepicker").datepicker(
		"option", "dateFormat",'yy-mm-dd'
	);
  });
 </script>
<body>

<div id="wrap">
<?php
include("top.php");	
?>



  <div id="left">
<?php include("navigation.php");?>
  </div>

  <div id="main">
    <div class="secondaryMenu">
      <h1 class="float"></h1>
    </div>
    <div id="content">
		<h2><?=$title;?></h2>
		<?=$nav;?>
		
		<div class="editArea">
			<?php
			if(isset($msg)){ echo $msg ;}
			?>
			<?=$table;?>
		</div>
		<?php
		if(isset($js)){
			echo '<script>'.$js.'</script>';
		}
		?>
		
    </div>
      <p>&nbsp;</p>
    </div>
  </div>
</form>
</div>
<div id="footer">
  <div id="copyright">© 2010 Jamzoo Inc. 醬子科技股份有限公司 <a href="http://www.jamzoo.com.tw/" target="_blank">www.jamzoo.com.tw</a></div> 
</div>

<script src="<?php echo base_url();?>Content/js/bootstrap-modal.js"></script>

</body></html>