<?php
$manager_level = $this->session->userdata['manager_level'];
$ul = '';

switch ($manager_level) {
    case 'editor':
         $ul = '<li class="nav-header">教學單位</li>
		<li ><a href="' . base_url() . 'teach">教學單位清單</a></li>
	';
        /*$ul .= '<li class="nav-header">行政單位</li>
		<li ><a href="' . base_url() . 'admin">行政單位內容</a></li>';*/
        break;
    
    case 'super':
          $ul = '<li class="nav-header">帳號管理</li>
		<li ><a href="' . base_url() . 'manager/manager_list">帳號清單</a></li>
	';
        $ul .= '<li class="nav-header">醒吾Chat</li>
		<li ><a href="' . base_url() . 'chat/">Chat 清單</a></li>
		<li ><a href="' . base_url() . 'chat/send_push/">發送 Chat 給所有人</a></li>
		<li ><a href="' . base_url() . 'chat/assign/">指定對象群組發送</a></li>
		<li ><a href="' . base_url() . 'chat/single/">指定單一對象發送</a></li>
	';
        $ul .= '<li class="nav-header">廣告管理</li>
		<li ><a href="' . base_url() . 'ads/">廣告清單</a></li>
	';
        $ul .= '<li class="nav-header">軍訓室</li>
		<li ><a href="' . base_url() . 'location/">緊急聯絡電話撥打紀錄</a></li>
		<li ><a href="' . base_url() . 'house/">租屋資訊清單</a></li>
	';
        $ul .= '<li class="nav-header">英文組</li>
		<li ><a href="' . base_url() . 'exam/">測驗清單</a></li>
        <li ><a href="' . base_url() . 'exam/en_teach">英文資源</a></li>
		
    ';
        $ul .= '<li class="nav-header">行政單位</li>
		<li ><a href="' . base_url() . 'admin">行政單位內容</a></li>
	';
        $ul .= '<li class="nav-header">交通資訊</li>
		<li ><a href="' . base_url() . 'traffic/">自行前往</a></li>
		';
        $ul .= '<li class="nav-header">教學單位</li>
		<li ><a href="' . base_url() . 'teach">教學單位清單</a></li>
	';
        $ul .= '<li class="nav-header">智慧型公佈欄</li>
		<li ><a href="' . base_url() . 'smart/exp_log">發送紀錄匯出</a></li>
		<li ><a href="' . base_url() . 'smart/">訊息清單</a></li>
		
	';
        $ul .= '<li class="nav-header">行事曆</li>
		<li ><a href="' . base_url() . 'calendar/">年度清單</a></li>
		<li ><a href="' . base_url() . 'calendar/billboard">公告訊息</a></li>
	';

        $ul .= '<li class="nav-header">學生資料匯入</li>
		<li ><a href="' . base_url() . 'student/import/">學生資料匯入</a></li>
	';
        $ul .= '<li class="nav-header">點數查詢</li>
		<li ><a href="' . base_url() . 'code_query/">剩餘點數查詢</a></li>
	';
        break;
    case 'h079':
    case 'h045':
         $ul = '<li class="nav-header">學生資料匯入</li>
		<li ><a href="' . base_url() . 'student/import/">學生資料匯入</a></li>
	';
        $ul .= '<li class="nav-header">醒吾Chat</li>
		<li ><a href="' . base_url() . 'chat/">Chat 清單</a></li>
		<li ><a href="' . base_url() . 'chat/send_push/">發送 Chat 給所有人</a></li>
		<li ><a href="' . base_url() . 'chat/single/">指定單一對象發送</a></li>
                    ';
        $ul .= '<li class="nav-header">智慧型公佈欄</li>
		<li ><a href="' . base_url() . 'smart/exp_log">發送紀錄匯出</a></li>
		<li ><a href="' . base_url() . 'smart/">訊息清單</a></li>
	';
        break;
    case 'chat':
        $ul = '<li class="nav-header">帳號管理</li>
		<li ><a href="' . base_url() . 'manager/free_push_log/?manager_id='.$this->session->userdata('manager_id').'">發送紀錄</a></li>
	';
        $ul .= '<li class="nav-header">醒吾Chat</li>
		<li ><a href="' . base_url() . 'chat/">Chat 清單</a></li>
		<li ><a href="' . base_url() . 'chat/send_push/">發送 Chat</a></li>
		<li ><a href="' . base_url() . 'chat/assign/">指定對象群組發送</a></li>
		<li ><a href="' . base_url() . 'chat/single/">指定單一對象發送</a></li>
	';
        break;
    case 'fet':
        $ul .= '<li class="nav-header">行動優惠</li>
		<li ><a href="' . base_url() . 'coupon/">行動優惠清單</a></li>
		<li ><a href="' . base_url() . 'coupon/add">建立新訊息</a></li>
	';
        $ul .= '<li class="nav-header">點數查詢</li>
		<li ><a href="' . base_url() . 'code_query/">剩餘點數查詢</a></li>
	';
        break;
    case 'h007': // 軍訓室
        $ul .= '<li class="nav-header">軍訓室</li>
		<li ><a href="' . base_url() . 'location/">緊急聯絡電話撥打紀錄</a></li>
		<li ><a href="' . base_url() . 'house/">租屋資訊清單</a></li>
	';
        $ul .= '<li class="nav-header">智慧型公佈欄</li>
		<li ><a href="' . base_url() . 'smart/exp_log">發送紀錄匯出</a></li>
		<li ><a href="' . base_url() . 'smart/">訊息清單</a></li>
	';
        break;
    case 'recruit': //招生相關
        $ul = '<li class="nav-header">醒吾Chat</li>
		<li ><a href="' . base_url() . 'chat/">Chat 清單</a></li>
		<li ><a href="' . base_url() . 'chat/send_push/">發送 Chat</a></li>
		<li ><a href="' . base_url() . 'chat/assign/">指定對象群組發送</a></li>
		<li ><a href="' . base_url() . 'chat/single/">指定單一對象發送</a></li>
	';
        $ul .= '<li class="nav-header">招生訊息</li>
		<li ><a href="' . base_url() . 'recruit/">招生訊息清單</a></li>
		<li ><a href="' . base_url() . 'recruit/add">建立新訊息</a></li>
	';
         $ul .= '<li class="nav-header">智慧型公佈欄</li>
		<li ><a href="' . base_url() . 'smart/exp_log">發送紀錄匯出</a></li>
		<li ><a href="' . base_url() . 'smart/">訊息清單</a></li>
		
	';
        break;
    case 'h073':
        $ul .= '<li class="nav-header">招生訊息</li>
		<li ><a href="' . base_url() . 'recruit/">招生訊息清單</a></li>
		<li ><a href="' . base_url() . 'recruit/add">建立新訊息</a></li>
	';
        break;
    case 'h036':
           $ul = '<li class="nav-header">英文組</li>
		<li ><a href="' . base_url() . 'exam/">測驗清單</a></li>
		<li ><a href="' . base_url() . 'exam/en_teach">英文資源</a></li>
        ';
        break;
    case 'smart':
         $ul .= '<li class="nav-header">智慧型公佈欄</li>
		<li ><a href="' . base_url() . 'smart/exp_log">發送紀錄匯出</a></li>
		<li ><a href="' . base_url() . 'smart/">訊息清單</a></li>
		
	';
        break;
}
?>
<div id="navigation">
    <ul class="nav nav-list">
        <?= $ul; ?>
    </ul>
</div>
