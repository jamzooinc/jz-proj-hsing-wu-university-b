<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Api_exam extends CI_Controller{
	function __construct()
	{
		parent::__construct();
		$this->load->model('mod_exam');
		//$this->output->enable_profiler(TRUE);
	}
	
//取得現在題庫編號
	function get_now_exam(){
		$json_arr = array();
		$type = $this->input->get('type');
		$json_arr['sys_code'] = '200';
		$json_arr['sys_msg'] = '處理成功';
		$q = array();
		$q = $this->mod_exam->get_now_exam($type);
		if($q == false){
		}else{
			$json_arr['q_id'] = $q['q_id'];
		}
		
		
		echo json_encode($json_arr);
	}
//取得題庫
	function get_exam(){
		$q_id = $this->input->get('q_id');
		if($q_id == ""){
			$json_arr['sys_code'] = '000';
			$json_arr['sys_msg'] = '參數不足';
		}elseif($this->mod_exam->chk_exam($q_id) == false){
			$json_arr['sys_code'] = '100';
			$json_arr['sys_msg'] = '查無試題';
		}else{
			$e = $this->mod_exam->get_exam($q_id);
			if($e == false){
				$json_arr['sys_code'] = '100';
				$json_arr['sys_msg'] = '查無試題';
			}else{
				$json_arr['sys_code'] = '200';
				$json_arr['sys_msg'] = '取得成功';
//$json_arr['use_qty'] =$this->mod_exam->get_qty($q_id);
$json_arr['use_qty'] = count($e);
				$json_arr['q_id'] = $q_id;
				$json_arr['exam'] = $e;
			}
			
		}
		echo json_encode($json_arr);
	}
//寫入最高分
	function top(){
		$member_id = $this->input->get('member_id');
		$q_id = $this->input->get('q_id');
		$score = $this->input->get('score'); 
		$type = $this->input->get('type');
		if($member_id == "" OR $q_id == "" OR $score == ""){
			$json_arr['sys_code'] = '000';
			$json_arr['sys_msg'] = '參數不足';
		}elseif($this->mod_exam->chk_exam($q_id) == false){
			$json_arr['sys_code'] = '100';
			$json_arr['sys_msg'] = '查無試題';
		}else{
			if($this->mod_exam->insert_top($member_id,$q_id,$score,$type) == true){
				$json_arr['sys_code'] = '200';
				$json_arr['sys_msg'] = '分數更新成功';
			}else{
				$json_arr['sys_code'] = '500';
				$json_arr['sys_msg'] = '分數更新失敗';
			}
		}
                $this->db->query("insert into `chat_log` (`to`,`from`,`sn`,`num`)values('".$member_id."','".$q_id."','".$type."-".$score."','".$json_arr['sys_msg']."')");

		echo json_encode($json_arr);
	}
//
//英語學習
	function en_teach(){
		$json_arr = $this->mod_exam->en_teach();
		echo json_encode($json_arr);
	}
  
  function remove_teach(){
      $sn = $this->input->get('key');
      if($this->mod_exam->remove_teach($sn)){
          $json_arr['sys_code'] = '200';
      }else{
          $json_arr['sys_code'] = '500';
      }
      echo json_encode($json_arr);
  }
}
?>
