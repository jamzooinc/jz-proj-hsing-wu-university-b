<?php
class Location extends CI_Controller{
	function __construct()
	{	
		  parent::__construct();
		  $this->load->library('myhtml');
		  $this->load->model('mod_location');
            $this->load->library('pagination');
		  // $this->output->enable_profiler(TRUE);
	}
	
	
//---清單-------
	public function index(){
		$manager_id = $this->session->userdata('manager_id');
		$table = '';
		$table_tr = '';
		$loc =  array();
		
		foreach($this->mod_location->get_list() as $loc){
			$table_tr .= '<tr>
									<td>'.$loc['phone_name'].'</td>
									<td>'.$loc['member_id'].'</td>
									<td>'.date("Y-m-d H:i:s",$loc['time']).'</td>
									<td>
										<a href="https://maps.google.com/maps?q='.$loc['location'].'" target="_blank"><li class=" icon-eye-open"></li>點我</a>
									</td>
									</tr>';
		}
		
		if(count($loc) < 1){
			$msg = $this->myhtml->msg('info','提示','查無資料');	
		
		}else{
			$msg = '';
			$table = '<table class="table table-striped">
							<tr><th>聯絡單位</th><th>會員帳號</th><th>時間</th><th>地圖</th></tr>
							'.$table_tr.'
						</table>';
		}			
    
    $config['base_url'] = base_url().'location/?';
    $config['total_rows'] = 20;
    $config['per_page'] = 0; 

$this->pagination->initialize($config); 

echo $this->pagination->create_links();
		
		$data = array(
			'title' => '緊急聯絡電話撥打紀錄',
			'nav' =>'',
			'table' => $table,
			'js' =>'',
			'msg'=>$msg
		);
		$this->load->view('list',$data);
	}

}
?>
