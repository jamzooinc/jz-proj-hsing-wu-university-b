<?php
class Api_webview extends CI_Controller{
	function __construct()
	{	
		  parent::__construct();
		  $this->load->library('myhtml');
		  $this->load->model('mod_admin');
	}
	//----行政單位
	function admin(){
			$info = $this->mod_admin->admin();
			$data = array(
				'info' => $info,
			);
			$this->load->view('webview',$data);
	}
    //----交通資訊 自行前往
   function traffic(){
       $this->load->model('mod_traffic');
       $info = $this->mod_traffic->get_info();
       $data = array(
				'info' => $info,
			);
			$this->load->view('webview',$data);
   }
}
?>
