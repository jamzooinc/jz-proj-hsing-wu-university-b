<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Api_talk extends CI_Controller{
	function __construct()
	{
		parent::__construct();
		$this->load->model('mod_member');
	}
	
//------ Use member_id search member_info Use to contacts
  function search_member(){
      $from = $this->input->get('from');
      $to = $this->input->get('to');
      if($to  == "" || $from == ""){
          $json_arr['sys_code'] = '000';
          $json_arr['sys_msg'] = '參數不齊全';
      }else{
          $f = $this->mod_member->search_member($from,$to);
          if($f == false){
                $json_arr['sys_code'] = '201';
                $json_arr['sys_msg'] = '查無資料內容';
          }else{
              $json_arr['sys_code'] = '200';
              $json_arr['sys_msg'] = 'Success';
              $json_arr['member'] = $f;
          }
      }
      echo json_encode($json_arr);
  }

  
//----- Get all contact list------------------------------------------------------
  function contacts_list(){
      $member_id  = $this->input->get('member_id');
      if($member_id == ""){
          $json_arr['sys_code'] = '000';
          $json_arr['sys_msg'] = '參數不齊全';
      }elseif($this->mod_member->chk_member_type($member_id) != 'student'){
          $json_arr['sys_code'] = '500';
          $json_arr['sys_msg'] = 'only for student use';
      }else{
          $classmate = $this->mod_member->classmate($member_id); 
          $friends  = $this->mod_member->friends($member_id);
          $to_be_confirm = $this->mod_member->to_be_confirm($member_id);
          $json_arr['sys_code'] = '200';
          $json_arr['sys_msg'] = 'success';
          $json_arr['classmate'] = $classmate;
          $json_arr['friends']  = $friends;
          $json_arr['to_be_confirm']  = $to_be_confirm;
      }
      echo json_encode($json_arr);
  }
  
//===== Friends =================================================
//----- 邀請加入會員 API-----------------------------------------------------
  function friend_add(){
      $from = $this->input->get('from');
      $to = $this->input->get('to');
      
      if($from == "" || $to == "" ){
          $json_arr['sys_code'] = '000';
          $json_arr['sys_msg'] = '參數不齊全';
      }elseif($from == $to){
          $json_arr['sys_code'] = '500';
          $json_arr['sys_msg'] = '不可以邀請自己為好友';
      }else{
          $this->load->model('mod_friends');
          $status = $this->mod_friends->friends_status($from,$to);
          switch ($status){
              case '0':
                  $this->mod_friends->add_friend($from,$to);
                  $json_arr['sys_code'] = '200';
                  $json_arr['sys_msg'] = '邀請已經送出';
              break;
              case '1':
                  $json_arr['sys_code'] = '200';
                  $json_arr['sys_msg'] = '已經是好友狀態';
              break;
              case '2b':
                  $this->mod_friends->add_friend($from,$to);
                  $json_arr['sys_code'] = '200';
                  $json_arr['sys_msg'] = '好友建立完成';
              break;
              case '2a':
                  $json_arr['sys_code'] = '200';
                  $json_arr['sys_msg'] = '等待對方確認中';
              break;
              case '3':
                  $json_arr['sys_code'] = '500';
                  $json_arr['sys_msg'] = '出乎合理範圍操作';
              break;
          }
          $json_arr['status'] = $status;
      }
      echo json_encode($json_arr);
  }
  
  //----------刪除好友----------------------------------------------------
    function friend_unlink(){
        $from = $this->input->get('from');
        $to = $this->input->get('to');
        if($from == "" OR $to == ""){
            $json_arr['sys_code'] = '000';
            $json_arr['sys_msg'] = '參數不齊全';
        }else{
            $this->load->model('mod_friends');
           //$status = $this->mod_friends->friends_status($from,$to);
//           switch($status){
//               case '2a':
//               case '1':
                   $this->mod_friends->unlink($from,$to);
                   $json_arr['sys_code'] = '200';
                   $json_arr['sys_msg'] = '好友刪除完成';
//               break;
//               default :
//                   $json_arr['sys_code'] = '500';
//                   $json_arr['sys_msg'] = '無法執行';
//                   $json_arr['status'] = $status;
//           }
        }
        echo json_encode($json_arr);
    }
  
//--------黑名單--------
    function block(){
        $from = $this->input->get('from');
        $to = $this->input->get('to');
        $switch = $this->input->get('switch');
        if($from == "" || $to == ""  || $switch == ""){
            $json_arr['sys_code'] = '000';
            $json_arr['sys_msg'] = '參數不齊全';
        }elseif($switch != 'on' AND $switch != 'off'){
            $json_arr['sys_code'] = '000';
            $json_arr['sys_msg'] = 'switch 僅接受on 或  off';
        }else{
            $this->load->model('mod_friends');
            $bolck = $this->mod_friends->block($from,$to,$switch);
            
                $json_arr["sys_code"] = '200';
                $json_arr['sys_msg'] = '處理成功';
                $json_arr['block'] = $bolck;
           
             //echo $this->db->last_query();
        }
        echo json_encode($json_arr);
    }
}
?>
