<?php

class Billboard extends CI_Controller {
    
    
    public function billboard_manage() {
        $this->load->model('mod_comment');
        $comment_arr = $this->mod_comment->get_comment('billboard');
        
        
        $this->load->library('grocery_CRUD');
        $crud = new grocery_CRUD();
        $crud->set_theme('datatables');
        $crud->set_table('billboard');
        $crud->set_subject('公佈欄');
        $crud->required_fields('billboard_content','billboard_start_time','billboard_end_time','type');
        $crud->columns('billboard_id', 'billboard_content', 'billboard_start_time', 'billboard_end_time','type')
                ->display_as('billboard_id',$comment_arr['billboard_id'])
                ->display_as('billboard_content',$comment_arr['billboard_content'])
                ->display_as('billboard_start_time',$comment_arr['billboard_start_time'])
                ->display_as('billboard_end_time',$comment_arr['billboard_end_time'])
                ->display_as('type',$comment_arr['type']);
        
        
        $crud->field_type('billboard_content','string');
        $crud->field_type('billboard_start_time','date');
        $crud->field_type('billboard_end_time','date');
        $crud->field_type('type','enum',array('全校','日間部','夜間部'));
        
        $output = $crud->render();
        $nice = $this->load->view('fet',$output,false);
//        $this->load->view('fet',array('content'=>$nice));
//        $this->load->view('fet_view',array('content'=>$nice));
        
    }
}