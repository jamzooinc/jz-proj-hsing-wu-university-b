﻿<?php

class Exam extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->library('myhtml');
        $this->load->model('mod_exam');
        // $this->output->enable_profiler(TRUE);
    }

    function index() {
        $table_tr = '';
        $table = '';
        $exam = $this->mod_exam->get_all_exam();
        if ($exam == false) {
            $msg = $this->myhtml->msg('info', '提示', '查無資料');
        } else {
            foreach ($exam as $e) {
                $table_tr .= '<tr>
										<td>' . $e['q_id'] . '</td>
										<td>' . date("Y-m-d H:i:s", $e['start']) . '</td>
										<td>' . $this->mod_exam->exam_type($e['type']) . '</td>
										<td>' . $e['use_qty'] . '</td>
										<td>
											<a href="' . base_url() . 'exam/remove_exam/?q_id=' . $e['q_id'] . '"><li class="icon-remove"></li>刪除 </a><a href="exam/examlist/?eid='.$e['q_id'].'"><li class="icon-list"></li>報表 </a>
										</td>
									</tr>';
            }
            $msg = '';
            $table = '<table class="table table-striped">
							<tr><th>試題編號</th><th>開始時間</th><th>分類</th><th>題目使用數量</th><th></th></tr>
							' . $table_tr . '
						</table>';
        }
        $nav = '<div class="alert alert-info"><h4>提示</h4>請按月新增英文題庫和開始時間，每次50題，系統將會自動讀取當月最新的題庫。</div>
            <ul class="nav nav-pills">
					<a href="' . base_url() . 'exam/edit_exam" class="btn btn-small"><li class="icon-plus"></li> 新增測驗</a>
				</ul>
                                ';
        $data = array(
            'title' => '測驗清單',
            'nav' => $nav,
            'table' => $table,
            'js' => '',
            'msg' => $msg
        );
        $this->load->view('list', $data);
    }
    function examlist(){
        $this->load->model("mod_score");
        $eid=$this->db->escape_str($this->input->get("eid"));
        $class=$this->db->escape_str($this->input->post("target"));
        $syscode=$this->db->escape_str($this->input->post("syscode"));
        $lv1=$this->db->escape_str($this->input->post("lv1"));
        $lv2=$this->db->escape_str($this->input->post("lv2"));
        $data=$this->mod_score->member_id_query($class,$syscode,$lv1,$lv2);
        $query=$this->mod_exam->exam_main($eid);
        $msg='';
        if($query['start']==""){
            echo $this->myhtml->alert_togo('查無資料', base_url() . 'exam/');
        }
        if($class!=""){
            $people_num=0;
            $score_num=0;
            $score["level0"]=0;
            $score["level1"]=0;
            $score["level2"]=0;
            $score["level3"]=0;
            $score["level4"]=0;
            $score["level5"]=0;
            $score["level6"]=0;
            $score["level7"]=0;
            $score["level8"]=0;
            $score["level9"]=0;
            $score_total=0;
            $score_avg=0;
            foreach ($data as $data_main){
                $people_score_data=$this->mod_score->score_query($data_main['member_id'],$eid);
                if($people_score_data!=FALSE){
                    $score_num=(($people_score_data['score']-1)/10);
                    $score_total+=$people_score_data['score'];
                    $vs=  explode(".", $score_num);
                    $score["level".abs($vs[0])]++;
                    $people_num++;
                }
            }
            $score_avg=@round(($score_total/$people_num),2);
            $form_html = '
                                    <fieldset >
                                        <legend>欲產生報表之題庫資訊</legend>
                                        <p>試題編號：'.$query['q_id'].'</p>
                                        <p>開始時間：'.date("Y-m-d H:i:s",$query['start']).'</p>
                                        <p>試題類型：'.$this->mod_exam->exam_type($query['type']).'</p>
                                        <p>題目使用數量：'.$query['use_qty'].'</p>
                                      </fieldset>
                                    <fieldset >
                                        <legend>查詢結果</legend>
                                        <p>測驗人數：'.$people_num.'人</p>
                                        <p>平均分數：'.$score_avg.'分</p>
                                        <legend>分數組距</legend>
                                    ';
                                    for($x=1;$x<=10;$x++){
                                        $form_html .='<p>'.(($x-1)*10).'分~'.($x*10).'分：'.$score['level'.($x-1)].'人</p>';
                                    }
                                   $form_html .= '</fieldset>'.$this->myhtml->form_button('匯出Excel');;
                                   
            $data = array(
                'title' => '顯示報表',
                'nav' => '',
                'msg' => $msg,
                'form_html' => $form_html,
                'enctype' => 'multipart/form-data',
                'form_action' => base_url() .'exam/xls_export/?eid='.$eid.'&target='.$class.'&syscode='.$syscode.'&lv1='.$lv1.'&lv2='.$lv2,
                'js' => ''
            );
            $this->load->view('form', $data);
        }else{
            $manager_id = $this->session->userdata('manager_id');
            $this->load->model('mod_chat');
            $chat_key = $this->mod_chat->manager_target($manager_id);
            $s1_html = '<label>選擇查詢對象</label><div id="target_div" style="dislpay:inline;"></div>';
            $s1_html .= '<label>目前欲查詢對象</label>
                ' . $this->myhtml->form_hidden('target', $chat_key) . $this->myhtml->form_hidden('syscode', "") . $this->myhtml->form_hidden('lv1', "") .$this->myhtml->form_hidden('lv2', "") . '
                    <ul class="breadcrumb" id="breadcrumb"   style="cursor: pointer;">
                  <li class="tar_span" chat_key="' . $chat_key . '" style="text-decoration:underline;margin-left:10px;">全部 <span >></span></li>
                </ul><div style="clear:both" />';
            $form_html = '
                                    <fieldset >
                                        <legend>欲產生報表之題庫資訊</legend>
                                        <p>試題編號：'.$query['q_id'].'</p>
                                        <p>開始時間：'.date("Y-m-d H:i:s",$query['start']).'</p>
                                        <p>試題類型：'.$this->mod_exam->exam_type($query['type']).'</p>
                                        <p>題目使用數量：'.$query['use_qty'].'</p>
                                      </fieldset>
                                    <fieldset >
                                        <legend>Step 1:挑選對象</legend>' . $s1_html . '
                                      </fieldset>
                                      ' . $this->myhtml->form_button('執行') . '
                                ';
            $js = '
		var tar = "' . $chat_key . '";
		var url = "' . base_url() . 'api_chat/js_target_select_tag/?key=" + tar;
        
		$("#target_div").load(url);
		$("#target_div span").live("click",function(){
				var tar = $(this).attr("chat_key");
                                var syscode = $(this).attr("syscode");
                                var lv1 = $(this).attr("lv1");
                                var lv2 = $(this).attr("lv2");
				var name = $(this).html();
				var url = "' . base_url() . 'api_chat/js_target_select_tag/?key=" + tar + "&syscode=" + syscode + "&lv1=" + lv1 + "&lv2=" + lv2;
				$("#target_div span").fadeOut();
				$("#target_div").load(url);
				var new_html = " <li class=\" tar_span\" chat_key=\""+tar+"\" syscode=\""+syscode+"\" lv1=\""+lv1+"\" lv2=\""+lv2+"\" style=\"text-decoration:underline;margin-left:10px;\">"+name+" <span >></span></li>";
				$("#breadcrumb").append(new_html).hide().fadeIn();
				$("#target").val(tar);
                                $("#syscode").val(syscode);
                                $("#lv1").val(lv1);
                                $("#lv2").val(lv2);
		});
		
		$(".tar_span").live("click",function(){
				$(this).nextAll().fadeOut();
				var tar =  $(this).attr("chat_key");
				var name = $(this).html();
                                var syscode = $(this).attr("syscode");
                                var lv1 = $(this).attr("lv1");
                                var lv2 = $(this).attr("lv2");
				var url = "' . base_url() . 'api_chat/js_target_select_tag/?key=" + tar + "&syscode=" + syscode + "&lv1=" + lv1 + "&lv2=" + lv2;
				$("#target_div").load(url);
				$("#target").val(tar);
                                $("#syscode").val(syscode);
                                $("#lv1").val(lv1);
                                $("#lv2").val(lv2);
		});

		';
            $data = array(
                'title' => '顯示報表',
                'nav' => '',
                'msg' => $msg,
                'form_html' => $form_html,
                'enctype' => 'multipart/form-data',
                'form_action' => base_url() . 'exam/examlist/?eid='.$eid,
                'js' => $js
            );
            $this->load->view('form', $data);
        }
    }
    function xls_export(){
        $this->load->model("mod_score");
        $eid=$this->db->escape_str($this->input->get("eid"));
        $class=$this->db->escape_str($this->input->get("target"));
        $syscode=$this->db->escape_str($this->input->get("syscode"));
        $lv1=$this->db->escape_str($this->input->get("lv1"));
        $lv2=$this->db->escape_str($this->input->get("lv2"));
        $data=$this->mod_score->member_id_query($class,$syscode,$lv1,$lv2);
        $query=$this->mod_exam->exam_main($eid);
        $member = array();
        $msg='';
        if($query['start']==""){
            echo $this->myhtml->alert_togo('查無資料', base_url() . 'exam/');
        }
        if($class!=""){
            $people_num=0;
            $score_num=0;
            $score["level0"]=0;
            $score["level1"]=0;
            $score["level2"]=0;
            $score["level3"]=0;
            $score["level4"]=0;
            $score["level5"]=0;
            $score["level6"]=0;
            $score["level7"]=0;
            $score["level8"]=0;
            $score["level9"]=0;
            $score_total=0;
            $score_avg=0;
            foreach ($data as $data_main){
                $people_score_data=$this->mod_score->score_query($data_main['member_id'],$eid);
                if($people_score_data!=FALSE){
                    $member[] = array(
                        'member_id' => $people_score_data['member_id'],
                        'score' => $people_score_data['score'],
                        'time' => $people_score_data['time'],
                    );
                    $score_num=(($people_score_data['score']-1)/10);
                    $score_total+=$people_score_data['score'];
                    $vs=  explode(".", $score_num);
                    $score["level".abs($vs[0])]++;
                    $people_num++;
                }
            }
            $score_avg=@round(($score_total/$people_num),2);
            $file="English_export_".date("YmdHis").".xls";
            header("Content-type: application/vnd.ms-excel"); //文件內容為excel格式
            header("Content-Disposition: attachment; filename=$file;"); //將PHP轉成下載的檔案指定名稱與副檔名.xls

            echo '<HTML xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40">'."\n";
            echo '<head><meta http-equiv="content-type" content="application/vnd.ms-excel; charset=UTF-8"></head>'."\n";
            echo '<body>';

                    echo '<table>
                    <tr>
                            <td colspan="4"><center>英文測驗報表</center></td>
                    </tr>
                    <tr>
                            <td colspan="4"></td>
                    </tr>
                    <tr>
                            <td>試題編號：'.$query['q_id'].'</td>
                            <td>開始時間：'.date("Y-m-d H:i:s",$query['start']).'</td>
                            <td>試題類型：'.$this->mod_exam->exam_type($query['type']).'</td>
                            <td>題目使用數量：'.$query['use_qty'].'</td>
                    </tr>
                    <tr>
                            <td colspan="2">測驗人數：'.$people_num.'人</td>
                            <td colspan="2">平均分數：'.$score_avg.'分</td>
                    </tr>';
                    echo '<tr>
                            <td colspan="4"><center>組距表</center></td>
                          </tr>';
                    for($x=1;$x<=10;$x++){
                        echo '<tr>';
                        echo '<td colspan="4">'.(($x-1)*10).'分~'.($x*10).'分：'.$score['level'.($x-1)].'人</td>';
                        echo '</tr>';
                    }
                    echo '<tr>
                            <td colspan="4"><center>分數清單</center></td>
                          </tr>';
                        echo '<tr>';
                        echo '<td>學號</td>';
                        echo '<td>分數</td>';
                        echo '<td colspan="2">測驗時間</td>';
                        echo '</tr>';
                    $sort = array();
                    foreach($member as $v){
                        $sort[]=$v['member_id'];
                    }
                    array_multisort($sort, SORT_ASC, $member);
                    foreach ($member as $people_score_data){
                            echo '<tr>';
                            echo '<td>'.$people_score_data['member_id'].'</td>';
                            echo '<td>'.$people_score_data['score'].'</td>';
                            echo '<td colspan="2">'.date("Y-m-d H:i:s",$people_score_data['time']).'</td>';
                            echo '</tr>';
                    }
            echo '</table>';

            echo '</body></html>';
        }
    }
//=========編輯============
    function edit_exam() {
        $q_id = $this->input->get_post('q_id');
        $title = '編輯題庫';
        $act = $this->input->get_post('act');
        if ($q_id == "") {

            $title = '新增題庫';
            $q_id = time();
        }
        //print_r($this->input->post());
        //表單
        if ($act == "") {

            $ex = $this->mod_exam->exam_main($q_id);
            $type_arr[1] = array('value' => 1, 'text' => '初級英文',);
            $type_arr[2] = array('value' => 2, 'text' => '中級英文',);
            //$use_qty_arr[] = array('value' => 10, 'text' => 10);
            //$use_qty_arr[] = array('value' => 15, 'text' => 15);
            $use_qty_arr[] = array('value' => 50, 'text' => 50);

            $form_html = $this->myhtml->form_hidden('act', 'do');
            $form_html .= $this->myhtml->form_hidden('q_id', $q_id);
            $form_html .= $this->myhtml->form_select('分類', 'type', $ex['type'], $type_arr);
            $form_html .= $this->myhtml->form_date('開始日期', 'start');
            $form_html .= $this->myhtml->form_select('題目顯示數量', 'use_qty', $ex['use_qty'], $use_qty_arr);
            $form_html .= $this->myhtml->form_text_input('上傳檔案', 'file', '', 'file');
            $form_html .= $this->myhtml->form_button('送出');
            $nav='<div class="alert alert-info"><h4>提示</h4>請按月新增英文題庫和開始時間，每次50題，系統將會自動讀取當月最新的題庫。</div>';
            $data = array(
                'title' => $title,
                'nav' => $nav,
                'js' => '',
                'form_html' => $form_html,
                'form_action' => '',
                'enctype' => 'multipart/form-data',
            );
            $this->load->view('form', $data);
        } else {
            //處理	

            $this->load->library('file_ctrl');
            $csv = $this->file_ctrl->import_csv('file');
            $type = $this->input->post('type');
            $start = strtotime($this->input->post('start'));
            $use_qty = $this->input->post('use_qty');
            $this->db->insert('exam_main', array('q_id' => $q_id, 'type' => $type, 'start' => $start, 'use_qty' => $use_qty));
            foreach ($csv as $ex) {
                $data = array(
                    'q_id' => $q_id,
                    'q_title' => $ex[1],
                    'a' => $ex[2],
                    'b' => $ex[3],
                    'c' => $ex[4],
                    'd' => $ex[5],
                    'correct' => $ex[6],
                );
                $this->db->insert('exam', $data);
            }
            redirect(base_url() . 'exam');
        }
    }

    function en_teach() {
        $this->load->library('lib_js');
        $table = '';
        $td_arr = $this->mod_exam->en_teach();
        $td_html = '';
        foreach ($td_arr as $td) {
            $td_html .= '<tr><td>
                                    <a href="' . $td['link'] . '" target="_blank">' . $td['title'] . '</a>
                                </td>
                                <td>
                                    ' . $this->lib_js->list_remove_icon($td['sn']) . '
                                </td></tr>
                                ';
        }
        $table = '<table class="table table-striped"><tr><th>標題</th><th></th></tr>' . $td_html . '</table>';
        $nav = '<ul class="nav nav-pills">
					<a href="' . base_url() . 'exam/add_teach" class="btn btn-small"><li class="icon-plus"></li>  新增英文資源</a>
				</ul>';
        $msg = '';
        $url = base_url() . 'api_exam/remove_teach';
        $js = $this->lib_js->list_remove_js($url);
        $data = array(
            'title' => '英文資源清單',
            'nav' => $nav,
            'table' => $table,
            'js' => $js,
            'msg' => $msg
        );
        $this->load->view('list', $data);
    }

    function add_teach() {
        $act = $this->input->get_post('act');
        $msg = '';
        if($act == 'add'){
            $title = $this->input->post('title');
            $link = $this->input->post('link');
            if($this->mod_exam->ins_teach($title,$link)){
                echo $this->myhtml->alert_togo('新增完成',base_url().'exam/en_teach');
            }else{
                $msg = $this->myhtml->msg('error','警告','新增失敗請重新檢查資料內容');
            }
        }
        $form_html  = '';
        $form_html .= $this->myhtml->form_hidden('act','add');
        $form_html .= $this->myhtml->form_text_input('標題','title','');
        $form_html .= $this->myhtml->form_text_input('網址','link','');
        $form_html .= $this->myhtml->form_button('送出');
        $data = array(
            'title' => '新增英文資源',
            'nav' => '',
            'js' => '',
            'msg'=>$msg,
            'form_html' => $form_html,
            'form_action' => '',
            'enctype' => 'multipart/form-data',
        );
        $this->load->view('form', $data);
    }

//===刪除===
    function remove_exam() {
        $q_id = $this->input->get('q_id');
        $this->db->where('q_id', $q_id)->delete('exam_main');
        $this->db->where('q_id', $q_id)->delete('exam');
        redirect(base_url() . 'exam');
    }

}

?>
