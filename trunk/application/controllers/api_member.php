<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Api_member extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('mod_member');
        $this->load->model('mod_jampush');
        $this->load->model('start_model');
    }

    
    
    function re_member(){
        $n1 = array();
        $this->db->where('sys_name','進修部學務組');
        $this->db->or_where('lv_1_name','進修部學務組');
        foreach($this->db->get('member_info')->result_array() as $n1){
            $this->db->where('member_id',$n1['member_id']);
            $this->db->delete('member_main');
            
            $this->db->where('member_id',$n1['member_id']);
            $this->db->delete('member_info');
        }
    }
   
    function member_login() {
        $member_id = $this->input->get('member_id');
        $member_pwd = $this->input->get('member_pwd');
        if ($member_id == "guest") {
            $member_pwd = 'guest';
        }
        if($member_id!="1001503002" or $member_id!="102144027"){
            $dxid = $this->input->get('dxid');
        }else{
            $dxid = md5(rand(10000000, 99999999));
        }

        if ($member_id == "") { //OR $dxid == ""
            $json_arr['sys_code'] = '000';
            $json_arr['sys_msg'] = '請輸入帳號與密碼！';
        } else {
            //如果格式不是email 則判定是校內人員
            if ($this->mod_member->chk_inside($member_id) AND $member_id != "guest") {
                $email = $member_id . '@mail.hwu.edu.tw';
                //校內人員直接去LDAP
                if ($this->mod_member->chk_ldap($member_id, $member_pwd)) {
                //如果LDAP成功則清掉之前DXID 建立新配對
                $this->mod_member->renew_dxid($member_id, $dxid, $email);


                $member = $this->start_model->get_a_row('member_main', 'member_id', $member_id);

                $json_arr['sys_code'] = '200';
                $json_arr['sys_msg'] = '登入成功';
                $json_arr['member_id'] = $member_id;
                $json_arr['last_login'] = $member->datetime;
                $json_arr['dxid'] = $member->dxid;
                $json_arr['nickname'] = ($member->nickname == null || $member->nickname == '') ? (string) $member->member_id : (string) $member->nickname;
                $json_arr['avatar'] = ($member->avatar == null || $member->avatar == '') ? (string) '' : (string) base_url('upload/avatar/' . $member->avatar);
                $json_arr['member_type'] = $this->mod_member->chk_member_type($member_id);
//                    $json_arr['avatar'] = base_url('/upload/avatar/').'/'.$member->avatar;
                } else {
                    $json_arr['sys_code'] = '202';
                    $json_arr['sys_msg'] = '帳密錯誤';
                    $json_arr['member_id'] = $member_id;
                }
            } else {
                if ($member_id == 'guest') {  //guest註冊
                    $member_id = 'guest@' . time();
                    $email = '';
                } elseif (strpos($member_id, 'uest@')) { //guest登入
                    $email = '';
                } else {//會員登入
                    $email = $member_id;
                }
                $this->mod_member->renew_dxid($member_id, $dxid, $email);
                $json_arr['sys_code'] = '200';
                $json_arr['sys_msg'] = '登入成功';
                $json_arr['member_id'] = $member_id;
            }
        }
        echo json_encode($json_arr);
    }

//---確認會員是否存在
    function is_member() {
        $member_id = $this->input->get('member_id');
        if ($member_id == "") {
            $json_arr['sys_code'] = '000';
        } elseif ($this->mod_member->is_member($member_id)) {
            $json_arr['sys_code'] = '200';
        } else {
            $json_arr['sys_code'] = '100';
        }
        echo json_encode($json_arr);
    }
    

    //ntpdate ntp.ubuntu.com     --linux update time command
    function update_resttime() {
//        echo 'ccc';
//        echo date("l jS \of F Y h:i:s A");
//          echo $ccc = strtotime('2012-11-10 10:10:10');
//        echo date('Y-m-d H:i:s');
//        echo '<br />';
//        echo $ccc = strtotime("20:00");
//        echo '<br />';
//        echo time();
//          echo $ccc = strtotime('2013-7-1 2:46:01');
//          echo date('H:i:s');
        //now 18
        //start time 22
        //end time 5
        //time between 

        $member_id = $this->input->get('member_id');
        $start_time = $this->input->get('start');
        $end_time = $this->input->get('end');

        if ($member_id == null || $start_time == null || $end_time == null) {
            $json_arr['sys_code'] = '301';
            $json_arr['sys_msg'] = '缺參數';
            echo json_encode($json_arr);
            die();
        }


        if ($this->mod_member->is_member($member_id)) {
            $this->db->update('member_main', array('start_time' => $start_time, 'end_time' => $end_time), array('member_id' => $member_id));
            $json_arr['sys_code'] = '200';
            $json_arr['sys_msg'] = 'OK';
            echo json_encode($json_arr);
            die();
        } else {
            $json_arr['sys_code'] = '201';
            $json_arr['sys_msg'] = '無此人';
            echo json_encode($json_arr);
            die();
        }
    }

//----填寫會員暱稱
    //input get member_id
    function update_nickname() {
        $member_id = $this->input->get('member_id');
        $nickname = $this->input->get('nickname');

        $json_arr = array();
        if ($member_id == null || $nickname == null) {
            $json_arr['sys_code'] = '301';
            $json_arr['sys_msg'] = '缺參數';
            echo json_encode($json_arr);
            die();
        }

        if ($this->mod_member->is_member($member_id)) {
            $this->start_model->modify_a_column('member_main', 'member_id', $member_id, 'nickname', $nickname);
            $json_arr['sys_code'] = '200';
            $json_arr['sys_msg'] = 'OK';
            echo json_encode($json_arr);
            die();
        } else {
            $json_arr['sys_code'] = '201';
            $json_arr['sys_msg'] = '無此人';
            echo json_encode($json_arr);
            die();
        }
    }

    //----輸入會員大頭照
    //input post
    function update_avatar() {
        $member_id = $this->input->post('member_id');
        $avatar = $this->input->post('avatar');


        $json_arr = array();
        if ($member_id == null || $avatar == null) {
            $json_arr['sys_code'] = '301';
            $json_arr['sys_msg'] = '缺參數';
            echo json_encode($json_arr);
            die();
        }

        if ($this->mod_member->is_member($member_id)) {

            $mtime = microtime(true);
            $mtime *= 10000;
            $file_name = $mtime . '.png';
            $this->start_model->modify_a_column('member_main', 'member_id', $member_id, 'avatar', $file_name);
            write_file("./upload/avatar/{$file_name}", base64_decode($avatar));
            $json_arr['avatar_url'] = base_url("/upload/avatar/{$file_name}");
            $json_arr['sys_code'] = '200';
            $json_arr['sys_msg'] = 'OK';
            echo json_encode($json_arr);
            die();
        } else {
            $json_arr['sys_code'] = '201';
            $json_arr['sys_msg'] = '無此人';
            echo json_encode($json_arr);
            die();
        }
    }

    function check_member() {
        $member_id = $this->input->get('member_id');
        $member = $this->start_model->get_a_row('member_main', 'member_id', $member_id);

        $json_arr = array();
        if ($member != null) {
            $json_arr['member_id'] = (string) $member->member_id;
            $json_arr['datetime'] = (string) $member->datetime;
            $json_arr['dxid'] = (string) $member->dxid;
            $json_arr['email'] = (string) $member->email;
            $json_arr['nickname'] = ($member->nickname == null || $member->nickname == '') ? (string) $member->member_id : (string) $member->nickname;
//            $json_arr['nickname'] = (string)$member->nickname;
//            $json_arr['avatar'] = (string)(base_url($member->avatar == null ||$member->avatar == '')?'':'upload/avatar/'.$member->avatar);
//            $json_arr['avatar'] = base_url('/upload/avatar/').'/'.$member->avatar;
            $json_arr['avatar'] = ($member->avatar == null || $member->avatar == '') ? (string) '' : (string) base_url('upload/avatar/' . $member->avatar);

            $json_arr['jampush'] = (string) $member->jampush;

            $json_arr['start_time'] = (string) $member->start_time;
            $json_arr['end_time'] = (string) $member->end_time;


            $json_arr['sys_code'] = '200';
            $json_arr['sys_msg'] = 'OK';
        } else {
            $json_arr['sys_code'] = '201';
            $json_arr['sys_msg'] = '無此人';
        }
        echo json_encode($json_arr);
    }

    function getBytesFromHexString($hexdata) {
        for ($count = 0; $count < strlen($hexdata); $count+=2)
            $bytes[] = chr(hexdec(substr($hexdata, $count, 2)));

        return implode($bytes);
    }

    function getImageMimeType($imagedata) {
        $imagemimetypes = array(
            "jpeg" => "FFD8",
            "png" => "89504E470D0A1A0A",
            "gif" => "474946",
            "bmp" => "424D",
            "tiff" => "4949",
            "tiff" => "4D4D"
        );

        foreach ($imagemimetypes as $mime => $hexbytes) {
            $bytes = getBytesFromHexString($hexbytes);
            if (substr($imagedata, 0, strlen($bytes)) == $bytes)
                return $mime;
        }

        return NULL;
    }
   
    //系統管理員專用===========================================

function clear_dxid(){
     $this->db->update('member_main',array('dxid'=>''));
    
}
   
 
}

?>
