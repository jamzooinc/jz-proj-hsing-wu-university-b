<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Student extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->library('myhtml');
        $this->load->model('mod_student');
    }

    function import() {
        $form_html = $this->myhtml->form_text_input('請選擇上傳檔案', 'file', '', 'file');
        $form_html .= $this->myhtml->form_button('submit');
        $data = array(
            'title' => '匯入學生資料',
            'nav' => '',
            'msg' => '',
            'form_html' => $form_html,
            'enctype' => 'multipart/form-data',
            'form_action' => base_url() . 'student/import_do/',
        );
        $this->load->view('form', $data);
    }

    function remove_std($member_id) {
        $this->db->where('member_id', $member_id)->delete('member_main');
        $this->db->where('member_id', $member_id)->delete('member_info');
    }

    function import_do() {
        ini_set("auto_detect_line_endings", "1");
        $class = $this->input->get_post('class');
        if(@$_FILES['file']){
            $type=$_FILES['file']['type'];
            $size=$_FILES['file']['size'];
            $list=0;
            setlocale(LC_ALL, 'en_US.utf-8');
            setlocale(LC_ALL, 'zh_TW.BIG5');
            if($type=="text/csv" or $type=="application/vnd.ms-excel"){
                $this->db->query("delete from `member_main`");
                $this->db->query("delete from `member_info`");
                $handle = fopen($_FILES['file']['tmp_name'], "rb"); //開啟上傳檔案
                while (($data = fgetcsv($handle, 0, ","))) {
                    if($list>=1){
                        if($data[0]!=""){
                            $data_last = array(
                                'member_id' => @iconv("big5", "utf-8//IGNORE", $data[0]),
                                'email' => @iconv("big5", "utf-8//IGNORE", $data[1]),
                                'class_code' => @iconv("big5", "utf-8//IGNORE", $data[2]),
                                'lv_2_code' => @iconv("big5", "utf-8//IGNORE", $data[3]),
                                'lv_1_code' => @iconv("big5", "utf-8//IGNORE", $data[4]),
                                'sys_code' => @iconv("big5", "utf-8//IGNORE", $data[5]),
                                'class_name' => @iconv("big5", "utf-8//IGNORE", $data[6]),
                                'lv_2_name' => @iconv("big5", "utf-8//IGNORE", $data[7]),
                                'lv_1_name' => @iconv("big5", "utf-8//IGNORE", $data[8]),
                                'sys_name' => @iconv("big5", "utf-8//IGNORE", $data[9]),
                                'name' => @iconv("big5", "utf-8//IGNORE", $data[10]),
                            );
                            $this->mod_student->insert($data_last);
                        }
                    }
                    $list++;
                }
                
                echo $this->myhtml->alert_togo('匯入完成', "../../student/import/");
            }else{
                echo $this->myhtml->alert_togo('匯入失敗，請選擇正確的csv檔案上傳', "../../student/import/");
            }
        }
        //先將該分類內容清空
        //if ($this->mod_student->clear($class)) {
        /*
        $import = $_FILES['file']['tmp_name'];
            //匯入
            $c_arr = $this->lib_excel->imp($import);
            
            print_r($c_arr);
            exit();
            foreach ($c_arr as $main) {
                if ($main['A'] != "") {
                    $data = array(
                        'member_id' => $main['A'],
                        'email' => $main['B'],
                        'class_code' => $main['C'],
                        'lv_2_code' => $main['D'],
                        'lv_1_code' => $main['E'],
                        'sys_code' => $main['F'],
                        'class_name' => $main['G'],
                        'lv_2_name' => $main['H'],
                        'lv_1_name' => $main['I'],
                        'sys_name' => $main['J'],
                        'name' => $main['K'],
                    );
                    //$this->mod_student->insert($data);
                }
            }*/
            //echo $this->myhtml->alert_togo('匯入完成', base_url());
        /*}else{
            echo 'error';
        }*/

    }

    function exp() {
        $class = $this->input->get('class');
        switch ($class) {
            case 'spr':
                $lv_1_name = '進修部學務組';
                break;
            case 'cha':
                $lv_1_name = '國際事務組';
                break;
            case 'all':
                $lv_1_name = 'all';
                break;
        }

        $th = array('member_id', 'email', 'class_code', 'lv_2_code', 'lv_1_code', 'sys_code', 'class_name', 'lv_2_name', 'lv_1_name', 'sys_name', 'name');
        $td = $this->mod_student->class_member($lv_1_name);
//        print_r($td);
//        echo $this->db->last_query();
      
        $this->lib_excel->exp($th, $td);
    }

}

?>