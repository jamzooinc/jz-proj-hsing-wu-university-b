<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Api_house extends CI_Controller{
	function __construct()
	{
		parent::__construct();
		$this->load->model('mod_house');
	}
	
	function index(){
		$where = array();
		if($this->input->get('distance') != ""){
			$distance = explode(',',$this->input->get('distance'));
			$where['distance >='] = $distance[0];
			$where['distance <='] = $distance[1];
		}
		if($this->input->get('type') != ""){
			$where['type'] = $this->input->get('type');
		}
		if($this->input->get('price') != ""){
			$price = explode(',',$this->input->get('price'));
			$where['price >='] = (int)$price[0];
			$where['price <='] = (int) $price[1];
		}
		if($this->input->get('orderby') == "distance"){
			$orderby = 'distance';
		}else{
			$orderby = 'price';
		}
		// $this->output->enable_profiler(TRUE);
		$json_arr = array();
		$c = $this->mod_house->search_count($where);
		if($c == 0){
			$json_arr['sys_code'] = '100';
			$json_arr['sys_msg'] = '無相關資料';
		}else{
			$json_arr['sys_code'] = '200';
			$json_arr['sys_msg'] = '資料蒐尋完成';
			$json_arr['count'] = $c;
			foreach($this->mod_house->search($where,$orderby) as $house){
			$json_arr['house'][] = $house;
			}
		}
		
		
		echo json_encode($json_arr);
	}
	
	
	function info(){
		$sn = $this->input->get('sn');
		$q = $this->mod_house->info($sn);
		if($q == false){
			$json_arr['sys_code'] = '100';
			$json_arr['sys_msg'] = '無相關資料';
		}else{
			$json_arr['sys_code'] = '200';
			$json_arr['sys_msg'] = '資料處理完成';
			$json_arr['house'] = $q;
		}
		echo json_encode($json_arr);
	}
//-----給後台用的
//-----刪除
	function remove(){
		$sn = $this->input->get('sn');
		$this->db->where('sn',$sn);
		if($this->db->delete('house')){
			$json_arr['sys_code'] = '200';
			$json_arr['sys_msg'] = '資料處理完成';
		}else{
			$json_arr['sys_code'] = '500';
			$json_arr['sys_msg'] = '資料處理失敗';
		}
		
		echo json_encode($json_arr);
	}
//------檢視
	function view(){
		$sn = $this->input->get('sn');
		$house = $this->mod_house->info($sn);
		
		$html = '
			<table>
				<tr><th>編號</th><td>'.$house['sn'].'</td></tr>
				<tr><th>房東姓名</th><td>'.$house['name'].'</td></tr>
				<tr><th>租金</th><td>'.$house['price'].'</td></tr>
				<tr><th>地址</th><td>'.$house['address'].'</td></tr>
				<tr><th>電話</th><td>'.$house['phone'].'</td></tr>
				<tr><th>屋齡</th><td>'.$house['year'].'</td></tr>
				<tr><th>房間數量</th><td>'.$house['qty'].'</td></tr>
				<tr><th>設備說明</th><td>'.$house['equipment'].'</td></tr>
				<tr><th>其他備註</th><td>'.$house['extra'].'</td></tr>
				<tr><th>距離</th><td>'.$house['distance'].'</td></tr>
				<tr><th>緯度</th><td>'.$house['lat'].'</td></tr>
				<tr><th>經度</th><td>'.$house['lon'].'</td></tr>
			</table>
		';
		echo $html;
	}

}
?>
