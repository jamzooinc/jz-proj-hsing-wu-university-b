<?php

class Calendar extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->library('myhtml');
        $this->load->model('mod_calendar');
        $this->load->library('lib_js');
    }

    function index() {
        $year_list = $this->mod_calendar->year_list();
        $td = '';
        if ($year_list != false) {
            foreach ($year_list as $y) {
                $td .= '<tr>
                            <td>' . $y['calendar_year'] . '</td>
                           <td>
                               <a href="' . base_url() . 'calendar/edit_pdf/?year=' . $y['calendar_year'] . '"><li class="icon-edit"></li> 編輯 pdf 檔案</a>
                                   ' . $this->lib_js->list_remove_icon($y['calendar_year']) . '
                           </td>
                       </tr>';
            }
        } else {
            $td = '<tr><td colspan="2">查無資料</td></tr>';
        }
        $table = '<table class="table"><tr><th>年度</th><th></th></tr>' . $td . '</table>';

        $nav = '<ul class="nav nav-pills">
					<a href="' . base_url() . 'calendar/add_year" class="btn btn-small"><li class="icon-plus"></li> 新增年度</a>
                        
				</ul>';

        $js = $this->lib_js->list_remove_js(base_url() . 'api_calendar/remove_year');
        $msg = $this->myhtml->msg('info', '提醒', '請先新增年度後再進行 pdf 檔案的上傳');
        $data = array(
            'title' => '年度清單',
            'nav' => $nav,
            'table' => $table,
            'js' => $js,
            'msg' => $msg
        );
        $this->load->view('list', $data);
    }

    function add_year() {
        $year = $this->input->get_post('year');
        $form_html = $this->myhtml->form_text_input('輸入學年（數字）', 'year', '');
        $form_html .= $this->myhtml->form_button('送出');
        if ($year != "") {
            if ($this->mod_calendar->add_year($year)) {
                echo $this->myhtml->alert_togo('新增完成', base_url() . 'calendar');
            } else {
                echo $this->myhtml->alert_togo('新增失敗', base_url() . 'calendar');
            }
        }
        $data = array(
            'title' => '新增年度',
            'nav' => '',
            'msg' => "",
            'form_html' => $form_html,
            'enctype' => 'multipart/form-data',
            'form_action' => '',
            'js' => ''
        );
        $this->load->view('form', $data);
    }

    function edit_pdf() {
        $year = $this->input->get_post('year');
        $file = $this->mod_calendar->year_file($year);
        $form_html = $this->myhtml->form_text_input('日間部上學期', 'semester_1_day', '', 'file');
        $form_html .= $this->myhtml->form_text_input('日間部下學期', 'semester_2_day', '', 'file');
        $form_html .= $this->myhtml->form_text_input('夜間部上學期', 'semester_1_evening', '', 'file');
        $form_html .= $this->myhtml->form_text_input('夜間部下學期', 'semester_2_evening', '', 'file');
        $form_html .= $this->myhtml->form_hidden('year', $year);
        $form_html .= $this->myhtml->form_hidden('act', 'update');
        $form_html .= $this->myhtml->form_button('送出');
        $act = $this->input->post('act');
        if ($act == 'update') {
            if ($_FILES['semester_1_day']['name'] != "") {
                $file_name = 'd1-' . time() . '.pdf';
                copy($_FILES['semester_1_day']['tmp_name'], '/var/www/html/trunk/upload/calendar/' . $file_name);
                $this->mod_calendar->set_pdf($year, 'semester_1_day', $file_name);
            }

            if ($_FILES['semester_2_day']['name'] != "") {
                $file_name = 'd2-' . time() . '.pdf';
                copy($_FILES['semester_2_day']['tmp_name'], '/var/www/html/trunk/upload/calendar/' . $file_name);
                $this->mod_calendar->set_pdf($year, 'semester_2_day', $file_name);
            }

            if ($_FILES['semester_1_evening']['name'] != "") {
                $file_name = 'e1-' . time() . '.pdf';
                copy($_FILES['semester_1_evening']['tmp_name'], '/var/www/html/trunk/upload/calendar/' . $file_name);
                $this->mod_calendar->set_pdf($year, 'semester_1_evening', $file_name);
            }

            if ($_FILES['semester_2_evening']['name'] != "") {
                $file_name = 'e2-' . time() . '.pdf';
                copy($_FILES['semester_2_evening']['tmp_name'], '/var/www/html/trunk/upload/calendar/' . $file_name);
                $this->mod_calendar->set_pdf($year, 'semester_2_evening', $file_name);
            }
        }




        $msg = $this->myhtml->msg('info', '提醒', '如果不須更新的檔案可以不用上傳就會直接略過不更新該檔');
        $data = array(
            'title' => $file['calendar_year'] . ' 行事曆 pdf 檔案',
            'nav' => '',
            'msg' => $msg,
            'form_html' => $form_html,
            'enctype' => 'multipart/form-data',
            'form_action' => '',
            'js' => ''
        );
        $this->load->view('form', $data);
    }

    //----公告
    function billboard() {
        $this->load->library('lib_js');
        $td = '';
        $billboard = $this->mod_calendar->get_billboard_list();

        if ($billboard == FALSE) {
            $td = '<tr><td colspan="5">查無資料</td></tr>';
        } else {

            foreach ($billboard as $bill) {
                $td .= '<tr>
                                <td>' . $bill['billboard_content'] . '</td>
                                <td>' . $bill['billboard_start_time'] . '</td>
                                <td>' . $bill['billboard_end_time'] . '</td>
                                <td>' . $bill['type'] . '</td>
                                <td>
                                    <a href="' . base_url() . 'calendar/edit_billboard/?id=' . $bill['billboard_id'] . '"><li class="icon-edit"></li> 編輯</a>
                                    ' . $this->lib_js->list_remove_icon($bill['billboard_id']) . '
                                </td>
                            </tr>';
            }
        }

        $table = '<table class="table"><tr><th>標題</th><th>開始</th><th>結束</th><th>部別</th><th></th></tr>' . $td . '</table>';
        $nav = '<ul class="nav nav-pills">
					<a href="' . base_url() . 'calendar/add_billboard" class="btn btn-small"><li class="icon-plus"></li> 新增公告</a>
				</ul>';
        $js = $this->lib_js->list_remove_js(base_url() . 'api_calendar/remove_billboard'); //刪除 js
        $data = array(
            'title' => '公告訊息',
            'nav' => $nav,
            'table' => $table,
            'js' => $js,
            'msg' => ''
        );
        $this->load->view('list', $data);
    }

    function add_billboard() {
        $act = $this->input->get_post('act');
        if ($act == 'add') {
            $start = $this->input->post('start');
            $end = $this->input->post('end');
            $subject = $this->input->post('subject');
            $type = $this->input->post('type');
            if ($start == "" || $end == "" || $subject == "" || $type == "") {
                echo $this->myhtml->alert_togo('所有欄位都是必填', base_url() . 'calendar/add_billboard');
            } else {
                $data = array(
                    'billboard_content' => $subject,
                    'billboard_start_time' => $start,
                    'billboard_end_time' => $end,
                    'type' => $type,
                );
                if ($this->mod_calendar->add_billboard($data)) {
                    echo $this->myhtml->alert_togo('新增成功', base_url() . 'calendar/billboard');
                } else {
                    echo $this->myhtml->alert_togo('新增失敗', base_url() . 'calendar/add_billboard');
                }
            }
        }

        $type_arr[] = array('text' => '日間部', 'value' => '日間部');
        $type_arr[] = array('text' => '夜間部', 'value' => '夜間部');
        $form_html = $this->myhtml->form_hidden('act', 'add');
        $form_html .= $this->myhtml->form_date('開始日期', 'start');
        $form_html .= $this->myhtml->form_date('結束日期', 'end');
        $form_html .= $this->myhtml->form_textarea('內容', 'subject', '');
        //$form_html .= $this->myhtml->form_text_input('內容', 'subject', '', 'text', 'input-xlarge');
        $form_html .= $this->myhtml->form_select('部別', 'type', '', $type_arr);
        $form_html .= $this->myhtml->form_button('送出');
        $data = array(
            'title' => ' 新增公告訊息',
            'nav' => '',
            'msg' => '',
            'form_html' => $form_html,
            'enctype' => '',
            'form_action' => '',
            'js' => ''
        );
        $this->load->view('form', $data);
    }

    function edit_billboard() {
        $id = $this->input->get_post('id');
        $act = $this->input->get_post('act');
      
        if ($act == 'edit') {
            $start = $this->input->post('start');
            $end = $this->input->post('end');
            $subject = $this->input->post('subject');
            $type = $this->input->post('type');
            
                if($subject != ""){$data['billboard_content'] = $subject;}
                if($start != ""){$data['billboard_start_time'] = $start;}
                if($end != ""){$data['billboard_end_time'] = $end;}
                if($type != ""){$data['type'] = $type;}
               
                if ($this->mod_calendar->edit_billboard($id,$data)) {
                    echo $this->myhtml->alert_togo('修改成功', base_url() . 'calendar/billboard');
                } else {
                    echo $this->myhtml->alert_togo('修改失敗', base_url() . 'calendar/edit_billboard');
                }
           
        }

        $info = $this->mod_calendar->get_once_billboard($id);
        
        $type_arr[] = array('text' => '日間部', 'value' => '日間部');
        $type_arr[] = array('text' => '夜間部', 'value' => '夜間部');
        $form_html = $this->myhtml->form_hidden('act', 'edit');
        $form_html .= $this->myhtml->form_hidden('id', $info['billboard_id']);
        $form_html .= $this->myhtml->form_date('開始日期','start' ,$info['billboard_start_time']);
        $form_html .= $this->myhtml->form_date('結束日期','end' ,$info['billboard_end_time']);
        $form_html .= $this->myhtml->form_textarea('內容', 'subject', $info['billboard_content']);
        $form_html .= $this->myhtml->form_select('部別', 'type', $info['type'], $type_arr);
        $form_html .= $this->myhtml->form_button('送出');
        $data = array(
            'title' => ' 編輯公告訊息',
            'nav' => '',
            'msg' => '',
            'form_html' => $form_html,
            'enctype' => '',
            'form_action' => '',
            'js' => ''
        );
        $this->load->view('form', $data);
    }

}