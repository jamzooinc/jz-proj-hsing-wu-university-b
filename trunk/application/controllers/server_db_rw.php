<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Server_db_rw extends CI_Controller {

    function index() {
        echo 'bbb';
    }

    function ccc() {
        echo 'NICE';
    }

    function export_db() {
        //if file exist
        if (file_exists("/var/www/html/tmp/stu_course.sql")) {
            unlink("/var/www/html/tmp/stu_course.sql");
        }
        if (file_exists("/var/www/html/tmp/stu_credit.sql")) {
            unlink("/var/www/html/tmp/stu_credit.sql");
        }
        if (file_exists("/var/www/html/tmp/member_depart.sql")) {
            unlink("/var/www/html/tmp/member_depart.sql");
        }
        if (file_exists("/var/www/html/tmp/class.sql")) {
            unlink("/var/www/html/tmp/class.sql");
        }
        //unlink it.


        $dbhost = 'localhost:3306';
        $dbuser = 'root';
        $dbpass = 'jamzoo!!mysql';
        $conn = mysql_connect($dbhost, $dbuser, $dbpass);
        if (!$conn) {
            die('Could not connect: ' . mysql_error());
        }


        mysql_select_db('hwu');

        //there are four db.
        //class
        //member_depart
        //stu_course
        //stu_credit

        
        
        $table_name = "stu_course";
        $backup_file = "/var/www/html/tmp/stu_course.sql";
        $sql = "SELECT * INTO OUTFILE '$backup_file' FROM $table_name";

        $retval = mysql_query($sql, $conn);
        if (!$retval) {
            die('Could not take data backup: ' . mysql_error());
        }
        echo "Backedup  data successfully\n";


        $table_name = "stu_credit";
        $backup_file = "/var/www/html/tmp/stu_credit.sql";
        $sql = "SELECT * INTO OUTFILE '$backup_file' FROM $table_name";

        $retval = mysql_query($sql, $conn);
        if (!$retval) {
            die('Could not take data backup: ' . mysql_error());
        }
        echo "Backedup  data successfully\n";


        $table_name = "member_depart";
        $backup_file = "/var/www/html/tmp/member_depart.sql";
        $sql = "SELECT * INTO OUTFILE '$backup_file' FROM $table_name";

        $retval = mysql_query($sql, $conn);
        if (!$retval) {
            die('Could not take data backup: ' . mysql_error());
        }
        echo "Backedup  data successfully\n";


        $table_name = "class";
        $backup_file = "/var/www/html/tmp/class.sql";
        $sql = "SELECT * INTO OUTFILE '$backup_file' FROM $table_name";

        $retval = mysql_query($sql, $conn);
        if (!$retval) {
            die('Could not take data backup: ' . mysql_error());
        }
        echo "Backedup  data successfully\n";

        mysql_close($conn);
    }

    function import_db() {
        $time_pre = microtime(true);
        
        //call remote web server to dump database to temp.
        $file = file_get_contents('http://120.102.129.95/export_db/export_db.php', true);
        if($file != "ALL_DONE"){
            echo 'NO OUTPUT BYE';
            die();
        }
        
        
        $dbhost = 'localhost:3306';
        $dbuser = 'root';
        $dbpass = 'jamzoo!!mysql';
        $conn = mysql_connect($dbhost, $dbuser, $dbpass);
        if (!$conn) {
            die('Could not connect: ' . mysql_error());
        }
        mysql_select_db('hwu');
        
        //there are four db.
        //class
        //member_depart
        //stu_course
        //stu_credit
        
        
        copy("http://120.102.129.95/tmp/class.sql", "/var/www/html/tmp/class.sql");
        copy("http://120.102.129.95/tmp/member_depart.sql", "/var/www/html/tmp/member_depart.sql");
        copy("http://120.102.129.95/tmp/stu_course.sql", "/var/www/html/tmp/stu_course.sql");
        copy("http://120.102.129.95/tmp/stu_credit.sql", "/var/www/html/tmp/stu_credit.sql");
        
        $tableName = 'class';
        $sql = "DELETE FROM $tableName;";
        $retval = mysql_query($sql);
        if (!$retval) {
            die('Could not take data backup: ' . mysql_error());
        }
        echo "DB $tableName delete successfully\n";
        $file = "/var/www/html/tmp/class.sql";
        $query = "LOAD DATA LOCAL INFILE '$file' REPLACE INTO TABLE $tableName CHARACTER SET UTF8;";
        $result = mysql_query($query);
        echo "$tableName BACKUP DONE\n";
        
        $tableName = 'member_depart';
        $sql = "DELETE FROM $tableName;";
        $retval = mysql_query($sql);
        if (!$retval) {
            die('Could not take data backup: ' . mysql_error());
        }
        echo "DB $tableName delete successfully\n";
        
        $file = "/var/www/html/tmp/member_depart.sql";
        $query = "LOAD DATA LOCAL INFILE '$file' REPLACE INTO TABLE $tableName CHARACTER SET UTF8;";
        $result = mysql_query($query);
        echo "$tableName BACKUP DONE\n";
        
        $tableName = 'stu_course';
        $sql = "DELETE FROM $tableName;";
        $retval = mysql_query($sql);
        if (!$retval) {
            die('Could not take data backup: ' . mysql_error());
        }
        echo "DB $tableName delete successfully\n";
        $file = "/var/www/html/tmp/stu_course.sql";
        $query = "LOAD DATA LOCAL INFILE '$file' REPLACE INTO TABLE $tableName CHARACTER SET UTF8;";
        $result = mysql_query($query);
        echo "$tableName BACKUP DONE\n";
        
        $tableName = 'stu_credit';
        $sql = "DELETE FROM $tableName;";
        $retval = mysql_query($sql);
        if (!$retval) {
            die('Could not take data backup: ' . mysql_error());
        }
        echo "DB $tableName delete successfully\n";
        $file = "/var/www/html/tmp/stu_credit.sql";
        $query = "LOAD DATA LOCAL INFILE '$file' REPLACE INTO TABLE $tableName CHARACTER SET UTF8;";
        $result = mysql_query($query);
        mysql_close($conn);
        echo "$tableName BACKUP DONE\n";
        
        echo "ALL_DONE\n";
        
        $time_post = microtime(true);
        $exec_time = $time_post - $time_pre;
        $file = '/var/www/html/log/remote_db_sync_log.txt';
        $date = date("Y-m-d H:i:s");
        $current = "REMOTE SYNC AT DATE {$date} TAKES {$exec_time}\n";
        file_put_contents($file, $current, FILE_APPEND);
    }

}