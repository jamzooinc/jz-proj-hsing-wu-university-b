<?php
class Member extends CI_Controller{
	function __construct()
	{
		  parent::__construct();
		  $this->load->database('default');
		  $this->load->library('myform');
		  $this->load->library('myhtml');
		  $this->load->model('ven');
		  
		  
	}
//---會員清單-----
	function list_member(){
		$vendor_id = $this->ven->select_vendor('會員列表');
		if($vendor_id == "" ){
		}else{
			$table_tr= '';
			$member_query = $this->db->where('vendor_id',$vendor_id);
			$i = 0;
			$type = $this->input->get('type');
			if($type != ""){
				$member_query = $this->db->where('type',$type);
			}
			$search = $this->input->get('search');
			if($search != ""){
				$member_query = $this->db->like('member_name',$search);
				$member_query = $this->db->or_like('member_id',$search);
			}

			$member_query = $this->db->order_by('last_datetime','desc');
			foreach($member_query->get('member')->result_array() as $member){ //取得會員清單
				$i++;
				$modal_html = '';
				$this->db->where('member_id',$member['member_id']);
				$this->db->where('vendor_id',$member['vendor_id']);
				$this->db->order_by('datetime','desc');
				$modal_tr = '';
				foreach($this->db->get('member_client')->result_array() as $client){ //取得使用者裝置
					$modal_tr .= '<tr>
									<td>'.$client['os'].'</td>
									<td>'.$client['os_key'].'</td>
									<td>'.$client['datetime'].'</td>
									<td>
										<li class="icon-remove remove_client" id="'.$client['member_id'].'" os="'.$client['os'].'" os_key="'.$client['os_key'].'" vendor_id="'.$client['vendor_id'].'"></li>
									</td>
								</tr>'; 
				}
				$modal_html = '<table class="table table-striped"><tr><td>作業系統</td><td>裝置編號</td><td>最後登入時間</td><td></td></tr>'.$modal_tr.'</table>';
				
								
				
				$table_tr .= '<tr>
								<td>'.$this->myhtml->crop_string($member['member_id'],10).'</td>
								<td>'.$this->myhtml->crop_string($member['member_name'],10).'</td>
								<td>'.$member['last_datetime'].'</td>
								<td>'.$member['type'].'</td>
								<td>
									<a href="'.base_url().'member/member_point/?member_id='.$member['member_id'].'&vendor_id='.$vendor_id.'"><li class="icon-heart" title="點數"></li> 點數 </a> | 
									<a href="'.base_url().'order/list_order/?id='.$member['member_id'].'&vendor_id='.$vendor_id.'"><li class="icon-list" title="訂單"></li> 訂單 </a> | 
									<a href="'.base_url().'member/edit_member/?member_id='.$member['member_id'].'&vendor_id='.$vendor_id.'"><li class="icon-edit" title="編輯"></li> 編輯 </a> | 
									<a data-toggle="modal"  href="#Modal_'.$i.'"><li class="icon-wrench" title="查詢使用者裝置"></li> 裝置 </a> | 
									'.$this->myhtml->modal('查詢使用者裝置',$modal_html,'Modal_'.$i).'
								</td>
							</tr>';
			}
			
			$table = '<table class="table table-striped">
				<tr><th>email</th><th>姓名</th><th>最後登入時間</th><th>狀態</th><th></th></tr>
				'.$table_tr.'
				</table>';
			$js = "
			$('.remove_client').on('click', function() {  
				var id = $(this).attr('id');
				var os = $(this).attr('os');
				var os_key = $(this).attr('os_key');
				var vendor_id = $(this).attr('vendor_id');
				var newhtml = ($(this).text());
				var _sender = $(this);
				$.ajax({
					url:'".base_url()."api_member/remove_client/?id=' + id + '&os=' + os + '&os_key=' + os_key + '&vendor_id=' + vendor_id,
					type:'GET',
					dataType:'json',
					success:function(json) {
						if(json['sys_code'] == '200'){
							$(_sender).parents('tr').eq(0).remove();
							alert('裝置移除成功');
						}
					}
				});
			});  
		";
			$nav = '<ul class="nav nav-pills">
					<form class="form-search">
					  <input type="text" class="input-medium search-query" name="search">
					  '.$this->myhtml->form_hidden('vendor_id',$vendor_id).'
					  <button type="submit" class="btn">Search</button>
					</form>
					<a href='.base_url().'member/list_member/?type=checking&vendor_id='.$vendor_id.'> 只顯示會員</a></ul>';
			
			$data = array(
				'title'=>'會員列表',
				'nav'=>$nav,
				'table'=>$table,
				'js'=>$js,
				'page_bar'=>'',
			);
			$this->load->view('list',$data);
		}
		
	}
//----修改會員資料 -----
	function edit_member(){
		$vendor_id = $this->input->get_post('vendor_id');
		$member_id = $this->input->get_post('member_id');
		$act = $this->input->get_post('act');
		
		foreach($this->db->get_where('member',array('member_id'=>$member_id))->result_array() as $old_member){}
		// print_r($old_member);
		switch($act){
		case '':
			if($old_member['epaper'] == '1'){$paper = 'checked="checked"';}else{$paper = '';}
			$form_html = '';
			$form_html .= $this->myhtml->form_hidden('act','upadte');
			$form_html .= $this->myhtml->form_hidden('vendor_id',$vendor_id);
			$form_html .= $this->myhtml->form_text_input('會員帳號','member_id',$member_id,'readonly');
			$form_html .= '
			<div class="control-group">
				<label class="control-label" >修改密碼</label>
				<div class="controls" id="passwd">
				 <span class="passwd">修改密碼點我</span>
				</div>
			 </div>
			';
			$form_html .= $this->myhtml->form_text_input('會員名稱','member_name',$old_member['member_name']);
			$form_html .= $this->myhtml->form_date('會員生日','birthday',$old_member['birthday']);
			// $form_html .= $this->myhtml->form_text_input('性別','gender',$old_member['gender']);
			$form_html .= $this->myhtml->form_text_input('電話','phone',$old_member['phone']);
			$form_html .= '
			<div class="control-group">
				<label class="control-label" ></label>
				<div class="controls">
				 <label class="checkbox">
				  <input type="checkbox" value="1" name="epaper" '.$paper.'> 訂閱電子報
				</label>
				</div>
			 </div>
			';
			$js = '
				$(\'.passwd\').on(\'click\',function(){
					// $(this).remove();
					$(this).parents(\'#passwd\').html("<input  name=\"member_pwd\">");
				})
				
			';
				
			
			$form_html .= $this->myhtml->form_button('送出');
			$data = array(
				'title' => '修改會員資料',
				'enctype'=>'multipart/form-data',
				'form_action'=>'',
				'form_html'=>$form_html,
				'nav'=>'',
				'msg'=>'',
				'js'=>$js,
			);
			$this->load->view('form',$data);
		break;
		case 'upadte':
			$member_id = $this->input->post('member_id');
			$vendor_id = $this->input->post('vendor_id');
			if($this->input->post('member_name') != ''){$data['member_name'] = $this->input->post('member_name');}
			if($this->input->post('birthday') != ''){$data['birthday'] = $this->input->post('birthday');}
			if($this->input->post('member_pwd') != ''){$data['member_pwd'] = md5($this->input->post('member_pwd'));}
			if($this->input->post('epaper') == '1'){
				$data['epaper']='1';
			}else{
				$data['epaper']='0';
			}
			$data['last_datetime'] = date("Y-m-d H:i:s");
			$this->db->where('member_id',$member_id);
			$this->db->where('vendor_id',$vendor_id);
			$this->db->update('member',$data);
			redirect(base_url().'member/edit_member/?member_id='.$member_id.'&vendor_id='.$vendor_id);
		break;
		}
	// $this->output->enable_profiler(TRUE);
	}
//---點數-----
	function member_point(){
		// print_r($this->session->userdata);
		$vendor_id = $this->input->get_post('vendor_id');
		$member_id = $this->input->get_post('member_id');
		$act = $this->input->get_post('act');
		$point = $this->input->get_post('point');
		$notice = $this->input->get_post('notice');
		if($act == 'add'){
			// print_r($_GET);
			$this->db->where('member_id',$member_id);
			$this->db->where('vendor_id',$vendor_id);
			foreach($this->db->get('member')->result_array() as $member){}
			$surplus_point = (int)$member['surplus_point'] + (int)$point;
			$this->db->where('member_id',$member_id);
			$this->db->where('vendor_id',$vendor_id);
			$this->db->update('member',array('surplus_point'=>$surplus_point));
			
			$new_data = array(
				'member_id'=>$member_id,
				'vendor_id'=>$vendor_id,
				'chang_point'=>$point,
				'notice'=>$notice,
				'surplus_point'=>$surplus_point,
				'datetime'=>date("Y-m-d H:i:s"),
			);
			$this->db->insert('member_point_log',$new_data);
			redirect(base_url().'member/member_point/?member_id='.$member_id.'&vendor_id='.$vendor_id);
		}else{
			$table_tr = '';
			$this->db->where('vendor_id',$vendor_id);
			$this->db->where('member_id',$member_id);
			$this->db->order_by('datetime','desc');
			foreach($this->db->get('member_point_log')->result_array() as $point){
				$table_tr .= '<tr><td>'.$point['notice'].'</td><td>'.$point['chang_point'].'</td><td>'.$point['surplus_point'].'</td><td>'.$point['datetime'].'</td></tr>';
			}
			$table = '<table class="table table-striped">
					<tr><th>動作</th><th>更動點數</th><th>剩餘點數</th><th>時間</th></tr>
					'.$table_tr.'
					</table>';
			$nav = '<ul class="nav nav-pills">
						<form class="form-search">
						  手動儲值  <input type="text" class="input-medium search-query" name="point">
						  '.$this->myhtml->form_hidden('vendor_id',$vendor_id).'
						  '.$this->myhtml->form_hidden('member_id',$member_id).'
						  '.$this->myhtml->form_hidden('act','add').'
						  '.$this->myhtml->form_hidden('notice','管理員手動儲值('.$this->session->userdata('email').')').'
						  <button type="submit" class="btn"> add </button>
						</form>
					</ul>';
			$data = array(
					'title'=>'會員點數清單',
					'table'=>$table,
					'nav'=>$nav
				);
			$this->load->view('list',$data);
		}
		
		
	}
}

	
