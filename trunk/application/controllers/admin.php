<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Admin extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('mod_admin');
        $this->load->library('myhtml');
    }

    function index() {
        $act = $this->input->post('act');
        if($act == ""){
            $info = $this->mod_admin->admin();
        }else{
            $info = $this->input->post('info');
            
            $this->mod_admin->edit($info);
            echo $this->myhtml->alert_togo('編輯完成', base_url() . 'admin');

        }
        
        $form_html = '';
        $form_html .= $this->myhtml->form_hidden('act','edit');
        $form_html .= $this->myhtml->ckeditor('info',$info) .'<br/>';
        $form_html .= $this->myhtml->form_button('Submit');
        
       $data = array(
            'title' => '行政單位',
            'nav' => '',
            'msg' => '',
            'form_html' => $form_html,
            'enctype' => 'multipart/form-data',
            'form_action' => '',
            'js' => ''
        );
        $this->load->view('form', $data);
    }

}

?>