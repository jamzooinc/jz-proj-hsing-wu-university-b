<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Api_bus extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
	}
	public function index()
	{
		$this->load->model("model_bus",'car481');
		$this->car481->config('481','3903','3957');
		$data=$this->car481->station_query();
		$bus_data[]=array(
			'back_station'=>"林口",
			'back_time'=>$this->car481->time(1),
			'com'=> "三重客運",
			'go_station'=> "圓山捷運站",
			'go_time'=>$this->car481->time(0),
			'route_name'=>"936",
			'station_anme'=>"醒吾科技大學"
		);
		$this->load->model("model_bus",'car443');
		$this->car443->config('443','14221','14248');
		$data=$this->car443->station_query();
		$bus_data[]=array(
			'back_station'=>"樹林",
			'back_time'=>$this->car443->time(1),
			'com'=> "三重客運",
			'go_station'=> "長庚醫院",
			'go_time'=>$this->car443->time(0),
			'route_name'=>"858",
			'station_anme'=>"醒吾科技大學"
		);
		$this->load->model("model_bus",'car101');
		$this->car101->config('101','5104','5167');
		$data=$this->car101->station_query();
		$bus_data[]=array(
			'back_station'=>"林口",
			'back_time'=>$this->car101->time(1),
			'com'=> "台北客運",
			'go_station'=> "板橋",
			'go_time'=>$this->car101->time(0),
			'route_name'=>"920",
			'station_anme'=>"醒吾科技大學"
		);
		$this->load->model("model_bus",'car194');
		$this->car194->config('194','4714','4740');
		$data=$this->car194->station_query();
		$bus_data[]=array(
			'back_station'=>"林口",
			'back_time'=>$this->car194->time(0),
			'com'=> "台北客運",
			'go_station'=> "三重",
			'go_time'=>$this->car194->time(1),
			'route_name'=>"925",
			'station_anme'=>"醒吾科技大學"
		);
		echo json_encode($bus_data);
	}
	/*public function bus_list($route="112"){
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_URL, "http://117.56.77.59/IOTAPT/BusTTEInfoXML?route={$route}&id=intosell&pwd=intosell123");
		curl_setopt($ch, CURLOPT_HEADER, FALSE);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
		$result=curl_exec($ch); 
		curl_close($ch);
		$str=$result;
		$res = @simplexml_load_string($str);
		$res = json_decode(json_encode($res),true);
		echo json_encode($res);
		//echo ($result);
	}*/
}

/* End of file api_bus.php */
/* Location: ./application/controllers/api_bus.php */
?>