<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Api_coupon extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('mod_coupon');
        $this->load->model('mod_chat');
        $this->load->model('start_model');
        $this->load->library('lib_jampush');
    }
    
    
    function  index(){
        $category_id = $this->input->get('category_id');
        $coupon_list = $this->mod_coupon->category_info_list($category_id);
        if($category_id == ""){
            $json_arr['sys_code'] = '000';
            $json_arr['sys_msg'] = '參數不正確';
        }else{
            if( $coupon_list == false){
                $json_arr['sys_code'] = '500';
                $json_arr['sys_msg'] = '處理失敗';
            }else{
                $json_arr['sys_code'] = '200';
                $json_arr['sys_msg'] = '處理完成';
                $json_arr['list'] = $coupon_list ;
            }
        }
        echo json_encode($json_arr);
    }
            
    





//-----使用說明
    function text() {
        echo json_encode($this->mod_coupon->text());
    }

//----優惠內容
    function info() {
        $coupon_id = $this->input->get('coupon_id');
        if ($coupon_id == "") {
            $json_arr['sys_code'] = '000';
            $json_arr['sys_msg'] = '參數錯誤';
        } else {
            $info = $this->mod_coupon->coupon($coupon_id);
            if ($info == false) {
                $json_arr['sys_code'] = '100';
                $json_arr['sys_msg'] = '查無資料';
            } else {
                $json_arr['sys_code'] = '200';
                $json_arr['sys_msg'] = '處理完成';
                $json_arr['coupon'] = $info;
            }
        }
        echo json_encode($json_arr);
    }


    
    //get the menu.
    function get_text(){
        $json_arr = array();
        $json_arr['text'] = "使用下方優惠時請出示此APP及學生證";
        $json_arr['sys_code'] = '200';
        $json_arr['sys_msg'] = 'OK';
        echo json_encode($json_arr);
    }
 function remove(){
     $sn  = $this->input->get('key');
     if( $this->mod_coupon->remove($sn)){
         $json_arr['sys_code'] = '200';
         $json_arr['sys_msg'] = 'Success';
     }else{
         $json_arr['sys_code'] = '500';
         $json_arr['sys_msg'] = 'error';
     }
     echo json_encode($json_arr);
   
 }
}

?>
