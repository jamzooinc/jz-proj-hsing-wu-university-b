<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class api_Mqtt extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('mod_mqtt');
        $this->load->model('start_model');
        $this->load->library('curl');
    }

    /**
     * mqtt初始化設定
     */
    public function setup() {
        // register callback Log
        $link = 'https://hwu-mq2.jampush.com.tw/v1/log/' . mqtt_resCode . '/' . urlencode('http://hwu.ladesign.tw/api_mqtt/channel_log');
        echo $this->curl->just_curl($link);

        //register callback health
        $link = 'https://hwu-mq2.jampush.com.tw/v1/health/' . mqtt_resCode . '/' . urlencode('http://hwu.ladesign.tw/api_mqtt/health');
        echo $this->curl->just_curl($link);
    }

    /**
     * 訊息監聽
     * 
     */
    public function channel_log() {
        $this->load->library('lib_log');
        $info = $this->input->post();
        $data = array(
            'mid' => $this->input->post('mid'),
            'say' => $this->input->post('say'),
            'fid' => $this->input->post('fid'),
            'tid' => $this->input->post('tid'),
            't' => $this->input->post('t'),
            'channel' => $this->input->post('channel'),
        );
        $this->mod_mqtt->save_log($data);
   //     $this->lib_log->save('channel-' . time(), json_encode($info));
    }

    public function health() {
        $this->load->library('lib_log');
        $info = $this->input->post();
      //  $this->lib_log->save('health-' . time(), json_encode($info));
    }

    /**
     * 2013/07/26 @james
     * 幫使用者新增幫使用者新增dxid
     */
    public function member_dxid() {
        $member_id = $this->input->get('member_id', true);
        $dxid = $this->input->get('dxid', true);
        $act = $this->input->get('act');

        if ($act == "" || $dxid == "" || $member_id == "") {
            $json_arr['sys_code'] = '000';
            $json_arr['sys_msg'] = '參數不足';
        } else {
            if ($this->mod_mqtt->member_dxid($member_id, $dxid, $act)) {
                $json_arr['sys_code'] = '200';
                $json_arr['sys_msg'] = 'Success';
            } else {
                $json_arr['sys_code'] = '500';
                $json_arr['sys_msg'] = 'Error';
            }
        }
        echo json_encode($json_arr);
    }

    /**
     * 建立單人聊天頻道
     */
    public function create() {
        $this->load->model('mod_friends');
        $from = $this->input->get('from');
        $to = $this->input->get('to');


        if ($from == "" || $to == "") {
            $json_arr['sys_code'] = '000';
            $json_arr['sys_msg'] = '參數不足';
        } else {
            if ($this->mod_friends->chk_classmate($from, $to)) {
                 $channel = @$this->mod_mqtt->create($from, $to);
                if ($channel != false) {
                    $json_arr['sys_code'] = '200';
                    $json_arr['sys_msg'] = 'Success';
                    $json_arr['channel'] = $channel;
                } else {
                    $json_arr['sys_code'] = '500';
                    $json_arr['sys_msg'] = '處理失敗';
                }
            } elseif ($this->mod_friends->friends_status($from, $to) == 1) {
                $channel = @$this->mod_mqtt->create($from, $to);
                if ($channel != false) {
                    $json_arr['sys_code'] = '200';
                    $json_arr['sys_msg'] = 'Success';
                    $json_arr['channel'] = $channel;
                } else {
                    $json_arr['sys_code'] = '500';
                    $json_arr['sys_msg'] = '處理失敗';
                }
            } else {
                $json_arr['sys_code'] = '500';
                $json_arr['sys_msg'] = '權限不足，請先取得好友或同學關係';
            }
        }
        echo json_encode($json_arr);
    }

    function history() {
        $channel = $this->input->get('channel');
        $from = $this->input->get('from');
        $to = $this->input->get('to');
        $offset = $this->input->get('offset');
        if($channel == ""){
               $channel  =   $this->mod_mqtt->get_channel($from,$to);
        }
       $t = $this->input->get('t');
        if ($channel == "" ) {
            $json_arr['sys_code'] = '000';
            $json_arr['sys_msg'] = '參數錯誤，或查無連線資訊';
        } else {
            if($t == ""){$t = time()*1000;}
            
            if($offset == "" ){$offset = 10;}
            //echo $t;
            $his = $this->mod_mqtt->history($channel, $t,$offset);

            if ($his == false) {
                $json_arr['sys_code'] = '500';
                $json_arr['sys_msg'] = '無資料';
            } else {
                $json_arr['sys_code'] = '200';
                $json_arr['sys_msg'] = 'Success';
                $json_arr['history'] = $his;
            }
        }
        echo json_encode($json_arr);
    }

}

?>
