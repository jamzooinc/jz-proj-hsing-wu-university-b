<?php
class Order extends CI_Controller{
	function __construct()
	{
		  parent::__construct();
		  $this->load->database('default');
		  $this->load->library('myhtml');
		$this->load->model('ven');
	}
//---訂單列表-------
	function list_order(){
		$super_man[] = 'jamzoo@test.com';
		$super_man[] = 'jamzoo@test.com.tw';
		
		if($this->session->flashdata('man_type') == ""){
			$man_type = $this->input->get_post('man_type');
			$this->session->set_flashdata('man_type',$man_type);
		}else{
			$man_type = $this->session->flashdata('man_type');
		}
		$id = $this->input->get_post('id');
		$book_id = $this->input->get_post('book_id');
		$start = $this->input->get_post('start');
		if($start == ""){$start = date("Y-m").'-01';}
		$end = $this->input->get_post('end');
		if($end == ""){
			$end = date("Y-m-d").' 23:59:59';
		}else{
			$end = $end.' 23:59:59';
		}
		if($man_type == 'super'){
			$title = '訂單列表(測試)';
		}else{
			$title = '訂單列表';
		}
		$vendor_id = $this->ven->select_vendor($title);
		$msg = $this->myhtml->msg('info','顯示區間','您目前顯示的訂單區間為：'.$start.' ~ '.$end);
		if($vendor_id == "" ){
			
		}else{
			$table_tr= '';
			if($id != ""){
				$this->db->where('member_id',$id);
			}
			if($book_id != ""){
				$this->db->where('book_id',$book_id);
			}
			$this->db->where('order_datetime >=',$start);
			$this->db->where('order_datetime <=',$end);
			$this->db->where('vendor_id',$vendor_id);
			$this->db->order_by('order_datetime','desc');
			foreach($this->db->get('order_main')->result_array() as $order){
				if(in_array($order['member_id'],$super_man) == true AND $man_type == 'super' ){
				$table_tr .= '<tr>
								<td>'.$order['order_num'].'</td>
								<td>'.$this->myhtml->crop_string($order['member_id'],15).'</td>
								<td>'.$order['book_id'].'</td>
								<td>'.$order['order_datetime'].'</td>
								<td>'.$order['status'].'</td>
								<td>
									<a href="'.base_url().'order/order_info/?order_num='.$order['order_num'].'"><li class="icon-pencil" title="編輯訂單"></li> 編輯 </a> | 
								</td>
								</tr>
								';
				}elseif(in_array($order['member_id'],$super_man) == false AND $man_type != 'super' ){
				$table_tr .= '<tr>
								<td>'.$order['order_num'].'</td>
								<td>'.$this->myhtml->crop_string($order['member_id'],15).'</td>
								<td>'.$order['book_id'].'</td>
								<td>'.$order['order_datetime'].'</td>
								<td>'.$order['status'].'</td>
								<td>
									<a href="'.base_url().'order/order_info/?order_num='.$order['order_num'].'"><li class="icon-pencil" title="編輯訂單"></li> 編輯 </a> | 
								</td>
								</tr>
								';
				}
			}
			
			$table = '<table class="table table-striped">
				<tr><th>訂單編號</th><th>會員</th><th>書籍編號</th><th>交易時間</th><th>狀態</th><th></th></tr>
				'.$table_tr.'
				</table>';
			
			$nav = '<ul class="nav nav-pills">
				<form class="form-search">
					  '.$this->myhtml->form_hidden('vendor_id',$vendor_id).'
					  選擇區間：<input type="text" class="datepicker" name="start" /> ~ 
					  <input type="text" class="datepicker" name="end" />
					  <button type="submit" class="btn">Search</button>
					</form>
			</ul>';
			// $nav = '';
			$data = array(
				'title'=>$title,
				'nav'=>$nav,
				'table'=>$table,
				'msg'=>$msg,
			);
			$this->load->view('list',$data);
		}
		// $this->output->enable_profiler(TRUE);
	}
//-----訂單內容 ------
	function order_info(){
		$status_arr[0] = '新訂單未付款';
		$status_arr[1] = '付款完成';
		$status_arr[2] = '取消';
		$order_num = $this->input->get_post('order_num');
		$act = $this->input->post('act');
		if($act == ""){
			$form_html = '';
			foreach($this->db->get_where('order_main',array('order_num'=>$order_num))->result_array() as $order_main){}
			foreach($this->db->get_where('book',array('book_id'=>$order_main['book_id']))->result_array() as $book){}
			foreach($this->db->get_where('vendor',array('vendor_id'=>$order_main['vendor_id']))->result_array() as $vendor){}
			switch($order_main['status']){
			case '0':
				$opt_arr[] = array('text'=>'新訂單未付款','value'=>'0');
				$opt_arr[] = array('text'=>'付款完成','value'=>'1');
				$opt_arr[] = array('text'=>'取消','value'=>'2');
			break;
			case '1':
				$opt_arr[] = array('text'=>'付款完成','value'=>'1');
			break;
			case '2':
			break;
			}
			$form_html .= '<table class="table table-striped">
						<tr><th>訂單編號<th><td>'.$order_main['order_num'].'</td></tr>
						<tr><th>書籍<th><td>'.$book['book_name'].' ('.$order_main['book_id'].')</td></tr>
						<tr><th>金額<th><td>'.$order_main['price'].'</td></tr>
						<tr><th>訂單日期<th><td>'.$order_main['order_datetime'].'</td></tr>
						<tr><th>出版商<th><td>'.$vendor['vendor_name'].' ('.$order_main['vendor_id'].')</td></tr>
			</table>';
			
			$form_html .= $this->myhtml->form_hidden('act','update');
			$form_html .= $this->myhtml->form_hidden('order_num',$order_main['order_num']);
			$form_html .= $this->myhtml->form_select('訂單狀態','status',$order_main['status'],$opt_arr);
			$form_html .= $this->myhtml->form_button('送出');
			
			
			$form_action = base_url().'order/order_info/';
				$data = array(
					'title' => '訂單內容',
					'enctype'=>'multipart/form-data',
					'form_action'=>$form_action,
					'form_html'=>$form_html,
				);
			$this->load->view('form',$data);
			// $this->output->enable_profiler(TRUE);
		}else{
			foreach($this->db->get_where('order_main',array('order_num'=>$order_num))->result_array() as $order_main){}
			switch($this->input->post('status')){
			case '1':
				if($order_main['status'] == 0){
					$this->db->where('order_num',$order_num);
					$this->db->update('order_main',array('status'=>'1','order_datetime'=>date("Y-m-d H:i:s")));
					$data = array(
						'order_num'=>$order_num,
						'datetime'=>date("Y-m-d H:i:s"),
						'do'=>'status == 1',
						'result'=>'(200) '.$this->session->userdata('name').' 變更交易狀態',
					);
					$this->db->insert('order_log',$data);
				}
			break;
			case '2':
			break;
			}
			// $this->output->enable_profiler(TRUE);
			redirect(base_url().'order/list_order/?vendor_id='.$order_main['vendor_id']);
		}
	}
}

	
