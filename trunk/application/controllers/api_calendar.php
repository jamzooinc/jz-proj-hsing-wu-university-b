<?php
/**
 * 日間部接  get_calendar_day
 * 夜間部接 get_calendar_evening
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Api_calendar extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->model('mod_calendar');
    }

    

    



    public function get_calendar_day() {
        $calendar = $this->db->get('calendar');
        $data_arr = array();
        
        $i = 1;
        foreach($calendar->result() as $key => $row){
            $json_arr['name'.$i] = '日間'.$row->calendar_year;
            foreach($row as $row_key => $column){
                if($row_key == 'semester_1_day' || $row_key == 'semester_2_day'){
                    if(!empty($column)){
                        $json_arr[$row_key.$i] = base_url('upload/calendar/').'/'.$column;
                    }else{
                        $json_arr[$row_key.$i] = ''; 
                    }
                }
            }
            $i += 1;
        }
        $this->db->where('billboard_start_time <=', date("Y-m-d"));
        $this->db->where('billboard_end_time >=', date("Y-m-d"));
        $this->db->order_by("billboard_start_time","desc");
        $billboard = $this->db->get('billboard');
        $billboard_arr = array();
        foreach($billboard->result() as $key => $row){
            if($row->type == '全校'||$row->type == '日間部')
                $billboard_arr[] = $row;
        }
        

        $json_arr['billboard'] = $billboard_arr;
        $json_arr['sys_code'] = '200';
        $json_arr['sys_msg'] = 'send OK';
        echo json_encode($json_arr);
    }
    
    //-----------------------------
    public function get_calendar_evening() {
        $calendar = $this->db->get('calendar');
        $data_arr = array();
        $i = 1;
        foreach($calendar->result() as $key => $row){
            $json_arr['name'.$i] = '夜間'.$row->calendar_year;
            foreach($row as $row_key => $column){
                if($row_key == 'semester_1_evening' || $row_key == 'semester_2_evening'){
                    if(!empty($column)){
                        $json_arr[$row_key.$i] = base_url('upload/calendar/').'/'.$column;
                    }else{
                        $json_arr[$row_key.$i] = ''; 
                    }
                }
            }
            $i += 1;
        }
        $this->db->where('billboard_start_time <=', date("Y-m-d"));
        $this->db->where('billboard_end_time >=', date("Y-m-d"));
        $this->db->order_by("billboard_start_time","desc");
        $billboard = $this->db->get('billboard');
        $billboard_arr = array();
        foreach($billboard->result() as $key => $row){
            if($row->type == '全校'||$row->type == '夜間部')
                $billboard_arr[] = $row;
        }
        
        $json_arr['billboard'] = $billboard_arr;
        $json_arr['sys_code'] = '200';
        $json_arr['sys_msg'] = 'send OK';
        echo json_encode($json_arr);
    }
    
    
    
    //-------公告
    function remove_billboard(){
        $key = $this->input->get('key');
        if($this->mod_calendar->remove_billboard($key)){
            $json_arr['sys_code'] = '200';
        }else{
            $json_arr['sys_code'] = '500';
        }
        echo json_encode($json_arr);
    }
   
    
    function remove_year(){
        $year = $this->input->get('key');
        if($this->mod_calendar->remove_year($year)){
            $json_arr['sys_code'] = '200';
        }else{
            $json_arr['sys_code'] = '500';
        }
        echo json_encode($json_arr);
    }
    
}