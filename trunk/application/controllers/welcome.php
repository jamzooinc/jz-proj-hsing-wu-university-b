<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Welcome extends CI_Controller {

	public function __construct() {
		parent::__construct ();
		$this->load->helper('form');
		//~ $this->output->enable_profiler(TRUE);
	}
	
	public function index()
	{

		if($this->session->userdata("manager_id") AND $this->session->userdata('manager_name') ){
			$this->load->view('blank');
			
		}else{
			$this->load->view('login');
		}
	}
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
