<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Api_news extends CI_Controller{
	function __construct()
	{
		  parent::__construct();
	
	}
//---取得清單--------
	function get_list(){
		$json_arr = array();
		foreach($this->db->order_by('create_datetime','desc')->get('msg_message',50,0)->result_array() as $news){
			$json_arr[] = array(
				'msg_id'=>$news['msg_id'],
				'msg_title'=>$news['msg_title'],
				'update_datetime'=>date("Y-m-d H:i:s",$news['create_datetime']),
			);
		}
		
echo json_encode($json_arr);
//echo '';
	}
//---取得內容--------
	function get_info(){
		$msg_id = $this->input->get('msg_id');
		foreach($this->db->get_where('msg_message',array('msg_id'=>$msg_id))->result_array() as $news){
			$json_arr = array(
				'msg_id'=>$news['msg_id'],
				'msg_type'=>$news['msg_type'],
				'msg_date'=>$news['msg_date'],
				'msg_title'=>$news['msg_title'],
				'msg_content'=>$news['msg_content'],
				'msg_url'=>$news['msg_url'],
				'img_url'=>$news['img_url'],
				'file_flag'=>$news['file_flag'],
				'update_datetime'=>date("Y-m-d H:i:s",$news['update_datetime']),
			);
		}
		echo json_encode($json_arr);
	}
}
?>
