<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Api_credit extends CI_Controller{
    function __construct() {
        parent::__construct();
        $this->load->model('start_model');
        $this->load->model('mod_credit');
    }


    /**
 * 查詢學分資料
 */
    function check_credit(){
        $json_arr = array();
        $member_id = $this->input->get('member_id');
        if($member_id == ""){
            $json_arr['sys_code'] = '200';
            $json_arr['sys_msg'] = '參數不足';
        }else{
            $member = $this->mod_credit->member_credit($member_id);
            if($member == false){
                $json_arr['sys_code'] = '500';
            $json_arr['sys_msg'] = 'Error';
            }else{
                $json_arr['sys_code'] = '200';
               $json_arr['sys_msg'] = 'Success';
               $json_arr['credit'] = $member;
            }
        }
        echo json_encode($json_arr);
    }
    
    //
}
    