<?php

class Manager extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->library('myhtml');
        $this->load->model('mod_manager');
        // $this->output->enable_profiler(TRUE);
    }

//---登入-------
    public function login() {
        $username = $this->input->get_post('username');
        $password = $this->input->get_post('password');

        if ($this->mod_manager->login($username, $password) == true) {
            $m = $this->mod_manager->manager($username);
            $session_array = array(
                'manager_id' => $m['manager_id'],
                'manager_name' => $m['manager_name'],
                'manager_email' => $m['manager_email'],
                'manager_level' => $m['manager_level'],
            );
            $this->session->set_userdata($session_array);
            echo $this->myhtml->alert_togo('登入完成', base_url());
        } else {

            echo $this->myhtml->alert_togo('帳密錯誤', base_url());
        }
    }

    function manager_list() {
        $m_list = $this->mod_manager->manager_list();
        $td = '';
        foreach ($m_list as $m) {
            $td .= '<tr>
                            <td>' . $m['manager_id'] . '</td>
                            <td>' . $m['manager_name'] . '</td>
                            <td>
                               <a href="' . base_url() . 'manager/manager_edit/?sn=' . $m['sn'] . '"><li class="icon-edit"></li> 修改帳號</a>  |  
                               <a href="'.base_url().'manager/free_push_log/?manager_id='.$m['manager_id'].'"><li class="icon-th-list"></li> 訊息紀錄
                            </td>
                     </tr>';
        }
        $table = '<table class="table"><tr><th>帳號</th><th>單位名稱</th><th></th></tr>' . $td . '</table>';
        $data = array(
            'title' => '管理員清單',
            'nav' => '',
            'table' => $table,
            'js' => '',
            'msg' => ''
        );
        $this->load->view('list', $data);
    }

    function manager_edit() {
        $sn = $this->input->get_post('sn');
        $m = $this->mod_manager->get_manager($sn);
        $manager_id = $this->input->get_post('manager_id');


        $form_html = $this->myhtml->form_hidden('act', 'update');
        $form_html .= $this->myhtml->form_hidden('sn', $sn);
        $form_html .= $this->myhtml->form_text_input('帳號', 'manager_id', $m['manager_id']);
        $form_html .= $this->myhtml->form_button('送出');

        if ($this->input->get_post('act') == 'update') {
            if ($this->mod_manager->edit_manager($sn, $manager_id)) {
                echo $this->myhtml->alert_togo('修改完成', base_url() . 'manager/manager_list');
            } else {
                echo $this->myhtml->alert_togo('修改失敗', base_url() . 'manager/manager_list');
            }
        }

        $data = array(
            'title' => '修改  ' . $m['manager_name'] . '   帳號',
            'nav' => '',
            'msg' => '',
            'form_html' => $form_html,
            'enctype' => '',
            'form_action' => base_url() . 'manager/manager_edit/',
            'js' => ''
        );
        $this->load->view('form', $data);
    }

    function free_push_log() {
        $this->load->model('mod_jampush');
        $manager_id = $this->input->get('manager_id');
        $log_list = $this->mod_jampush->free_push_log($manager_id);
        $td = '';
        if ($log_list == false) {
            $td = '<tr><td colspan="3">查無資料</td></tr>';
        } else {
            foreach ($log_list as $log) {
                $td .= '<tr>
                                <td>' . $log['datetime'] . '</td>
                                <td>' . $log['subject'] . '</td>
                                <td>' . $log['to'] . '</td>
                        </tr>';
            }
        }
        
        if($this->session->userdata('manager_level') != 'chat'){
             $nav = '<ul class="nav nav-pills">
					<a href="' . base_url() . 'manager/manager_list" class="btn btn-small"><li class="icon-plus"></li> 管理員列表</a>
				</ul>';
        }
        
        $table= '<table class="table"><tr><th>時間</th><th>標題</th><th>對象</th></tr>'.$td.'</table>';
        
        $data = array(
            'title' => $manager_id .  '發送訊息紀錄',
            'nav' => $nav,
            'table' => $table,
            'js' => '',
            'msg' => ''
        );
        $this->load->view('list', $data);
    }

//---登出-------
    function logout() {
        $session_array = array(
            'manager_id' => '',
            'manager_name' => '',
            'manager_email' => '',
        );
        $this->session->unset_userdata($session_array);
        redirect(base_url());
    }

//---修改密碼-------
    function passwd() {
        
    }

}

?>
