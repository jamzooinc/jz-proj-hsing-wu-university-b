<?php

class Recruit extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->library('myhtml');
        $this->load->library('lib_js');
        $this->load->model('mod_recruit');
    }

//---清單-------
    public function index() {
        $manager_id = $this->session->userdata('manager_id');
        $class_arr = $this->mod_recruit->class_list();
        $class = $class_arr[$manager_id]['class_id'];
        $table_tr = '';
        $recruit_list = $this->mod_recruit->get_list($class);
        if ($recruit_list == false) {
            $table_tr = '<tr><td colspan="3" align="center">查無資料</td></tr>';
        } else {
            foreach ($recruit_list as $recruit) {
                $table_tr .= '<tr>
                                        <td>' . $recruit['sn'] . '</td>
                                        <td>' . $recruit['title'] . '</td>
                                            <td>
                                            
                                                <a href="' . base_url() . 'recruit/edit/?sn=' . $recruit['sn'] . '"><li class="icon-edit"></li>  編輯</a>
                                                ' . $this->lib_js->list_remove_icon($recruit['sn']) . '
                                            </td>
                                    </tr>';
            }
        }
        $table = '<table class="table table-striped">
							<tr><th>SN</th><th>標題</th><th>發布時間</th><th></th></tr>
							' . $table_tr . '
						</table>';
        $nav = '<ul class="nav nav-pills">
						<a href="' . base_url() . 'recruit/add" class="btn btn-small"><li class="icon-plus"></li> 新增訊息</a>
                </ul>';
        $js = $this->lib_js->list_remove_js(base_url() . 'api_recruit/remove/');
        $data = array(
            'title' => '招生訊息清單',
            'nav' => $nav,
            'table' => $table,
            'js' => $js,
            'msg' => ''
        );
        $this->load->view('list', $data);
    }

    function add() {
        $manager_id = $this->session->userdata('manager_id');
        $class_arr = $this->mod_recruit->class_list();
        $class = $class_arr[$manager_id]['class_id'];
        $act = $this->input->get_post('act');
        if ($act == 'add') {
            $sn = '-' . time();
            $img_link = '';
            if ($_FILES['img']['name'] != '') {
                $file_name = '/var/www/html/trunk/upload/recruit/recruit' . $sn . '.png';
                if (copy($_FILES['img']['tmp_name'], $file_name)) {
                    $img_link = base_url() . 'upload/recruit/recruit' . $sn . '.png';
                }
            }

            $data = array(
                'sn' => $sn,
                'class_id' => $this->input->get_post('class_id'),
                'title' => $this->input->get_post('title'),
                'img' => $img_link,
                'text' => $this->input->get_post('text'),
            );
            if ($this->mod_recruit->insert($data)) {
                $this->load->library('curl');
                $this->load->model('mod_jampush');
                $this->mod_jampush->save_free_main($sn, $data['title'], 'hwu', '2');
                //print_r($this->mod_jampush->get_dxid());
                $send_json['pid'] = $sn;
                $send_json['app_key'] = jampush_AppKey;
                $send_json['subject'] = $data['title'];
                $send_json['dxid'] = $this->mod_jampush->get_dxid('outside');
                $json = json_encode($send_json);
                header("location:recruit/");
                 //$this->curl->post_json(jampush_single_link, $json);
                // echo $this->myhtml->alert_togo('Success', base_url() . 'recruit');
            } else {
                echo $this->myhtml->alert_togo('Error', base_url() . 'recruit');
            }
        } else {
            $form_html = '';
            $form_html .= $this->myhtml->form_hidden('class_id', $class);
            $form_html .= $this->myhtml->form_hidden('act', 'add');
            $form_html .= $this->myhtml->form_text_input('標題', 'title', '');
            $form_html .= $this->myhtml->form_text_input('圖檔', 'img', '', 'file');
            $form_html .= $this->myhtml->form_textarea('內容說明', 'text', '');
            $form_html .= $this->myhtml->form_button('submit');
            $data = array(
                'title' => '建立新訊息 (' . $class_arr[$manager_id]['class_name'] . ')',
                'nav' => '',
                'msg' => '',
                'form_html' => $form_html,
                'enctype' => 'multipart/form-data',
                'form_action' => '',
                'js' => ''
            );
            $this->load->view('form', $data);
        }
    }

    function edit() {
        $msg = '';
        $manager_id = $this->session->userdata('manager_id');
        $class_arr = $this->mod_recruit->class_list();
        $class = $class_arr[$manager_id]['class_id'];
        $sn = $this->input->get_post('sn');
        $act = $this->input->post('act');
        if ($act == 'update') {
            if ($_FILES['img']['name'] != '') {
                $file_name = '/var/www/html/trunk/upload/recruit/recruit' . $sn . '.png';
                if (copy($_FILES['img']['tmp_name'], $file_name)) {
                    $img_link = base_url() . 'upload/recruit/recruit' . $sn . '.png';
                }
            }
            
            $title = $this->input->post('title');
            $text = $this->input->post('text');
            $this->mod_recruit->edit($sn,$title,$text);
            $msg = $this->myhtml->msg('success','','資料更新完成');
        }
        $recruit = $this->mod_recruit->get_info($sn);
        $form_html = $this->myhtml->form_hidden('act', 'update');
        $form_html .= $this->myhtml->form_hidden('sn', $sn);
        $form_html .= $this->myhtml->form_text_input('標題', 'title', $recruit['title'], "text", "input-xlarge");
        $form_html .= $this->myhtml->form_text_input('圖檔<br/>(未修改則不需重新上傳)', 'img', '', 'file');
        $form_html .= $this->myhtml->form_textarea('內容說明', 'text', $recruit['text']);
        $form_html .= $this->myhtml->form_button('submit');

        $data = array(
            'title' => '編輯訊息 (' . $class_arr[$manager_id]['class_name'] . ')',
            'nav' => '',
            'msg' => $msg,
            'form_html' => $form_html,
            'enctype' => 'multipart/form-data',
            'form_action' => '',
            'js' => ''
        );
        $this->load->view('form', $data);
    }

}

?>
