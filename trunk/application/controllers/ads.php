<?php

class Ads extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('mod_ads');
        $this->load->library('lib_js');
        $this->load->library('myhtml');
    }

//---列表
    function index() {
        $ad_list = $this->mod_ads->get_list();
        $tr = '';
        if ($ad_list != false) {
            foreach ($ad_list as $ad) {
                $tr .= '<tr>
                                <td><a target="_blank" href="' . $ad['goto_url'] . '"><img  width="500px" src="' . $ad['img_url'] . '"></a> <br/>' . $ad['goto_url'] . '</td>
                                <td>
                                    ' . $this->lib_js->list_remove_icon($ad['ads_sn']) . '
                                        <a href="'.base_url().'ads/edit/?ads_sn='.$ad['ads_sn'].'"><li class="icon-edit"></li>編輯</a>
</td>
                                
                                   </tr>';
            }
        }
        $table = '<table class="table table-striped">
							<tr><th>圖檔</th><th></th></tr>
							' . $tr . '
						</table>';
        $js = $this->lib_js->list_remove_js(base_url() . 'api_ads/remove'); //刪除 js
        $nav = '<ul class="nav nav-pills">
					<a href="' . base_url() . 'ads/new_ad" class="btn btn-small"><li class="icon-plus"></li> 新增廣告</a>
				</ul>';
        $data = array(
            'title' => '廣告清單',
            'nav' => $nav,
            'table' => $table,
            'js' => $js,
            'msg' => ''
        );
        $this->load->view('list', $data);
    }

//-----新增
    function new_ad() {
        $act = $this->input->post('act');
        if ($act == 'add') {
            $sn = time();
            $goto_url = $this->input->post('goto_url');
            $photo_name = $_FILES['photo']['name'];
            if ($goto_url == '' || $photo_name == '') {
                echo $this->myhtml->alert_togo('Data Error', base_url() . 'ads');
            } else {
                $img_url = base_url() . 'upload/images/ads/ads-' . $sn . '.png';
                if (copy($_FILES['photo']['tmp_name'], '/var/www/html/trunk/upload/images/ads/ads-' . $sn . '.png')) {
                    if ($this->mod_ads->insert($sn, $img_url, $goto_url)) {
                        echo $this->myhtml->alert_togo('Success', base_url() . 'ads');
                    } else {
                        echo $this->myhtml->alert_togo('Error', base_url() . 'ads');
                    }
                } else {
                    echo $this->myhtml->alert_togo('Error', base_url() . 'ads');
                }
            }
        } else {
            $form_html = '';
            $form_html .= $this->myhtml->form_hidden('act', 'add');
            $form_html .= $this->myhtml->form_text_input('請選擇圖檔上傳', 'photo', '', 'file');
            $form_html .= $this->myhtml->form_text_input('連結網址', 'goto_url', '');
            $form_html .= $this->myhtml->form_button('送出');
            $data = array(
                'title' => '新增廣告',
                'nav' => '',
                'msg' => '',
                'form_html' => $form_html,
                'enctype' => 'multipart/form-data',
                'form_action' => '',
                'js' => ''
            );
            $this->load->view('form', $data);
        }
    }

    function edit() {
        $ads_sn = $this->input->get_post('ads_sn');
        $act = $this->input->get_post('act');
        if ($act == 'update') {
            
            $goto_url = $this->input->post('goto_url');
            $photo_name = $_FILES['photo']['name'];

            $img_url = base_url() . 'upload/images/ads/ads-' . $ads_sn . '.png';
            if (copy($_FILES['photo']['tmp_name'], '/var/www/html/trunk/upload/images/ads/ads-' . $ads_sn . '.png')) {
                if ($this->mod_ads->edit($ads_sn, $img_url, $goto_url)) {
                    echo $this->myhtml->alert_togo('Success', base_url() . 'ads');
                } else {
                    echo $this->myhtml->alert_togo('Error', base_url() . 'ads');
                }
            } else {
                echo $this->myhtml->alert_togo('Error', base_url() . 'ads');
            }
        } else {
            $info = $this->mod_ads->get_once($ads_sn);
            $form_html = '';
            $form_html .= $this->myhtml->form_hidden('act', 'update');
            $form_html .= $this->myhtml->form_hidden('ads_sn', $ads_sn);
            $form_html .= $this->myhtml->form_text_input('請選擇圖檔上傳', 'photo', '', 'file');
            $form_html .= $this->myhtml->form_text_input('連結網址', 'goto_url', $info['goto_url']);
            $form_html .= $this->myhtml->form_button('送出');
            $data = array(
                'title' => '編輯廣告',
                'nav' => '',
                'msg' => '',
                'form_html' => $form_html,
                'enctype' => 'multipart/form-data',
                'form_action' => '',
                'js' => ''
            );
            $this->load->view('form', $data);
        }
    }

}