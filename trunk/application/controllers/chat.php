<?php

class Chat extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->library('myhtml');
        $this->load->model('mod_chat');
    }

//---清單-------
    public function index() {
        $manager_id = $this->session->userdata('manager_id');
        $table = '';
        $table_tr = '';
        $chat = array();
        foreach ($this->mod_chat->chat_self($manager_id) as $chat) {
            
            $new = '';
            if ($chat['readed'] == 0) {
                $new = '<li class="icon-exclamation-sign"></li>';
            }
            $table_tr .= '<tr>
									<td>' . $chat['from'] . '</td>
									<td>' . $new . ' ' . $chat['title'] . '</td>
									<td>' . date("Y-m-d H:i:s", $chat['datetime']) . '</td>
									<td>
										<span class="repo" to="' . $chat['from'] . '"><li class="icon-share"></li>直接回覆</span>
									</td>
									</tr>';
        }
        //echo $this->db->last_query();
        if (count($chat) < 1) {
            $msg = $this->myhtml->msg('info', '提示', '查無資料');
        } else {
            $msg = '';
            $table = '<table class="table table-striped">
							<tr><th>寄件者</th><th>內容</th><th>時間</th><th></th></tr>
							' . $table_tr . '
						</table>';
        }
        $js = '
			$(".repo").on("click",function(){
				var _sender = $(this);
				var title = prompt("輸入回覆內容");
				var to = $(this).attr("to");
				var url = "' . base_url() . 'api_chat/send_push/?from= + ' . $manager_id . '&to=" + to + "&title="+ title;
				if(title != ""){
				$.getJSON(url,function(json){
						if(json["sys_code"] == "200"){
							alert("完成");
						}else{
								alert(json["sys_msg"]);
						}
					});
				}
			});
		';
        $data = array(
            'title' => 'Chat 清單',
            'nav' => '',
            'table' => $table,
            'js' => $js,
            'msg' => $msg
        );
        $this->load->view('list', $data);
    }

//----發送界面
    function send_push() {
        $manager_id = $this->session->userdata('manager_id');
        $act = $this->input->post('act');
        $msg = '';
        $msg = $this->myhtml->msg('info', '提示', '預設發送給所有人,如果要發送給特定會員請選擇"指定對象發送"功能');
        $form_html = '';
        $form_html = '
			<fieldset>
				<label>輸入發送文字內容</label>
			<input type="text" name="title" id="title" style="width:600px;">
			<button type="button" class="btn">送出</button>
		  </fieldset>
			
			';
        $js = '
			
			$(".btn").on("click",function(){
				var _sender = $(this);
				var title = $("#title").val();
				
				
				var url = "' . base_url() . 'api_chat/send_push/?from=' . $manager_id . '&to=' . $this->mod_chat->manager_target($manager_id) . '&title="+ title;
				//alert(url);
				$.getJSON(url,function(json){
						if(json["sys_code"] == "200"){
							alert("完成");
						}else{
							alert(json["sys_msg"]);
						}
					});
			});
		';
        $data = array(
            'title' => '發送 Chat',
            'nav' => '',
            'msg' => $msg,
            'form_html' => $form_html,
            'enctype' => '',
            'form_action' => '',
            'js' => $js
        );
        $this->load->view('form', $data);
    }

//----發送界面
    function single() {
        $manager_id = $this->session->userdata('manager_id');
        $act = $this->input->post('act');
        $msg = '';
        $msg = $this->myhtml->msg('info', '提示', '預設發送給所有人,如果要發送給特定會員請選擇"指定對象發送"功能');
        $form_html = '';
        $form_html = '
			<fieldset>
				<label>輸入發送文字內容</label>
			<input type="text" name="title" id="title" style="width:600px;">
			<label>輸入指定對象帳號</label>
			<input type="text" name="to" id="to" >
			<button type="button" class="btn">送出</button>
		  </fieldset>
			
			';
        $js = '
			
			$(".btn").on("click",function(){
				var _sender = $(this);
				var title = $("#title").val();
				var to = $("#to").val();
				
				var url = "' . base_url() . 'api_chat/send_push/?from=' . $manager_id . '&to="+to+"&title="+ title;
				//alert(url);
				$.getJSON(url,function(json){
						if(json["sys_code"] == "200"){
							alert("完成");
						}else{
							
							alert(json["sys_msg"]);
						}
					});
			});
		';
        $data = array(
            'title' => '指定單一對象發送',
            'nav' => '',
            'msg' => $msg,
            'form_html' => $form_html,
            'enctype' => '',
            'form_action' => '',
            'js' => $js
        );
        $this->load->view('form', $data);
    }

//----指定對象群組發送------

    function assign() {
        $manager_id = $this->session->userdata('manager_id');
        $chat_key = $this->mod_chat->manager_target($manager_id);
        if($chat_key!="all" and $chat_key!=""){
            $chat_key = 'class_one';
        };

        $msg = '';
        //$extra = '<span id="select_down" style=" cursor: pointer;" ><li class="icon-arrow-down" style=""></li>前往下個階層</span>';
        $form_html = '<fieldset>';
        $form_html .= '<label>選擇發送對象</label><div id="target_div" style="dislpay:inline;"></div>';
        $form_html .= '<label>目前發送對象</label>
            ' . $this->myhtml->form_hidden('target', $chat_key) . $this->myhtml->form_hidden('syscode', "") . $this->myhtml->form_hidden('lv1', "") .$this->myhtml->form_hidden('lv2', "") . '
            	<ul class="breadcrumb" id="breadcrumb"   style="cursor: pointer;">
              <li class="tar_span" chat_key="' . $chat_key . '" style="text-decoration:underline;margin-left:10px;">全部 <span >></span></li>
            </ul><div style="clear:both" />';
        $form_html .= '<label>輸入發送文字內容</label>
			<input type="text" name="title" id="title" style="width:600px;"><br/>';
        $form_html .= $this->myhtml->form_button('送出', 'button');
        $form_html .= '</fieldset>';
        $js = '
		var tar = "' . $chat_key . '";
		var url = "' . base_url() . 'api_chat/js_target_select_tag/?key=" + tar;
		$("#target_div").load(url);
		$("#target_div span").live("click",function(){
				var tar = $(this).attr("chat_key");
                                var syscode = $(this).attr("syscode");
                                var lv1 = $(this).attr("lv1");
                                var lv2 = $(this).attr("lv2");
				var name = $(this).html();
				var url = "' . base_url() . 'api_chat/js_target_select_tag/?key=" + tar + "&syscode=" + syscode + "&lv1=" + lv1 + "&lv2=" + lv2;
				$("#target_div span").fadeOut();
				$("#target_div").load(url);
				var new_html = " <li class=\" tar_span\" chat_key=\""+tar+"\" syscode=\""+syscode+"\" lv1=\""+lv1+"\" lv2=\""+lv2+"\" style=\"text-decoration:underline;margin-left:10px;\">"+name+" <span >></span></li>";
				$("#breadcrumb").append(new_html).hide().fadeIn();
				$("#target").val(tar);
                                $("#syscode").val(syscode);
                                $("#lv1").val(lv1);
                                $("#lv2").val(lv2);
		});
		
		$(".tar_span").live("click",function(){
				$(this).nextAll().fadeOut();
				var tar =  $(this).attr("chat_key");
				var name = $(this).html();
                                var syscode = $(this).attr("syscode");
                                var lv1 = $(this).attr("lv1");
                                var lv2 = $(this).attr("lv2");
				var url = "' . base_url() . 'api_chat/js_target_select_tag/?key=" + tar + "&syscode=" + syscode + "&lv1=" + lv1 + "&lv2=" + lv2;
				$("#target_div").load(url);
				$("#target").val(tar);
                                $("#syscode").val(syscode);
                                $("#lv1").val(lv1);
                                $("#lv2").val(lv2);
		});
		
		
		$("button").on("click",function(){
				var _sender = $(this);
				var title = $("#title").val();
				var to =  $("#target").val();
                                var syscode = $("#syscode").val();
                                var lv1 = $("#lv1").val();
                                var lv2 = $("#lv2").val();
				if(to!="class_one"){
				var url = "' . base_url() . 'api_chat/send_push/?from=' . $manager_id . '&to="+ to +"&title="+ title+ "&syscode=" + syscode + "&lv1=" + lv1 + "&lv2=" + lv2;
				//alert(url);
				$.getJSON(url,function(json){
						if(json["sys_code"] == "200"){
							alert("完成");
						}else{
							alert(json["sys_msg"]);
						}
					});
                                }else{
                                    alert("請選擇欲發送之班級！");
                                }
			});
		';

        $data = array(
            'title' => '指定對象發送',
            'nav' => '',
            'msg' => $msg,
            'form_html' => $form_html,
            'enctype' => '',
            'form_action' => '',
            'js' => $js
        );
        $this->load->view('form', $data);
    }

}

?>
