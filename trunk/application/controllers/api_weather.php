<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Api_weather extends CI_Controller{
	function __construct()
	{
		  parent::__construct();
	
	}
	function index(){
		$w[0]='龍捲風';
		$w[1]="熱帶風暴";
		$w[2]="颶風";
		$w[3]="強雷暴";
		$w[4]="雷暴";
		$w[5]="混合雨雪天氣";
		$w[6]="混合雨和雨夾雪";
		$w[7]="混合雪和雨夾雪";
		$w[8]="冷凍小雨";
		$w[9]="細雨";
		$w[10]="凍雨";
		$w[11]="淋浴";
		$w[12]="淋浴";
		$w[13]="雪飄雪";
		$w[14]="光陣雪";
		$w[15]="吹雪";
		$w[16]="雪";
		$w[17]="冰雹";
		$w[18]="雨雪";
		$w[19]="灰塵";
		$w[20]="朦朧的";
		$w[21]="陰霾";
		$w[22]="煙";
		$w[23]="壞天氣的";
		$w[24]="有風";
		$w[25]="冷";
		$w[26]="陰天";
		$w[27]="晴間多雲（夜間）";
		$w[28]="晴間多雲（日）";
		$w[29]="晴間多雲（夜間）";
		$w[30]="部分多雲（天）";
		$w[31]="清除（夜）";
		$w[32]="晴朗";
		$w[33]="公平（夜）";
		$w[34]="公平（日）";
		$w[35]="混合雨和冰雹";
		$w[36]="熱";
		$w[37]="局部地區性雷暴";
		$w[38]="分散的雷暴";
		$w[39]="分散的雷暴";
		$w[40]="零星陣雨";
		$w[41]="大雪";
		$w[42]="零星陣雪";
		$w[43]="大雪";
		$w[44]="晴間多雲";
		$w[45]="雷陣雨";
		$w[46]="陣雪";
		$w[47]="隔離雷陣雨";
		$w[3200]="不可用";

		//小圖示使用 1 晴天 2 陰天 3 雨天
		$wcodeimg[0]='2';
		$wcodeimg[1]="2";
		$wcodeimg[2]="2";
		$wcodeimg[3]="2";
		$wcodeimg[4]="2";
		$wcodeimg[5]="3";
		$wcodeimg[6]="3";
		$wcodeimg[7]="3";
		$wcodeimg[8]="3";
		$wcodeimg[9]="3";
		$wcodeimg[10]="3";
		$wcodeimg[11]="3";
		$wcodeimg[12]="3";
		$wcodeimg[13]="2";
		$wcodeimg[14]="2";
		$wcodeimg[15]="3";
		$wcodeimg[16]="3";
		$wcodeimg[17]="3";
		$wcodeimg[18]="3";
		$wcodeimg[19]="2";
		$wcodeimg[20]="2";
		$wcodeimg[21]="2";
		$wcodeimg[22]="2";
		$wcodeimg[23]="2";
		$wcodeimg[24]="2";
		$wcodeimg[25]="2";
		$wcodeimg[26]="2";
		$wcodeimg[27]="1";
		$wcodeimg[28]="1";
		$wcodeimg[29]="1";
		$wcodeimg[30]="1";
		$wcodeimg[31]="1";
		$wcodeimg[32]="1";
		$wcodeimg[33]="1";
		$wcodeimg[34]="1";
		$wcodeimg[35]="3";
		$wcodeimg[36]="1";
		$wcodeimg[37]="3";
		$wcodeimg[38]="3";
		$wcodeimg[39]="3";
		$wcodeimg[40]="3";
		$wcodeimg[41]="3";
		$wcodeimg[42]="3";
		$wcodeimg[43]="3";
		$wcodeimg[44]="1";
		$wcodeimg[45]="3";
		$wcodeimg[46]="3";
		$wcodeimg[47]="3";
		$wcodeimg[3200]="3";

		$wget = $_GET['w'];
		$xml = simplexml_load_file("http://weather.yahooapis.com/forecastrss?u=c&w=$wget");

		foreach($xml->channel->children('yweather', TRUE)->location->attributes() as $a1 => $a2){
			$n[$a1] = (string)$a2;
		}
		
		foreach($xml->channel->item->children('yweather', TRUE)->forecast[0]->attributes() As $a => $b){
			$weather[$a]  = (string)$b;
		}
		$weather["ImgCode"] = $wcodeimg[$weather["code"]];

		$outPutArray['City'] = $n['country'].' '.$n['city'];
		$outPutArray['Temperature'] = $weather['low'].'~'.$weather['high'];
		$outPutArray['ImgCode'] = $wcodeimg[$weather["code"]];
		//取得版本號
		foreach($this->db->get('version',1,0)->result_array() as $ver){
			$outPutArray['ver']['ios'] = $ver['ios'];
			$outPutArray['ver']['andorid'] = $ver['andorid'];
		}

		echo urldecode(json_encode($outPutArray));

	}
}
?>