<?php

class Api_tool extends CI_Controller {

    function index() {
        $this->load->view('api_tool');
    }

    function api() {
        $json_arr = array();
        $api_arr = array();

        //----Jampush----
        $api_arr[] = array('CLASS' => 'HEADER', 'HEADER_NAME' => 'Push');
        $api_arr[] = array('URL' => 'http://hwu.ladesign.tw/api_jampush/get_free_push/', 'GET' => 'pid', 'POST' => '', 'TITLE' => '負值取頻道', 'COMMENT' => '收到PUSH之後如果pid是負值就接這隻，channel就是頻道  smart=>智慧型公佈欄,', 'EXAMPLE' => 'http://hwu.ladesign.tw/api_jampush/get_free_push/?pid=-1375503105');
        //----智慧型公佈欄
        $api_arr[] = array('CLASS' => 'HEADER', 'HEADER_NAME' => '智慧型公佈欄');
        $api_arr[] = array('URL' => 'http://hwu.ladesign.tw/api_smart/smart_list', 'GET' => '', 'POST' => '', 'TITLE' => '智慧型公佈欄清單', 'COMMENT' => '', 'EXAMPLE' => 'http://hwu.ladesign.tw/api_mqtt/create/?from=1011501003&to=1011501001');
        $api_arr[] = array('URL' => 'http://hwu.ladesign.tw/api_smart/get_info/', 'GET' => 'pid', 'POST' => '', 'TITLE' => '智慧型公佈欄內容', 'COMMENT' => '八種不同的推撥都接在同一個api上，type的區分方式如下0,標題 1,文字＋標題 2.圖文 3.youtube 4.音樂 5.地圖連結 6.網址連結 7.行事曆', 'EXAMPLE' => 'http://hwu.ladesign.tw/api_mqtt/create/?from=1011501003&to=1011501001');
        //----聊天功能
        $api_arr[] = array('CLASS' => 'HEADER', 'HEADER_NAME' => 'MQTT 聊天');
        $api_arr[] = array('URL' => 'http://hwu.ladesign.tw/api_mqtt/member_dxid/', 'GET' => 'member_id|dxid|act', 'POST' => '', 'TITLE' => '註冊使用者DXID', 'COMMENT' => 'act有remove,add兩種狀態，使用者第一次進來時請使用這個功能把mqtt使用的dxid給後端', 'EXAMPLE' => 'http://hwu.ladesign.tw/api_mqtt/create/?from=1011501003&to=1011501001');
        $api_arr[] = array('URL' => 'http://hwu.ladesign.tw/api_mqtt/create/', 'GET' => 'from|to', 'POST' => '', 'TITLE' => '訂閱聊天頻道', 'COMMENT' => '開始聊天之前需要先用這個api建立聊天頻道，這隻api會將兩個帳號的dxid送去mqtt server 註冊，接下來的聊天內容請與mqtt server接收資料', 'EXAMPLE' => 'http://hwu.ladesign.tw/api_mqtt/create/?from=1011501003&to=1011501001');
        $api_arr[] = array('URL' => 'http://hwu.ladesign.tw/api_talk/block/', 'GET' => 'from|to|switch', 'POST' => '', 'TITLE' => '黑名單', 'COMMENT' => '好友或同學的關係都可以設定黑名單，需要 from to 和  switch  。switch 參數輸入 on 或 off', 'EXAMPLE' => 'http://hwu.ladesign.tw/api_talk/block/?from=1011501001&to=1011501003&switch=on');
        $api_arr[] = array('URL' => 'http://hwu.ladesign.tw/api_talk/search_member/', 'GET' => 'from|to', 'POST' => '', 'TITLE' => '搜尋會員', 'COMMENT' => '要吐出黑名單關係所以需要有自己的帳號和對方的帳號', 'EXAMPLE' => 'http://hwu.ladesign.tw/api_talk/search_member/?from=1011501001&to=1011501003');
        $api_arr[] = array('URL' => 'http://hwu.ladesign.tw/api_talk/friend_add/', 'GET' => 'from|to', 'POST' => '', 'TITLE' => '加入為好友', 'COMMENT' => '輸入對方的帳號和自己的帳號就會送出邀請，對方同意也是用這隻api，當兩邊都呼叫完畢後就可以是好友狀態了', 'EXAMPLE' => 'http://hwu.ladesign.tw/api_talk/friend_add/?from=1011501003&to=1011501001');
        $api_arr[] = array('URL' => 'http://hwu.ladesign.tw/api_talk/friend_unlink/', 'GET' => 'from|to', 'POST' => '', 'TITLE' => '刪除好友', 'COMMENT' => '刪除好友功能只能刪除好友，同學關係不能刪除，只能封鎖', 'EXAMPLE' => 'http://hwu.ladesign.tw/api_talk/friend_unlink/?from=1011501003&to=1011501001');
        $api_arr[] = array('URL' => 'http://hwu.ladesign.tw/api_talk/contacts_list/', 'GET' => 'member_id', 'POST' => '', 'TITLE' => '好友關係清單', 'COMMENT' => '會取得三種狀態的清單同時列出,classmate,friends,to_be_confirm   。to_be_confirm 是接收到邀請成為好友的請求清單', 'EXAMPLE' => 'http://hwu.ladesign.tw/api_talk/friend_unlink/?from=1011501003&to=1011501001');

        //-----行事曆
        $api_arr[] = array('CLASS' => 'HEADER', 'HEADER_NAME' => '行事曆（待修）');
        $api_arr[] = array('URL' => 'http://hwu.ladesign.tw/api_calendar/get_calendar_day/', 'GET' => '', 'POST' => '', 'TITLE' => '日間部', 'COMMENT' => '', 'EXAMPLE' => 'http://hwu.ladesign.tw/api_mqtt/create/?from=1011501003&to=1011501001');
        $api_arr[] = array('URL' => 'http://hwu.ladesign.tw/api_calendar/get_calendar_evening/', 'GET' => '', 'POST' => '', 'TITLE' => '夜間部', 'COMMENT' => '', 'EXAMPLE' => 'http://hwu.ladesign.tw/api_mqtt/create/?from=1011501003&to=1011501001');

        //---Chat
        $api_arr[] = array('CLASS' => 'HEADER', 'HEADER_NAME' => '醒吾Chat（待修）');
        //---英文測驗
        $api_arr[] = array('CLASS' => 'HEADER', 'HEADER_NAME' => '英文測驗');
        $api_arr[] = array('URL' => 'http://hwu.ladesign.tw/api_exam/get_now_exam/', 'GET' => 'type', 'POST' => '', 'TITLE' => '取得目前題庫編號', 'COMMENT' => '需要輸入type取得題庫類型 1,2,3', 'EXAMPLE' => 'http://hwu.ladesign.tw/api_exam/get_now_exam/?type=1');
        $api_arr[] = array('URL' => 'http://hwu.ladesign.tw/api_exam/get_exam/', 'GET' => 'q_id', 'POST' => '', 'TITLE' => '取得題庫內容', 'COMMENT' => '需要輸入type取得題庫類型 1,2,3', 'EXAMPLE' => 'http://hwu.ladesign.tw/api_exam/get_exam/?q_id=1369737221');
        //---會員功能
        $api_arr[] = array('CLASS' => 'HEADER', 'HEADER_NAME' => 'MEMBER');
        $api_arr[] = array('URL' => 'http://hwu.ladesign.tw/api_member/check_member', 'GET' => 'member_id', 'POST' => '', 'TITLE' => '查詢會員資料', 'COMMENT' => '取得會員資料

    input  *member_id 帳號(email address)

    output  member_id id
            datetime  最後登入時間
            dxid      dxid
            email     email
            nickname  暱稱
            avatar    大頭照 URL
            jampush   是否 jampush

    sys_out 200 OK
            201 無此人', 'EXAMPLE' => 'http://hwu.ladesign.tw/api_member/check_member?member_id=101110');

        $api_arr[] = array('URL' => 'http://hwu.ladesign.tw/api_member/update_nickname', 'GET' => 'member_id|nickname', 'POST' => '', 'TITLE' => '更改暱稱', 'COMMENT' => '取得會員資料

    input *member_id   會員ID
          *nickname    新暱稱

    sys_out 200 OK
            201 無此人
            301 缺參數', 'EXAMPLE' => '');

        $api_arr[] = array('URL' => 'http://hwu.ladesign.tw/api_member/update_avatar', 'GET' => '', 'POST' => 'member_id|avatar', 'TITLE' => '上傳大頭照', 'COMMENT' => '更新大頭照,上傳的大頭照會被壓城 PNG 檔案存入 Server  (upload/avatar)

    input *member_id     會員ID  
          *avatar        大頭照(base64_encode) 


    output avatar_url 大頭照的ＵＲＬ

    sys_out 200 OK
            201 無此人
            301 缺參數', 'EXAMPLE' => '');


        $api_arr[] = array('URL' => 'http://hwu.ladesign.tw/api_member/update_resttime', 'GET' => 'member_id|start|end', 'POST' => '', 'TITLE' => '更新睡眠時間', 'COMMENT' => '更新睡眠時間.

    input *member_id     會員ID  
          *start         睡眠起始時間
          *end           睡眠結束時間

    sys_out 200 OK
            201 無此人
            301 缺參數', 'EXAMPLE' => 'http://hwu.ladesign.tw/api_member/update_resttime?member_id=101110&start=20:00&end=8:00');



        $api_arr[] = array('CLASS' => 'HEADER', 'HEADER_NAME' => 'COUPON');


        $api_arr[] = array('URL' => 'http://hwu.ladesign.tw/api_coupon', 'GET' => '', 'POST' => '', 'TITLE' => '列出所有的優惠內容', 'COMMENT' => '列出所有的優惠內容

   output coupon[coupon_id]      ID
                [coupon_name]    標頭
                [coupon_info]    內容
                [coupon_preview] 縮圖', 'EXAMPLE' => '');
        $api_arr[] = array('URL' => 'http://hwu.ladesign.tw/api_coupon', 'GET' => 'type', 'POST' => '', 'TITLE' => '根據類別列出優惠內容', 'COMMENT' => '根據類別列出優惠內容
   input *type  類別

   output coupon->coupon_id   ID
                  coupon_name 標頭
                  coupon_info 內容
                  coupon_img  圖片

   sys_out N/A', 'EXAMPLE' => '');

        $api_arr[] = array('URL' => 'http://hwu.ladesign.tw/api_coupon/info', 'GET' => 'coupon_id', 'POST' => '', 'TITLE' => '根據 coupon id 列出優惠訊息', 'COMMENT' => 'input *coupon_id  優惠卷ID 

   output coupon->coupon_id   ID
                  coupon_name 標頭
                  coupon_info 內容
                  coupon_img  圖片

   sys_out 200 處理完成
           000 參數錯誤
           100 查無資料', 'EXAMPLE' => '');
        $api_arr[] = array('URL' => 'http://hwu.ladesign.tw/api_coupon/list_category', 'GET' => '', 'POST' => '', 'TITLE' => '列出類別的名稱', 'COMMENT' => '列出類別的名稱

   output category->category_id    類別id
                  ->category_name  類別名稱

   sys_out 200 OK', 'EXAMPLE' => '');
        $api_arr[] = array('URL' => 'http://hwu.ladesign.tw/api_coupon/get_text', 'GET' => '', 'POST' => '', 'TITLE' => '取得使用說明', 'COMMENT' => '取得使用說明

   output text 使用說明

   sys_out 200 OK', 'EXAMPLE' => '');

        $api_arr[] = array('CLASS' => 'HEADER', 'HEADER_NAME' => 'SCORE');


        $api_arr[] = array('URL' => 'http://hwu.ladesign.tw/api_score/check_score', 'GET' => 'member_id', 'POST' => '', 'TITLE' => '查詢成績', 'COMMENT' => '查詢成績專用 API , 根據 ID 來查詢成績.

   input *member_id   會員ID

   sys_out 200 OK
           201 無此人', 'EXAMPLE' => 'http://hwu.ladesign.tw/api_score/check_score?member_id=101110');


        $api_arr[] = array('URL' => 'http://hwu.ladesign.tw/api_class/check_class', 'GET' => 'member_id', 'POST' => '', 'TITLE' => '查詢課表', 'COMMENT' => '查詢課表 ＡＰＩ , 根據 ID 查詢課表


   input *member_id   會員ID

   sys_out 200 OK
           201 無此人
           301 Insufficient Parameter', 'EXAMPLE' => 'http://hwu.ladesign.tw/api_class/check_class?member_id=101110');

        $api_arr[] = array('URL' => 'http://hwu.ladesign.tw/api_credit/check_credit', 'GET' => 'member_id', 'POST' => '', 'TITLE' => '查詢學分', 'COMMENT' => '查詢學分 API , 根據 ID 查詢學分 , 有總學分 , 必修學分 , 選修學分.

   input *member_id   會員ID

   sys_out 200 OK
           201 無此人
           301 Insufficient Parameter', 'EXAMPLE' => 'http://hwu.ladesign.tw/api_credit/check_credit?member_id=101110');


        $api_arr[] = array('CLASS' => 'HEADER', 'HEADER_NAME' => 'CALENDAR');

        $api_arr[] = array('URL' => 'http://hwu.ladesign.tw/api_calendar/get_calendar_day', 'GET' => '', 'POST' => '', 'TITLE' => '日間部行事曆', 'COMMENT' => '日間部行事曆

  sys_out 200 OK', 'EXAMPLE' => '');

        $api_arr[] = array('URL' => 'http://hwu.ladesign.tw/api_calendar/get_calendar_evening', 'GET' => '', 'POST' => '', 'TITLE' => '夜間部行事曆', 'COMMENT' => '夜間部行事曆

  sys_out 200 OK', 'EXAMPLE' => '');

        $api_arr[] = array('CLASS' => 'HEADER', 'HEADER_NAME' => 'DOCUMENT');
        $api_arr[] = array('URL' => 'http://hwu.ladesign.tw/doc/document.txt', 'GET' => '', 'POST' => '', 'TITLE' => '下載文件', 'COMMENT' => '請複製連結下載文件', 'EXAMPLE' => '');


        //----ADS
        $api_arr[] = array('CLASS' => 'HEADER', 'HEADER_NAME' => '廣告功能');
        $api_arr[] = array('URL' => 'http://hwu.ladesign.tw/api_ads/', 'GET' => '', 'POST' => '', 'TITLE' => '底部橫幅廣告', 'COMMENT' => '不需要輸入參數，直接抓取回傳圖檔路徑和點下去以後的網址', 'EXAMPLE' => 'http://hwu.ladesign.tw/api_ads');




        $json_arr['content'] = $api_arr;
        echo json_encode($json_arr);
    }

}