<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Api_class extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('start_model');
        $this->load->model('mod_member');
    }
    

    function check_class() {
        $member_id = $this->input->get('member_id');
        if (!$this->start_model->check_parameter_exist($member_id)) {
            $json_arr = $this->start_model->gen_sys_code_msg('301', 'Insufficient Parameter');
            echo json_encode($json_arr);
            die();
        }

        if (!$this->start_model->check_exist('member_main', 'member_id', $member_id)) {
            $json_arr = $this->start_model->gen_sys_code_msg('201', '無此人');
            echo json_encode($json_arr);
            die();
        }

        $grade_name = $this->mod_member->class_name($member_id);
//pon($grade_name);
        $class = $this->db->get('class');
        $json_arr = array();

        
        foreach ($class->result() as $row) {

            if ($row->grade_name == $grade_name) {
                $json_arr['grade_name'] = $row->grade_name;
                $class_data = array('class_name' => $row->class_name, 'class_teacher' => $row->class_teacher, 'class_room' => $row->class_room);
                $json_arr['class'][$row->week_day][$row->class_period][] = $class_data;
            }
        }

        $json_arr = array_merge($json_arr, $this->start_model->gen_sys_code_msg('200', 'OK'));
        echo json_encode($json_arr);
    }

    function check_all() {
        $class = $this->db->get('class');
        $json_arr = array();
        foreach ($class->result() as $row) {
            
        }
        $json_arr['sys_code'] = '200';
        $json_arr['sys_msg'] = 'OK';
        echo json_encode($json_arr);
    }

}
