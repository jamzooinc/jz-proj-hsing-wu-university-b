<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Api_teach extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('mod_teach');
        $this->load->model('start_model');
    }

//---取得清單
    function index() {
        $id = $this->input->get('id');
        $json_arr = array();
        if ($id == "") {
            $json_arr['sys_code'] = '000';
            $json_arr['sys_msg'] = '參數輸入錯誤';
        } else {
            if ($this->mod_teach->get_count($id) == 0) {
                $json_arr['sys_code'] = '100';
                $json_arr['sys_msg'] = '查無資料';
            } else {
                $json_arr['sys_code'] = '200';
                $json_arr['sys_msg'] = '資料處理完成';
                $json_arr['list'] = $this->mod_teach->get_list($id);
            }
        }
        echo json_encode($json_arr);
    }

    function teaching_view() {

        $id = $this->input->get('id');
        $json_arr = array();
        if ($id == "") {
            $json_arr['sys_code'] = '000';
            $json_arr['sys_msg'] = '參數輸入錯誤';
        } else {
            $info = $this->mod_teach->teach_info($id);
            $this->load->view('webview', array('info' => $info['info_html']));
        }
//		echo json_encode($json_arr);
    }

}

?>
