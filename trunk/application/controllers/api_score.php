<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Api_score extends CI_Controller {

    function __construct() {
        parent::__construct();
        //$this->load->model('start_model');
        $this->load->model('mod_score');
    }

    function check_score() {
        $json_arr = array();
        $member_id = $this->input->get('member_id');
        if ($member_id == "") {
            $json_arr['sys_code'] = '000';
            $json_arr['sys_msg'] = '參數不足';
        } else {
            $score = $this->mod_score->member_score($member_id);
            if ($score == false) {
                $json_arr['sys_code'] = '500';
                $json_arr['sys_msg'] = 'Error';
            }else{
                $json_arr['sys_code'] = '200';
                $json_arr['sys_msg'] = 'Success';
                $json_arr['score'] = $score;
            }
        }
        echo json_encode($json_arr);
    }

}