<?php

class Coupon extends CI_Controller {

    /**
     * 校園行動優惠
     */
    function __construct() {
        parent::__construct();
        $this->load->library('myhtml');
        $this->load->library('lib_jampush');
        $this->load->library('curl');
        $this->load->model('mod_coupon');
    }

    function index() {
        $this->load->library('lib_js');
        $table = '';
        $table_tr = '';
        $coupon_list = $this->mod_coupon->all_list();
        if ($coupon_list == false) {
            $table_tr = '';
        } else {
            foreach ($coupon_list as $coupon) {
                $table_tr .= '<tr>
                            <td>' . $coupon['sn'] . '</td>
                            <td>' . $coupon['subject'] . '</td>
                            <td>' . date("Y-m-d H:i:s", $coupon['datetime']) . '</td>
                             <td>
                                 ' . $this->lib_js->list_remove_icon($coupon['sn']) . '
                             </td>
                        </tr>';
            }
            $js = $this->lib_js->list_remove_js(base_url() . 'api_coupon/remove'); //刪除 js
        }
        $table = '<table class="table table-striped">
							<tr><th>SN</th><th>標題</th><th>發布時間</th><th></th></tr>
							' . $table_tr . '
						</table>';
        $nav = '<ul class="nav nav-pills">
						<a href="' . base_url() . 'coupon/add" class="btn btn-small"><li class="icon-plus"></li> 新增訊息</a>
                </ul>';
        $data = array(
            'title' => '行動優惠清單',
            'nav' => $nav,
            'table' => $table,
            'js' => $js,
            'msg' => ''
        );
        $this->load->view('list', $data);
    }

    function add() {
        $s1_html = '<ul class="nav nav-tabs" id="myTab">
            <li class="active"><a href="#0">標題</a></li>
            <li><a href="#1">標題搭配文字</a></li>
            <li><a href="#2">圖片</a></li>
            <li><a href="#3">影片</a></li>
            <li><a href="#4">音樂</a></li>
            <li><a href="#5">地圖</a></li>
            <li><a href="#6">網址</a></li>
            <li><a href="#7">行事曆</a></li>
        </ul>

        <div class="tab-content">
            <div class="tab-pane active" id="0">' . $this->lib_jampush->type_0() . '</div>
            <div class="tab-pane" id="1"></div>
            <div class="tab-pane" id="2"></div>
            <div class="tab-pane" id="3"></div>
            <div class="tab-pane" id="4"></div>
            <div class="tab-pane" id="5"></div>
            <div class="tab-pane" id="6"></div>
            <div class="tab-pane" id="7"></div>
        </div>';

        $form_html = ' <fieldset>
                                        <legend>優惠類型</legend>' . $this->myhtml->form_select('選擇類型', 'category_id', '', $this->mod_coupon->category_list()) . '
                                      </fieldset>
                                      <fieldset>
                                        <legend>發送內容</legend>' . $s1_html . '
                                      </fieldset>
                                      ' . $this->myhtml->form_button('送出') . '
                                ';

        $js = '
         $("#myTab a").click(function(e) {
                e.preventDefault();
                $(this).tab("show");
                var id = $(this).attr("href");
                var id = id.slice(1,2);
                var api = "' . base_url() . 'coupon/send_form/" + id;
                $(".tab-pane").html("");
                $("#"+id).load(api);
                console.log(this);
            })    
        ';
        
          
        $data = array(
            'title' => '新增訊息',
            'nav' => '',
            'msg' => '',
            'form_html' => $form_html,
            'enctype' => 'multipart/form-data',
            'form_action' => base_url() . 'coupon/send/',
            'js' => $js
        );
        $this->load->view('form', $data);
    }

    //---表單內容
    function send_form($type) {
        switch ($type) {
            case '0':
                echo $this->lib_jampush->type_0();
                break;
            case '1':
                echo $this->lib_jampush->type_1();
                break;
            case '2':
                echo $this->lib_jampush->type_2();
                break;
            case '3':
                echo $this->lib_jampush->type_3();
                break;
            case '4':
                echo $this->lib_jampush->type_4();
                break;
            case '5':
                echo $this->lib_jampush->type_5();
                break;
            case '6':
                echo $this->lib_jampush->type_6();
                break;
            case '7':
                echo $this->lib_jampush->type_7();
                break;
        }
    }

    //傳送
    function send() {
        $save = false;
        $type = $this->input->post('type');
        $subject = $this->input->post('subject');
        $category_id = $this->input->post('category_id');
        $this->load->model('mod_jampush');
        $dxid = $this->mod_jampush->get_dxid();
       
        
        //$dxid[] = '48439592-ed2d-11e2-92bf-f23c917018f2'; //luke
        $text = $this->input->post('text');
        $url = $this->input->post('url');
        $addr = $this->input->post('addr');

        $sn = time();

        if ($this->lib_jampush->push_title($subject, $dxid, $sn)) {
            switch ($type) {
                case '0':
                    $data = array(
                        'sn' => $sn,
                        'subject' => $subject,
                        'type' => $type,
                        'datetime' => time(),
                        'category_id' => $category_id,
                    );
                    if ($this->mod_coupon->insert($data)) {
                        $save = true;
                    }
                    break;

                case '1':
                    $data = array(
                        'sn' => $sn,
                        'subject' => $subject,
                        'type' => $type,
                        'text' => $text,
                        'datetime' => time(),
                        'category_id' => $category_id,
                    );
                    if ($this->mod_coupon->insert($data)) {
                        $save = true;
                    }
                    break;

                case '2':
                    $link = base_url().'push_file/images/coupon-' . $sn . '.png';
                    if (copy($_FILES['images']['tmp_name'], '/var/www/html/trunk/push_file/images/coupon-' . $sn . '.png')) {
                        $data = array(
                            'sn' => $sn,
                            'subject' => $subject,
                            'type' => $type,
                            'link' => $link,
                            'text' => $text,
                            'datetime' => time(),
                            'category_id' => $category_id,
                        );
                        if ($this->mod_coupon->insert($data)) {
                            $save = true;
                        }
                    }
                    break;
                case '3':
                    $data = array(
                        'sn' => $sn,
                        'subject' => $subject,
                        'type' => $type,
                        'link' => $url,
                        'datetime' => time(),
                        'category_id' => $category_id,
                    );
                    if ($this->mod_coupon->insert($data)) {
                        $save = true;
                    }
                    break;
                case '4':
                    $data = array(
                        'sn' => $sn,
                        'subject' => $subject,
                        'type' => $type,
                        'link' => $url,
                        'datetime' => time(),
                        'category_id' => $category_id,
                    );
                    if ($this->mod_coupon->insert($data)) {
                        $save = true;
                    }
                    break;
                case '5':
                    $link = 'https://www.google.com/maps/preview#!q=' . $addr;
                    $data = array(
                        'sn' => $sn,
                        'subject' => $subject,
                        'type' => $type,
                        'link' => $link,
                        'datetime' => time(),
                        'category_id' => $category_id,
                    );
                    if ($this->mod_coupon->insert($data)) {
                        $save = true;
                    }
                    break;
                case '6':
                    $data = array(
                        'sn' => $sn,
                        'subject' => $subject,
                        'type' => $type,
                        'link' => $url,
                        'datetime' => time(),
                        'category_id' => $category_id,
                    );
                    if ($this->mod_coupon->insert($data)) {
                        $save = true;
                    }
                    break;

                case '7':
                    $calendar_start = $this->input->post('calendar_start');
                    $calendar_end = $this->input->post('calendar_end');
                    $this->load->library('lib_value');
                    $data = array(
                        'sn' => $sn,
                        'subject' => $subject,
                        'type' => $type,
                        'text' => $text,
                        'datetime' => time(),
                        'calendar_start' => $this->lib_value->date_to_timesmap($calendar_start),
                        'calendar_end' => $this->lib_value->date_to_timesmap($calendar_end),
                        'calendar_end' => $this->lib_value->date_to_timesmap($calendar_end),
                        'category_id' => $category_id,
                    );
                    if ($this->mod_coupon->save($data)) {
                        $save = true;
                    }
                    break;
            }
            if ($save == true) {
                echo $this->myhtml->alert_togo('發送成功', base_url() . 'coupon');
            } else {
                echo $this->myhtml->alert_togo('資料處理失敗', base_url() . 'coupon');
            }
        } else {
            echo $this->myhtml->alert_togo('發送失敗', base_url() . 'coupon');
        }
    }

}

?>