<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Api_location extends CI_Controller{
	function __construct()
	{
		parent::__construct();
		$this->load->model('mod_location');
	}
//---
	function index(){
		$location = $this->input->get('location');
		$member_id = $this->input->get('member_id');
		$phone = $this->input->get('phone');
		if($location == "" OR $member_id == "" OR $phone == ""){
			$json_arr['sys_code'] = '000';
			$json_arr['sys_msg'] = '資料不齊全';
		}elseif($this->mod_location->chk_phone($phone) == true){
			$i = array(
				'location'=>$location,
				'time'=>time(),
				'member_id'=>$member_id,
				'phone'=>$phone,
			);
			$this->mod_location->insert_log($i);
			$json_arr['sys_code'] = '200';
			$json_arr['sys_msg'] = '處理完成';
		}else{
			$json_arr['sys_code'] = '100';
			$json_arr['sys_msg'] = '查無電話資料';
		}
		
		echo json_encode($json_arr);
	}

}
?>
