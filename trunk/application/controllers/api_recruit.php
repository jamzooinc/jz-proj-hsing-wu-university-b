<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Api_recruit extends CI_Controller{
	function __construct()
	{
		  parent::__construct();
		  $this->load->model('mod_recruit');
		  $this->load->model('mod_chat');
		  $this->load->library('lib_jampush');
	}
//---取得清單--------
	function get_list(){
		$class_id = $this->input->get('class_id');
		if($class_id == ""){
			$json_arr['sys_code'] = '000';
			$json_arr['sys_msg'] = '資料不足';
		}else{
			$list = $this->mod_recruit->get_list($class_id);
			if($list == false){
				$json_arr['sys_code'] = '100';
				$json_arr['sys_msg'] = '查無資料';
			}else{
				$json_arr['sys_code'] = '200';
				$json_arr['sys_msg'] = '處理成功';
				$json_arr['list'] = $list;
			}
		}
		echo json_encode($json_arr);
	}
//-----取得招生內容
	function get_info(){
		$sn = $this->input->get('sn');
		if($sn == ""){
			$json_arr['sys_code'] = '000';
			$json_arr['sys_msg'] = '資料不足';
		}else{
			$info = $this->mod_recruit->get_info($sn);
			if($info == false){
				$json_arr['sys_code'] = '100';
				$json_arr['sys_msg'] = '查無資料';
			}else{
				$json_arr['sys_code'] = '200';
				$json_arr['sys_msg'] = '處理成功';
				$json_arr['info'] = $info;
			}
		}
		echo json_encode($json_arr);
	}
//-------發射
	function send_push(){
			$sn = $this->input->get('sn');
			$member  = $this->mod_chat->jampush_target('all_guest','hwu');
			//print_r($member);
			$info = $this->mod_recruit->get_info($sn);
			if(count($member) < 1 OR $info["sn"] == ""){
						$json_arr['sys_code'] = '100';
						$json_arr['sys_msg'] = '查無資料';
			}else{
				$custom = 'hwu,'.$info["sn"].',NULL';
				$pid = $this->lib_jampush->push_title($info['title'],$member,$custom);
				
				if($pid == false){
						$json_arr['sys_code'] = '500';
						$json_arr['sys_msg'] = '訊息發送失敗';
				}else{
						$json_arr['sys_code'] = '200';
						$json_arr['sys_msg'] = '系統發送成功';
				}
			}
			echo json_encode($json_arr);
	}
  
  function remove(){
      $sn = $this->input->get('key');
      if($this->mod_recruit->remove($sn)){
          $json_arr['sys_code'] = '200';
      }else{
          $json_arr['sys_code'] = '500';
      }
      echo json_encode($json_arr);
  }
}
?>
