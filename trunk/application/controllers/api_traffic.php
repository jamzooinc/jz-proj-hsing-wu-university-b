<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Api_traffic extends CI_Controller{
	function __construct()
	{
		parent::__construct();
		$this->load->model('mod_traffic');
	}
//---取得清單
	function index(){
		$json_arr['traffic'] = $this->mod_traffic->get_info();
		echo json_encode($json_arr);
	}

}
?>
