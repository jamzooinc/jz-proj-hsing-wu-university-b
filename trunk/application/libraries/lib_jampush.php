<?php

class Lib_jampush {

    //subject  -- content
    //dxid     -- dxid array contains a lot of dxid.
    //custom   -- message_id
    function push_title($Subject, $dxid, $custom = "") {
        $RestCode = jampush_resCode;
        $AppKey = jampush_AppKey;


        $new_dxid = array();

        $str = array();
        foreach ($dxid as $str) {
            $new_dxid[] = $str;
        }

        $DXID = json_encode($new_dxid);

        $parameterArray["ctrl"] = "sendpush_alert";
        $parameterArray["rest_code"] = "$RestCode";
        $parameterArray["appkey"] = $AppKey;
        $parameterArray["target_type"] = "device_id";
        $parameterArray["device_ids"] = $DXID;
        $parameterArray["within"] = "no";
        $parameterArray["schedule_type"] = "now";
        $parameterArray["subject"] = trim($Subject, "\t");
        $parameterArray["button"] = '開啟';
        $parameterArray["custom"] = $custom;


        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://api.jampush.com.tw/rest-api.php");
        curl_setopt($ch, CURLOPT_POST, true); // 啟用POST
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($parameterArray));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); //只處理不顯示
        $r = curl_exec($ch);
        $json = json_decode($r);
        curl_close($ch);

        if ($json->error_code == '200') {
            return $json->pid;
        } else {
            print_r($json);
            return false;
        }
    }

    //----layout---------
    /**
     * 
     * @return string
     */
    function type_0() {
        $html = '';
        $html = '
                    <input type="hidden" name="type" value="0">
                    <div class="control-group">
				<label class="control-label" >請輸入標題</label>
				<div class="controls">
				  <input type="text" id="subject" name="subject" value=""  class="input-xlarge">
				</div>
			  </div>';
        return $html;
    }

    function type_1() {
        $html = '';
        $html = '
                    <input type="hidden" name="type" value="1">
                    <div class="control-group">
				<label class="control-label" >請輸入標題</label>
				<div class="controls">
				  <input type="text" id="subject" name="subject" value=""  class="input-xlarge">
				</div>
			  </div>
         <div class="control-group">
				<label class="control-label" >請輸入內文</label>
				<div class="controls">
				  <textarea name="text" rows="5" style="width:450px;"></textarea>
				</div>
			  </div>
        ';
        return $html;
    }

    function type_2() {
        $html = '';
        $html = '
                    <input type="hidden" name="type" value="2">
                    <div class="control-group">
				<label class="control-label" >請輸入標題</label>
				<div class="controls">
				  <input type="text" id="subject" name="subject" value=""  class="input-xlarge">
				</div>
			  </div>
        <div class="control-group">
				<label class="control-label" >上傳圖片</label>
				<div class="controls">
				  <input type="file" name="images"/>
				</div>
			  </div>
         <div class="control-group">
				<label class="control-label" >請輸入內文</label>
				<div class="controls">
				  <textarea name="text" rows="5" style="width:450px;"></textarea>
				</div>
			  </div>
        ';
        return $html;
    }

    function type_3() {
        $html = '';
        $html = '
                    <input type="hidden" name="type" value="3">
                    <div class="control-group">
				<label class="control-label" >請輸入標題</label>
				<div class="controls">
				  <input type="text" id="subject" name="subject" value=""  class="input-xlarge">
				</div>
			  </div>
         <div class="control-group">
				<label class="control-label" >輸入 yuotube 網址</label>
				<div class="controls">
                          <input type="text" id="url" name="url" value=""  class="input-xlarge">
				</div>
			  </div>
        ';
        return $html;
    }

    function type_4() {
        $html = '';
        $html = '
                    <input type="hidden" name="type" value="4">
                    <div class="control-group">
				<label class="control-label" >請輸入標題</label>
				<div class="controls">
				  <input type="text" id="subject" name="subject" value=""  class="input-xlarge">
				</div>
			  </div>
         <div class="control-group">
				<label class="control-label" >輸入音樂線上路徑</label>
				<div class="controls">
                          <input type="text" id="url" name="url" value=""  class="input-xlarge">
				</div>
			  </div>
        ';
        return $html;
    }

    function type_5() {
        $html = '';
        $html = '
                    <input type="hidden" name="type" value="5">
                    <div class="control-group">
				<label class="control-label" >請輸入標題</label>
				<div class="controls">
				  <input type="text" id="subject" name="subject" value=""  class="input-xlarge">
				</div>
			  </div>
                    <div class="control-group">
				<label class="control-label" >請輸入內文</label>
				<div class="controls">
				  <textarea name="text" rows="5" style="width:450px;"></textarea>
				</div>
			  </div>
         <div class="control-group">
				<label class="control-label" >輸入地址</label>
				<div class="controls">
                          <input type="text" id="url" name="addr" value=""  class="input-xlarge">
				</div>
			  </div>
        ';
        return $html;
    }

    function type_6() {
        $html = '';
        $html = '
                    <input type="hidden" name="type" value="6">
                    <div class="control-group">
				<label class="control-label" >請輸入標題</label>
				<div class="controls">
				  <input type="text" id="subject" name="subject" value=""  class="input-xlarge">
				</div>
			  </div>
         <div class="control-group">
				<label class="control-label" >輸入網址</label>
				<div class="controls">
                          <input type="text" id="url" name="url" value=""  class="input-xlarge">
				</div>
			  </div>
                          
        ';
        return $html;
    }

    function type_7() {
        $html = '';
        $html = '
                    <input type="hidden" name="type" value="7">
                    <div class="control-group">
				<label class="control-label" >請輸入標題</label>
				<div class="controls">
				  <input type="text" id="subject" name="subject" value=""  class="input-xlarge">
				</div>
			  </div>
                <div class="control-group">
                <label class="control-label" >請輸入內文</label>
                <div class="controls">
                  <textarea name="text" rows="5" style="width:450px;"></textarea>
                </div>
                
                </div>
                <div class="control-group">
                <label class="control-label" >開始日期</label>
                <div class="controls">
                  <input class="datepicker" name="calendar_start" value=""/>
                </div>
        </div>
        
                <div class="control-group">
                <label class="control-label" >結束日期</label>
                <div class="controls">
                  <input class="datepicker" name="calendar_end" value=""/>
                </div>
                </div>
                
<div class="control-group">
				<label class="control-label" >重複週期</label>
				<div class="controls">
				  <select name="calendar_cycle" id="calendar_cycle"  class="span2" style="height:32px;">
					<option value=" "> 不重複 </option>
					<option value="day">每日</option>
					<option value="week">每周</option>
					<option value="month">每月</option>
				  </select>
				  
				</div>
			  </div>
                
<script>
  $(function() {
	$(".datepicker").datepicker();
    $(".datepicker").datepicker(
		"option", "dateFormat","yy-mm-dd"
	);
  });
 </script>

        ';
        return $html;
    }

}

?>
