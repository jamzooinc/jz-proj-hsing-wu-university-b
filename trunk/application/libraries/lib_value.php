<?php
class Lib_value {
		//二維陣列堆疊
		function array_push_2($array1,$array2){
			foreach($array2 as $array_new){
					$array1[] = $array_new;
			}
			return $array1;
		}
		//陣列去除空白
		function array_del_null($array,$key){
				$array_new = array();
				foreach($array as $a){
					if($a["$key"]  != ""){
							$array_new[] = $a;
					}
				}
				return $array_new;
		}
		//陣列去除重複
		function array_del_repeat($array,$key){
				$new_array = array();
				foreach($array as $new){
						if($this->array_in_array($new["$key"],$key,$new_array) == false){
							$new_array[] = $new;
						}
				}
				
				return $new_array;
		}	
		//二維陣列驗證存在
		function array_in_array($value,$key,$array){
			$result = false;
			foreach($array as $n){
				if($value === $n["$key"]){
					$result = true;
				}
			}
			return $result;
		}
    
        //日期時間轉時間戳
    function date_to_timesmap($datetime){
        
        $a = explode('-', $datetime);
//        print_r($a);
//        $b = explode(' ', $datetime);
//        print_r($b);
//        $c = explode(':', $b[1]);
       // print_r($c);
        $y = $a[0];
        $m = $a[1];
        $d = $a[2];
//        $h = $c[0];
//        $i = $c[1];
//        $s = $c[2];
        return mktime(0,0,0,$m,$d,$y);
    }
    
    
    
/**
 * 經緯度換算距離
 * @param type $d
 * @return type
 */    

function GetDistance($lat1, $lng1, $lat2, $lng2)  
{  
    $EARTH_RADIUS = 6378.137;  
    $radLat1 = $lat1*3.1415926535898 / 180.0;  
    //echo $radLat1;  
   $radLat2 = $lat2*3.1415926535898 / 180.0;  
   $a = $radLat1 - $radLat2;  
   $b = ($lng1*3.1415926535898 / 180.0) -($lng2*3.1415926535898 / 180.0);  
   $s = 2 * asin(sqrt(pow(sin($a/2),2) +  
    cos($radLat1)*cos($radLat2)*pow(sin($b/2),2)));  
   $s = $s *$EARTH_RADIUS;  
   $s = round($s * 10000) / 10000;  
   $s = $s * 1000;
   return $s;  
} 
		
}


?>
