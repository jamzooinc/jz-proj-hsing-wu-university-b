<?php
class Myhtml {
//=== Form ==================================
//----text_input------------------------
function form_hidden($name,$value){
	$new = '<input type="hidden" id="'.$name.'" name="'.$name.'" value="'.$value.'" />';
	return $new;
}

function form_text_input($text,$name,$value,$type="text",$size="input-medium"){
		if($type == 'readonly'){
			$readonly = 'readonly="readonly" ';
			$type = 'text';
		}else{
			$readonly = '';
		}
		$new = '<div class="control-group">
				<label class="control-label" >'.$text.'</label>
				<div class="controls">
				  <input type="'.$type.'" id="'.$name.'" name="'.$name.'" value="'.$value.'" '.$readonly.'  class="'.$size.'">
				</div>
			  </div>';
		return $new;
}
function form_textarea($text,$name,$value,$rows = 6){
		$new = '<div class="control-group">
				<label class="control-label" >'.$text.'</label>
				<div class="controls">
					<textarea name="'.$name.'" rows="'.$rows.'"  style="width:80%;">'.$value.'</textarea>
				</div>
			  </div>';
		return $new;
}
function form_date($text,$name,$value=""){
	$new = '<div class="control-group">
				<label class="control-label" >'.$text.'</label>
				<div class="controls">
					<input class="datepicker" name="'.$name.'" value="'.$value.'" />
				</div>
			  </div>';
		return $new;
}



//----select------------------------
function form_select($text,$name,$value='',$arr,$extra=''){
		$opt = '';
		
		foreach($arr as $a){
			if($value == $a['value']){
				$opt .= '<option value="'.$a['value'].' "selected="selected" >'.$a['text'].'</option>';
			}else{
				$opt .= '<option value="'.$a['value'].'" >'.$a['text'].'</option>';
			}
			
		}
		$new = '<div class="control-group">
				<label class="control-label" >'.$text.'</label>
				<div class="controls">
				  <select name="'.$name.'" id="'.$name.'"  class="span2" style="height:32px;">'.$opt.'</select>
				  '.$extra.'
				</div>
			  </div>';
		return $new;
	}
function form_select_join2($id1,$id2,$json){
	$script = '
	<script>
		$(document).ready(function(){
			tri($(\'#'.$id1.'\'),$(\'#'.$id2.'\'),"'.$json.'")
		});
	</script>
	';
	return $script;
}	
//-----button---------------------
function form_button($text,$type="submit"){
		$new = '<div class="control-group">
					<div class="controls">
					  <button type="'.$type.'" class="btn">'.$text.'</button>
					</div>
				  </div>';
		return $new;
	}
function form_button_lib($text,$type="submit"){
		$new = '<div class="control-group">
					<div class="controls">
					  <button type="'.$type.'" class="btn">'.$text.'</button>
                                          <span style="font-size:20px;">(連續發送時，請重新選擇發送對象)
					</div>
				  </div>';
		return $new;
	}
  //ckeditor
function ckeditor($name,$value="")
	{
		$text = '<textarea class="ckeditor" id="'.$name.'" name="'.$name.'">'.$value.'</textarea>';
		return $text;
	}
//===== message ========================================
function msg($type="info",$title="",$info=""){
	//type = error,block,success,info
	$html = '';
	$html = '<div class="alert alert-'.$type.'"><h4>'.$title.'</h4>'.$info.'</div>';
	return $html;
}
//=== JavaScript ======================================
//----彈出畫面--------
	function modal($title,$info,$id){
		$html = '<div id="'.$id.'" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			  <div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				<h3 id="myModalLabel">'.$title.'</h3>
			  </div>
			  <div class="modal-body">
				<p>'.$info.'</p>
			  </div>
			  <div class="modal-footer">
				<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
			  </div>
			</div>';
		return $html;
	}
	// Alert 後轉到下一頁
	function alert_togo($text,$href){
		$html = '<script>
						alert("'.$text.'");
						window.location.href="'.$href.'";
				</script>';
		return $html;
	}
//=====String========================================
//---裁切文字----
	function crop_string($str,$len,$over = '...'){
		if(mb_strlen($str) > $len){
			$str = mb_substr($str,0,$len,'utf-8').$over;
		}
		return $str;
	}
}
//---取得副檔名
	function get_subname($filename){
		$main = substr($filename,0,strrpos($filename,'.'));
		//取得副檔名，回傳值為 php (沒有 . 喔!)
		$extend = array_pop(explode('.',$filename));
		//若為避免在不同作業系統下出問題請自行使用轉小寫函式
		$extend = strtolower($extend);
		return $extend;
	}


?>
