<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of lib_excel
 *
 * @author james
 */
class lib_excel {

    function imp($imp) {
        $objPHPExcel = new PHPExcel();
        $objPHPExcel = IOFactory::load($imp);
        echo "ok!";
        $sheet = $objPHPExcel->getActiveSheet();
        $objPHPExcel->setActiveSheetIndex(0);


        $highestRow = $sheet->getHighestRow(); // 取得總行數
        $highestColumn = $sheet->getHighestColumn(); // 取得總列數
        echo "ok!";
        for ($j = 2; $j <= $highestRow; $j++) {
            for ($k = 'A'; $k <= $highestColumn; $k++) {
                //讀取單元格
                $c[$j][$k] = (string) $objPHPExcel->getActiveSheet()->getCell("$k$j")->getValue();
            }
        }
        return $c;
    }

    function exp($th_arr, $all_info) {
        //contro需要自己載入library
        //$this->load->library('PHPExcel');
        //        $this->load->library('PHPExcel/IOFactory');
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->setActiveSheetIndex(0);
        $col = 0;
        foreach ($th_arr as $field) {
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, 1, $field);
            $col++;
        }
        $row = 2;
        if ($all_info != "") {
            foreach ($all_info as $data) {
                $col = 0;
                foreach ($data as $d) {
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $d);
                    $col++;
                }
                $row++;
            }
        }

        $objPHPExcel->setActiveSheetIndex(0);
        $objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5');

        // Sending headers to force the user to download the file
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="' . date('dMy') . '.xls"');
        header('Cache-Control: max-age=0');

        $objWriter->save('php://output');
    }

}

?>
