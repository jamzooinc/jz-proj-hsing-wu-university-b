<?php 
class File_ctrl{
	function import_csv($file){
		//針對post進來的file資料直接解析為 array
		$temp_file = 'temp/'.$_FILES["$file"]['name'];
		copy($_FILES["$file"]['tmp_name'],$temp_file);
		$file = fopen('temp/'.$_FILES["$file"]['name'],"r");
		$n = 0;
		while(! feof($file))
		{
			if($n != 0){
				$arr[] = $this->fgetcsv($file);
			}
		  $n++;
		}
		
		foreach($arr as $n=>$new){
			if($n != 0 && $new[0] != ''){
				$return_arr[] = $new;
			}
		}
		// print_r($return_arr);
		unlink($temp_file);
		return $return_arr;
	}
	
	
	function fgetcsv(&$handle, $length = null, $d = ",", $e = '"') {
	$d = preg_quote($d);
	$e = preg_quote($e);
	$_line = "";
	$eof=false;
	while ($eof != true) {
		$_line .= (empty ($length) ? fgets($handle) : fgets($handle, $length));
		$itemcnt = preg_match_all('/' . $e . '/', $_line, $dummy);
		//~ $itemcnt = preg_match_all('/' . $e , $_line, $dummy);
		if ($itemcnt % 2 == 0){
			$eof = true;
		}
	}
	//~ $_line = iconv("big5","utf-8//ignore",addslashes($_line));
	$_line = iconv("big5","utf-8//ignore",$_line);

	$_csv_line = preg_replace('/(?: |[ ])?$/', $d, trim($_line));
	
	$_csv_pattern = '/(' . $e . '[^' . $e . ']*(?:' . $e . $e . '[^' . $e . ']*)*' . $e . '|[^' . $d . ']*)' . $d . '/';
	preg_match_all($_csv_pattern, $_csv_line, $_csv_matches);
	$_csv_data = $_csv_matches[1];
 
	for ($_csv_i = 0; $_csv_i < count($_csv_data); $_csv_i++) {
		$_csv_data[$_csv_i] = preg_replace("/^" . $e . "(.*)" . $e . "$/s", "$1", $_csv_data[$_csv_i]);
		$_csv_data[$_csv_i] = str_replace($e . $e, $e, $_csv_data[$_csv_i]);
		
	}

	return empty ($_line) ? false : $_csv_data;
}
	
}
  
?>