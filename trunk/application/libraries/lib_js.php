<?php
class Lib_js {
//列表刪除============================================
	function list_remove_icon($key){
		return '<span class="remove" key="'.$key.'" style="cursor: pointer;"><li class="icon-remove"></li> 刪除</span>';
	}
	function list_remove_js($url){
		$js = '
			$(".remove").click(function(){
				var key = $(this).attr("key");
				url = "'.$url.'/?key="+key;
				var _sender = $(this);
				if(confirm("您確定要刪除嗎？資料將無法復原")){
					$.getJSON(url,function(result){
					if(result["sys_code"] == "200"){
						$(_sender).parents("tr").fadeOut();
					}
					});
				}
			});';
		return $js;
	}
//Modal=================================================
	function modal_button($id,$text){
		return '<a href="#'.$id.'" role="button" class="btn" data-toggle="modal">'.$text.'</a>';
	}
	
	function modal_div($id,$header,$html,$js=''){
		return '<div id="'.$id.'" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			<h2 id="myModalLabel">'.$header.'</h2>
		  </div>
		  <div class="modal-body">
			'.$html.'
		  </div>
		  <div class="modal-footer">
			<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
			<button class="btn btn-primary" type="button">Save changes</button>
			'.$js.'
		  </div>
		</div>';
	}
	
		
}


?>
