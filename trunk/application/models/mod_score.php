<?php
class Mod_score extends CI_Model {
    function member_score($member_id){
        $m = array();
        $re = '';
       foreach($this->db->get_where('stu_course',array('stu_num'=>$member_id))->result_array() as $m){
           $re .= $m['course_name'].'：' . $m['score'] .'
';
       }
       if(count($m) < 1){
           return false;
       }  else {
           return $re;
       }
        
    }
    function score_query($member_id,$q_id){
        $re = '';
        $this->db->where("member_id",$member_id);
        $this->db->where("q_id",$q_id);
        $query=$this->db->get("score_log");
        if($query->num_rows()>=1){
            return $query->row_array();
        }else{
            return false;
        }
    }
    function member_id_query($group_id,$syscode="",$lv1="",$lv2="") {
        $this->load->library("curl");
        $member = array();
        $member_idbox=array();
        $m = array();
        if(strpos($group_id, 'uest_')){
            $g = explode('_', $group_id);
            $group_id = 'guest_';
            $year = $g[1];
        }
        if($syscode=="undefined"){
            $syscode="";
        }
        if($lv1=="undefined"){
            $lv1="";
        }
        if($lv2=="undefined"){
            $lv2="";
        }
        switch ($group_id) {
            case 'all':
                $this->db->order_by("member_id","asc");
                foreach ($this->db->get('member_main')->result_array() as $m) {
                    $member[] = array(
                        'member_id' => $m['member_id'],
                    );
                }
                break;
            case 'outside':
            case 'all_guest':
                $this->db->not_like('email', 'hwu.edu');   //校外會員
                $this->db->order_by("member_id","asc");
                foreach ($this->db->get('member_main')->result_array() as $m) {
                    $member[] = array(
                        'member_id' => $m['member_id'],
                    );
                }
                break;
            case 'guest':
                $this->db->not_like('email', 'uest@');
                $this->db->order_by("member_id","asc");
                foreach ($this->db->get('member_main')->result_array() as $m) {
                    $member[] = array(
                        'member_id' => $m['member_id'],
                    );
                }
                break;
            case 'guest_':
                
                $this->db->not_like('email', 'uest@');
                $this->db->where('year', $year);      
                $this->db->order_by("member_id","asc");
                foreach ($this->db->get('member_main')->result_array() as $m) {
                    $member[] = array(
                        'member_id' => $m['member_id'],
                    );
                }
                break;
            case 'outside0':
            case 'outside1':
            case 'outside2':
            case 'outside3':
                $this->db->not_like('email', 'hwu.edu');   //校外會員
                $this->db->not_like('email', 'uest@');   //校外會員
                $this->db->order_by("member_id","asc");
                foreach ($this->db->get('member_main')->result_array() as $m) {
                    $member[] = array(
                        'member_id' => $m['member_id'],
                    );
                }
                break; //bug????????????
            case 'inside':
            case 'h050':
            case 'h051':
            case 'h029':
                $this->db->order_by("member_id","asc");
                foreach ($this->db->get('member_main')->result_array() as $m) {
                    $member[] = array(
                        'member_id' => $m['member_id'],
                    );

                }
                break;
            case 'h045':
            case 'h079':
                if($group_id=="h045"){
                    $this->db->where('sys_code','6');
                }else if($group_id=="h079"){
                    $this->db->where('lv_1_code','CNST');
                }
                $this->db->order_by("member_id","asc");
                foreach ($this->db->get('member_main')->result_array() as $m) {
                            $member[] = array(
                                'member_id' => $m['member_id'],
                            );
                }
                break;          
            case 'staff':
                $this->db->order_by("member_id","asc");
                foreach ($this->db->get('member_main')->result_array() as $m) {
                    if (strlen($m['member_id']) < 9) {
                            $member[] = array(
                                'member_id' => $m['member_id'],
                            );
                    }
                }
                break;
            case 'student':
                $this->db->order_by("member_id","asc");
                foreach ($this->db->get('member_main')->result_array() as $m) {
                    if (strlen($m['member_id']) >= 9) {
                            $member[] = array(
                                'member_id' => $m['member_id'],
                            );
                    }
                }
                break;
            default:
                $where="";
                if(strlen($group_id)==1){
                    $where="`sys_code`='".$group_id."'";
                }else if($syscode!=""){
                    $where="`sys_code`='".$syscode."'";
                }
                if($syscode!=""){
                    if($where!=""){
                        $where=$where." and ";
                    }
                    if($lv1==""){
                        $where=$where."`lv_1_code`='".$group_id."'";
                    }else{
                        $where=$where."`lv_1_code`='".$lv1."'";
                    }
                }
                if($lv1!=""){
                    if($lv2==""){
                        if($where!=""){
                            $where=$where." and ";
                        }
                        $where=$where."`lv_2_code`='".$group_id."'";
                    }
                }
                if($lv2!=""){
                    if($where!=""){
                        $where=$where." and ";
                    }
                    $where=$where."`class_code`='".$group_id."'";
                }
                if(strlen($group_id)!=1 and $syscode=="" and $lv1=="" and $lv2==""){
                    $where="`class_code`='".$group_id."' or `sys_code`='".$group_id."' or `lv_1_code`='".$group_id."' or `lv_2_code`='".$group_id."' or `member_id`='".$group_id."'";
                }
                if($where!=""){
                    $where="where LENGTH(`member_id`)>='9' and ".$where;
                }
                $query=$this->db->query("select * from `member_info` ".$where." order by `member_id` asc");
                foreach ($query->result_array() as $m) {
                            $member[] = array(
                                'member_id' => $m['member_id'],
                            );
                }
                break;
        }
        if (count($member) < 0) {
            return false;
        } else {
            return $member;
        }
    }
}
