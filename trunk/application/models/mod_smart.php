<?php

class Mod_smart extends CI_Model {

    /**
     * 給api用的清單，回傳的訊息較少
     * @return boolean|array
     */
    function smart_list($member_id="") {
        $query=$this->db->query("select * from `member_info` where `member_id`='".$member_id."'");
        if($query->num_rows()!=0){
            $query_data=$query->row_array();
        }
        $type="";
        $lv1="";
        $lv2="";
        $class="";
        $student_key="";
        $where="";
        if($member_id!=""){
            $where="where `typecode`='0'";
            $this->load->model("mod_member");
            if($this->mod_member->is_member($member_id)){
                if($this->mod_member->chk_inside($member_id)){
                    $where=$where." or `typecode`='1'";
                    if(strlen($member_id)>=9){
                        if($member_id=="graduation"){
                            $where=$where." or `vcode`='graduation'";
                        }else{
                            $student_key=1;
                            $type=$query_data['sys_code'];
                            $lv1=$query_data['lv_1_code'];
                            $lv2=$query_data['lv_2_code'];
                            $class=$query_data['class_code'];
                            $where=$where." or `typecode`='3'";
                            $where=$where." or `typecode`='4'";
                        }
                    }else{
                        $where=$where." or `typecode`='2'";
                    }
                }else{
                    $where=$where." or `typecode`='5'";
                    if(strpos($member_id, 'uest_')){
                        $g = explode('_', $member_id);
                        $member_id = 'guest_';
                        $year = $g[1];
                        $where=$where." or `vcode`='".$year."'";
                    }else{
                        $where=$where." or `typecode`='7'";
                    }
                }
            }
        }else{
            //$where="where `typecode`='0'";
        }
        $openkey=0;
        $smart_query=$this->db->query("select * from `smart` ".$where." order by `datetime` desc");
        //echo $where."<br>";
        $list = array();
        $re="";
        foreach ($smart_query->result_array() as $list) {
            if($student_key=="" or $list['typecode']!=4){
                $openkey=1;
            }else if($list['vcode']=="" and $list['syscode']==$type){
                $openkey=1;
            }else if($list['syscode']==$type and ($type==$list['syscode'] or $lv1==$list['vcode'] or $lv2==$list['vcode'] or $class==$list['vcode'])){
                $openkey=1;
            }
            if($openkey==1){
                $re[] = array(
                    'pid' => $list['pid'],
                    'subject' => $list['subject'],
                    'datetime' => $list['datetime'],
                );
            }
            $openkey=0;
        }
        if (count($list) < 1) {
            return false;
        } else {
            return $re;
        }
    }

    /**
     * 後端使用的清單陣列，取得所有欄位
     * @return boolean|array
     */
    function smart_list_backend() {
        $list = array();
        foreach ($this->db->order_by('datetime', 'desc')->get('smart')->result_array() as $list) {
            $re[] = $list;
        }
        if (count($list) < 1) {
            return false;
        } else {
            return $re;
        }
    }

    function save_smart($pid, $data) {
        if ($this->db->insert('smart', $data)) {
            $this->db->where('pid', $pid)->update('smart', array('datetime' => time()));
            return true;
        } else {
            return false;
        }
    }

    function smart_info($pid) {
        $smart = array();
        foreach ($this->db->get_where('smart', array('pid' => $pid))->result_array() as $smart) {
            $start = $smart['calendar_start'];
            if ($start == "") {
                $start = time();
            }
            $end = $smart['calendar_end'];
            if ($end == "") {
                $end = time();
            }
            $re = array(
                'pid' => $smart['pid'],
                'subject' => $smart['subject'],
                'text' => $smart['text'],
                'link' => $smart['link'],
                'type' => $smart['type'],
                'datetime' => date("Y-m-d H:i:s", $smart['datetime']),
                'calendar_start' => date("Y-m-d", $start),
                'calendar_start_str' => $smart['calendar_start'],
                'calendar_end' => date("Y-m-d", $end),
                'calendar_end_str' => $smart['calendar_end'],
                'calendar_cycle' => $smart['calendar_cycle'],
            );
        }
        if (count($smart) < 1) {
            return false;
        } else {
            return $re;
        }
    }
/**
 * 匯出紀錄
 * @param type $start
 * @param type $end
 * @return boolean|array
 */
    function smart_log($start, $end) {
        $this->db->where('datetime >=', $start . ' 00:00:00');
        $this->db->where('datetime <=', $end . ' 23:59:59');
        $smart = array();
        foreach ($this->db->get('smart_log')->result_array() as $smart) {
            $res[] = $smart;
        }
        if (count($smart) < 1) {
            return false;
        } else {
            return $res;
        }
    }

    function remove($pid) {
        if ($this->db->where('pid', $pid)->delete('smart')) {
            return true;
        } else {
            return false;
        }
    }

}
