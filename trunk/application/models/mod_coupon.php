<?php

class Mod_coupon extends CI_Model {
//---內容
    function coupon($sn){
        $c = array();
        foreach($this->db->get_where('coupon',array('sn'=>$sn))->result_array() as $c){
             $start = $c['calendar_start'];
            if($start == ""){$start = time();}
            $end = $c['calendar_end'];
             if($end == ""){$end = time();}
            $re = array(
                'sn' => $c['sn'],
                'subject' => $c['subject'],
                'text' => $c['text'],
                'link' => $c['link'],
                'type' => $c['type'],
                'datetime' => date("Y-m-d H:i:s", $c['datetime']),
                'calendar_start' => date("Y-m-d", $start),
                'calendar_start_str' => $c['calendar_start'],
                'calendar_end' => date("Y-m-d", $end),
                'calendar_end_str' => $c['calendar_end'],
                'calendar_cycle' => $c['calendar_cycle'],
            );
        }
        if(count($c) < 1){
            return false;
        }else{
            return $re;
        }
    }
//---使用說明
    function text() {
        foreach ($this->db->get('coupon_text')->result_array() as $v) { }
        return $v;
    }
    function insert($data){
        if($this->db->insert('coupon',$data)){
            return true;
        }else{
            return false;
        }
    }
//---
    function category_list() {
        $arr[] = array('text' => '美食', 'value' => '1');
        $arr[] = array('text' => '娛樂', 'value' => '2');
        $arr[] = array('text' => '其他', 'value' => '3');
        return $arr;
    }
/*
 * 
 */
    function category_info_list($category_id) {
        $list = array();
        foreach ($this->db->get_where('coupon', array('category_id' => $category_id))->result_array() as $list){
            $res[] = array(
                'sn'=>$list['sn'],
                'subject'=>$list['subject'],
                'datetime'=>date("Y-m-d H:i:s",$list['datetime']),
                'unix_datetime'=>$list['datetime'],
            );
        }
        if (count($list) < 1) {
            return false;
        } else {
            return $res;
        }
    }
    /**
     * 取得所有分類的內容清單（後台用）
     * @return boolean
     */
    function all_list(){
        $list = array();
        foreach($this->db->order_by('sn','desc')->get('coupon')->result_array() as $list){
            $re[] = $list;
        }
        if (count($list) < 1) {
            return false;
        } else {
            return $re;
        }
    }
    
    /**
     * 
     * @param type $sn
     * @return boolean
     */
    function remove($sn){
        if($this->db->where('sn',$sn)->delete('coupon')){
            return true;
        }else{
            return false;
        }
    }

}
