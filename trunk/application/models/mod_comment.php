<?php

class Mod_comment extends CI_Model {
    
    //This is for mysql
    public function get_comment($tableName){
        
        $mysql_sql = "SHOW FULL FIELDS FROM {$tableName} FROM hwu";
        $result = $this->db->query($mysql_sql);
        
        $output_arr = array();
        foreach ($result->result_array() as $row){
            $output_arr[$row['Field']] = $row['Comment'];
        }
        return $output_arr;
    }
    
    
    //This is for postgres
//    public function get_comment($tableName){
//        $postgres_sql = "select 
//      ordinal_position, 
//      column_name, 
//      data_type, 
//      is_nullable,
//      description.description as comment
//  from 
//      information_schema.columns columns
//      inner join pg_class class on (columns.table_name = class.relname)
//      inner join pg_description description on (class.oid = description.objoid)
//      inner join pg_attribute attrib on  (class.oid = attrib.attrelid and columns.column_name = attrib.attname and attrib.attnum = description.objsubid)
//  where 
//    table_name='{$tableName}' 
//  group by
//    ordinal_position, column_name, data_type, is_nullable, description.description";
//       $result =  $this->db->query($postgres_sql);
//       $mid_arr = array();
//       $out_arr = array();
//       foreach($result->result_array() as $ccc){
//           $mid_arr[] = $ccc;
//       }
//       
//       foreach($mid_arr as $ccc){
//           $out_arr[$ccc['column_name']] = $ccc['comment'];
//       }
//       
//       return $out_arr;
//    }
}