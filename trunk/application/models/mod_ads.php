<?php

class Mod_ads extends CI_Model {

    /**
     * 亂數取得單一筆資料
     * 給api前端使用
     * @return boolean
     */
    function get_once() {
        $a = array();
        foreach ($this->db->order_by('img_url', 'random')->get('ads', 1, 0)->result_array() as $a) {
            $json_arr['img_url'] = $a['img_url'];
            $json_arr['goto_url'] = $a['goto_url'];
        }
        if (count($a) < 1) {
            return false;
        } else {
            return $json_arr;
        }
    }

    /**
     * 取得全部清單
     * @return boolean|array
     */
    function get_list() {
        $ads = array();
        foreach ($this->db->get('ads')->result_array() as $ads) {
            $res[] = $ads;
        }
        if (count($ads) < 1) {
            return FALSE;
        } else {
            return $res;
        }
    }

    /**
     * 刪除單筆資料
     * @param type $ads_sn
     * @return boolean
     */
    function remove($ads_sn) {
        if ($this->db->where('ads_sn', $ads_sn)->delete('ads')) {
            return TRUE;
        } else {
            return false;
        }
    }

    function edit($ads_sn, $img_url,$goto_url) {
        $data = array(
            'img_url' => $img_url,
            'goto_url' => $goto_url,
        );
        $this->db->where('ads_sn',$ads_sn);
        if ($this->db->update('ads', $data)) {
            return true;
        } else {
            return false;
        }
    }

    function insert($ads_sn, $img_url, $goto_url) {
        $data = array(
            'ads_sn' => $ads_sn,
            'img_url' => $img_url,
            'goto_url' => $goto_url,
        );
        if ($this->db->insert('ads', $data)) {
            return true;
        } else {
            return false;
        }
    }

}
