<?php 
class Model_bus extends CI_Model {
	private $carnum;
	private $tostatusid;
	private $backstatusid;
	private $totime;
	private $backtime;

	public function config($carnumber,$tonumber,$backnumber){
			$data=$this->tostatusid=$tonumber;
			$data=$this->backstatusid=$backnumber;
			$this->carnum=$carnumber;
	}
	public function station_query(){
		if($this->carnum!="" and $this->tostatusid!="" and $this->backstatusid!=""){
			$data=$this->bus_data_output();
			if($data=="false" or @$data['TTEData']=="" or @$data['TTEData']==NULL){
				return "-1";
			}else{
				$outdata=$data['TTEData'];
				$num=count($outdata);
				for($i=0;$i<$num;$i++){
					if($outdata[$i]['@attributes']['StationId']==$this->tostatusid){
						$this->totime=$outdata[$i]['@attributes']['EstimateTime'];
					}
					if($outdata[$i]['@attributes']['StationId']==$this->backstatusid){
						$this->backtime=$outdata[$i]['@attributes']['EstimateTime'];
					}
				}
				return "1";
			}
		}else{
			return "-1";
		}
	}
	public function time($type){
		if($type=="0"){
			$time=$this->totime;
		}else{
			$time=$this->backtime;
		}
		if($time==""){
			$time=" ";
		}
		return $time;
	}
	private function bus_data_output(){
		$route=$this->carnum;
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_URL, "http://117.56.77.59/IOTAPT/BusTTEInfoXML?route={$route}&id=intosell&pwd=intosell123");
		curl_setopt($ch, CURLOPT_HEADER, FALSE);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
		$str=curl_exec($ch); 
		curl_close($ch);
		$res = @simplexml_load_string($str,NULL,LIBXML_NOCDATA);
		$res = json_decode(json_encode($res),true);
		return $res;
	}
}
?>