<?php
class Start_model extends CI_Model {
    function __construct()
    {
        // 呼叫模型(Model)的建構函數
        parent::__construct();
        $this->load->helper('file');
    }
    
    
    
    
    function gen_sys_code_msg($sys_code,$sys_msg){
        return array('sys_code'=>$sys_code,'sys_msg'=>$sys_msg);
    }
    
    
    
    //check exist.
    //input table name , record name , column data.
    function check_exist($table_name,$column_name,$column_data) {
        $exist = false;
        if ($column_data == '') {
            return false;
        }

        $test_exist = $this->db->get_where($table_name, array($column_name => $column_data));
        if ($test_exist->num_rows > 0) {
            $exist = true;
        }
        return $exist;
    }
    
    //Get speceific field of data.
    //input table name , column name , column data.
    function get_a_row($table_name,$column_name,$column_data){
        if($this->check_exist($table_name, $column_name, $column_data) == false){
            return null;
        }else{
            $result = $this->db->get_where($table_name, array($column_name => $column_data));
            foreach($result->result() as $row){
                return $row;  //return the first row.
            }
        }
    }
    
    
    
    //Get a joined table with condition array.
    function get_join_where($table_name,$second_table_name,$join_conndition,$condition){
        $this->db->select('*')->from($table_name)->join($second_table_name,$join_conndition)->where($condition);
        return $this->db->get();
    }
    
    
    function get_join($table_name,$second_table_name,$join_conndition){
        $this->db->select('*')->from($table_name)->join($second_table_name,$join_conndition);
        return $this->db->get();
    }
    
    //Modify a column of a table
    function modify_a_column($table_name,$primary_key,$id,$column_name,$column_data){
       return $this->db->update($table_name, array($column_name =>$column_data), array($primary_key => $id));
    }
    
    
    //If one of the parameter is null return false
    function check_parameter_exist(){
        $para = func_get_args();
        foreach($para as $val){
            if($val == null || $val == ''){
                return false;
            }
        }
        return true;
    }
    
    
    
    //File Action.
    function keep_log($file,$function_name,$function_input,$request){
        //2013-11-11 11:11:11 | member_data | (inputs) | 
        $data = date('Y-m-d H:i:s').'|'.$function_name.'|'.$function_input.'|'.$request. '|';
        write_file("./log/{$file}.txt",$data,'a');
    }
    
    
    function action_log($file,$function_name,$action){
        $data = date('Y-m-d H:i:s').' | '.$function_name.' | '.$action." | \n";
        write_file("./log/{$file}.txt",$data,'a');
    }
    
    
    
    
    //This is for mysql
    public function get_comment($tableName){
        
        $mysql_sql = "SHOW FULL FIELDS FROM {$tableName} FROM {$this->db->database}";
        $result = $this->db->query($mysql_sql);
        
        $output_arr = array();
        foreach ($result->result_array() as $row){
            $output_arr[$row['Field']] = $row['Comment'];
        }
        return $output_arr;
    }
    
    
    
}
?>
