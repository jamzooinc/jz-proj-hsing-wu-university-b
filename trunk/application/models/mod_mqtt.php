<?php

class Mod_mqtt extends CI_Model {
    /**
     * 利用兩個人的帳號取得channel
     * @param type $from
     * @param type $to
     * @return type
     */
    function get_channel($from,$to){
        $c = array();
        $this->db->where('from',$from);
        $this->db->where('to',$to);
        foreach($this->db->get('mqtt_channel')->result_array() as $c){}
        
        if(count($c) < 1){
            return false;
        }else{
            return $c['channel'];
        }
        
    }

    /**
     * 
     * @param type $member_id
     * @param type $dxid
     * @param type $act
     * @return boolean
     * 更改會員的 dxid 狀態
     */
    function member_dxid($member_id, $dxid, $act) {
        switch ($act) {
            case 'add':
                $this->db->where('dxid', $dxid);
                if ($this->db->count_all_results('mqtt_account_dxid') == 0) {
                    if ($this->db->insert('mqtt_account_dxid', array('member_id' => $member_id, 'dxid' => $dxid))) {
                        return TRUE;
                    } else {
                        return FALSE;
                    }
                } else {
                    if ($this->db->where('dxid', $dxid)->update('mqtt_account_dxid', array('member_id' => $member_id, 'dxid' => $dxid))) {
                        //echo 'OK';
                        return TRUE;
                    } else {
                        return FALSE;
                    }
                }
                break;
            case 'remove':
                $this->db->where('dxid', $dxid);
                $this->db->where('member_id', $member_id);
                if ($this->db->delete('mqtt_account_dxid')) {
                    return TRUE;
                } else {
                    return FALSE;
                }
                break;
        }
    }

    /**
     * 建立 MQTT 頻道
     * @param type $from
     * @param type $to
     * 
     */
    function create($from, $to) {
        $link = 'https://hwu-mq2.jampush.com.tw/v1/channel/' . mqtt_resCode . '/sub';
        $dx1 = $this->get_dxid($from);
        $dx2 = $this->get_dxid($to);
        if ($dx1 == FALSE || $dx2 == FALSE) {
            return FALSE;
        } else {
            $send['fids'][] = $from;
            $send['fids'][] = $to;
            foreach ($dx1 as $d) {
                $send['dxids'][] = $d;
            }
            foreach ($dx2 as $d) {
                $send['dxids'][] = $d;
            }
            $json_send = json_encode($send);

            $mq = json_decode($this->curl->post_json($link, $json_send));
            if ($mq->error_code == "200") {
                $this->save_channel($from, $to, $mq->channel);
                return $mq->channel;
            } else {
                return FALSE;
            }
        }
    }
/**
 * 
 * @param type $from
 * @param type $to
 * @param type $channel
 * 
 */
    function save_channel($from, $to, $channel) {
        $this->db->where('from', $from);
        $this->db->where('to', $to);
        if ($this->db->count_all_results('mqtt_channel') == 0) {
            $d1 = array(
                'from' => $from,
                'to' => $to,
                'channel' => $channel,
            );
            $this->db->insert('mqtt_channel',$d1);
        }

        $this->db->where('from', $to);
        $this->db->where('to', $from);
        if($this->db->count_all_results('mqtt_channel') == 0){
            $d2 = array(
                'from' => $to,
                'to' => $from,
                'channel' => $channel,
            );
            $this->db->insert('mqtt_channel',$d2);
        }
    }

    /**
     * 取得  MQTT 使用的 DXID
     * 
     * @param type $member_id
     * @return boolean|array
     */
    function get_dxid($member_id) {
        $member = array();
        $dxid = array();
        foreach ($this->db->get_where('mqtt_account_dxid', array('member_id' => $member_id))->result_array() as $member) {
            $dxid[] = $member['dxid'];
        }
        if (count($dxid) == 0) {
            return FALSE;
        } else {
            return $dxid;
        }
    }

    /**
     * 儲存監聽資料
     * @param type $data
     * @return boolean
     */
    function save_log($data) {
        if ($this->db->insert('mqtt_log', $data)) {
            return true;
        } else {
            return false;
        }
    }

    function history($channel, $t,$offset) {
        $h = array();
        $data = array(
            'channel' => $channel,
            't <' => $t,
        );
        foreach ($this->db->order_by('t', 'desc')->get_where('mqtt_log', $data, $offset, 0)->result_array() as $h) {
            $res[] = $h;
        }
        if (count($h) < 1) {
            return false;
        } else {
            return array_reverse($res);
        }
    }

}
