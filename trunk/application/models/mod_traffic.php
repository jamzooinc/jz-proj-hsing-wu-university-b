<?php

class Mod_traffic extends CI_Model {

    function get_info() {
        $v = array();
        foreach ($this->db->get('traffic')->result_array() as $v) { }
        if (count($v) < 1) {
            return '';
        } else {
            return $v['traffic_info'];
        }
    }

    function set($html) {
        if ($this->db->count_all_results('traffic') > 0) {
            $this->db->update('traffic', array('traffic_info' => $html));
        } else {
            $this->db->insert('traffic', array('traffic_info' => $html));
        }
    }

}
