<?php

class Mod_jampush extends CI_Model {

    function get_dxid($target = "all") {
        $this->load->model('mod_member');
        $d = array();
        switch ($target) {
            case 'all':
                foreach ($this->db->where('jampush', '1')->where('dxid !=', '')->get('member_main')->result_array() as $d) {
                    if ($this->can_send($d['dxid'])) {
                        $dxid[] = $d['dxid'];
                    }
                }
                break;
            case 'outside':
                foreach ($this->db->where('jampush', '1')->where('dxid !=', '')->get('member_main')->result_array() as $d) {
                    if (strpos($d['member_id'], '@')) {

                        if ($this->can_send($d['dxid'])) {
                            $dxid[] = $d['dxid'];
                        }
                    }
                    
                }
                break;
        }
        return $dxid;
    }
/**
 * 建立免費的push查詢表內容
 * @param type $pid
 * @param type $subject
 * @param type $channel
 * @param type $type
 * @return boolean
 */
    function save_free_main($pid, $subject, $channel, $type) {
        $data = array(
            'pid' => $pid,
            'subject' => $subject,
            'channel' => $channel,
            'type' => $type,
            'datetime' => time(),
        );
        if ($this->db->insert('free_push_main', $data)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
    
    /**
     * 設定發送紀錄內容
     * @param type $manager_id
     * @param type $to
     * @param type $subject
     * @return boolean
     */
    function save_free_log($pid,$manager_id,$to,$subject){
        $data = array(
            'pid'=>$pid,
            'manager_id'=>$manager_id,
            'to'=>$to,
            'subject'=>$subject,
            'datetime'=>date("Y-m-d H:i:s"),
        );
        if($this->db->insert('free_push_log',$data)){
            return true;
        }else{
            return false;
        }
    }

    
    function save_smart_log($manager_id,$pid,$subject,$dxids){
        foreach($dxids as $dxid){
            $data = array(
                'manager_id'=>$manager_id,
                'pid'=>$pid,
                'subject'=>$subject,
                'dxid'=>$dxid,
                'datetime'=>date("Y-m-d H:i:s"),
            );
            $this->db->insert('smart_log',$data);
        }
    }
    /**
     * 利用pid 取得訊息主要內容
     * @param type $pid
     * @return boolean
     */
    function pid_get_free($pid, $member_id) {
        $this->db->where('pid', $pid);
        $push = array();
        foreach ($this->db->get('free_push_main')->result_array() as $push) {
            
        }
        if ($push['channel'] == 'chat') {
            $np = array();
            $this->db->where('pid', $pid);
            $this->db->where('to',$member_id);
            foreach ($this->db->get('chat')->result_array() as $np) {
                
            }
            if(count($np)>1){
            $this->load->model('mod_member');
                if($np['from']=="admin" or $np['from']=="h029" or $np['from']=="h090" or $np['from']=="h034" or $np['from']=="h039" or $np['from']=="h050" or $np['from']=="h051"){
                    $snfor=$np['from'];
                }else{
                    $snfor=$this->mod_member->get_class($np['from']);
                }
                $push = array(
                    'pid' => $pid,
                    'channel' => 'chat',
                    'sn' => $np['from'],
                );
            }else{
                $push = array(
                    'pid' => $pid,
                    'channel' => 'chat',
                    'sn' => '',
                );
            }
          //  echo $this->db->last_query();
        }

        if (count($push) < 1) {
            return FALSE;
        } else {
            return $push;
        }
    }

    function can_send($dxid) {
        $member = array();
        foreach ($this->db->get_where('member_main', array('dxid' => $dxid))->result_array() as $member) {
            
        }
        if (count($member) < 1) {
            return false;
        } else {
            $start = $member['start_time'];
            $end = $member['end_time'];
            $now = date('H:i');
            if ($start == $end) {
                return false;
            } elseif ($start > $end) {    //cross day.
                if ($now < $start && $now > $end) {
                    return true;
                } else {
                    return false;
                }
            } else {   // start < end  in a day.
                if ($now > $start && $now < $end) {
                    return false;
                } else {
                    return true;
                }
            }
        }
        
    }

    function stop($member_id) {
        $this->db->where('member_id', $member_id);
        if ($this->db->update('member_main', array('jampush' => 0))) {
            return true;
        } else {
            return false;
        }
    }

    function start($member_id) {
        $this->db->where('member_id', $member_id);
        if ($this->db->update('member_main', array('jampush' => 1))) {
            return true;
        } else {
            return false;
        }
    }

    function status($member_id) {
        foreach ($this->db->get_where('member_main', array('member_id' => $member_id))->result_array() as $m) {
            
        }
        if ($m['jampush'] == 1) {
            return true;
        } else {
            return false;
        }
    }

    function free_push_log($manager_id) {
        $l = array();
        foreach ($this->db->get_where('free_push_log', array('manager_id' => $manager_id))->result_array() as $l) {
            $res[] = $l;
        }
        if (count($l) < 1) {
            return false;
        } else {
            return $res;
        }
    }

}
