<?php

class Mod_recruit extends CI_Model {

    function class_list() {
        $res['h039'] = array('class_id' => 1, 'class_name' => '教務處');
        $res['h034'] = array('class_id' => 2, 'class_name' => '夜間辦公室');
        $res['h090'] = array('class_id' => 3, 'class_name' => '研究生院');
        $res['h029'] = array('class_id' => 4, 'class_name' => '推廣中心');
        return $res;
    }

    function get_list($class_id) {
        $this->db->where('class_id', $class_id);
        $list = array();
        $this->db->order_by("sn", "desc");
        foreach ($this->db->get('recruit')->result_array() as $l) {
         $list[] = array(
                'sn' => $l['sn'],
                'title' => $l['title'],
                'datetime' => date('Y-m-d H:i:s',$l['sn']),
            );
        }
        if ($list == null) {
            return false;
        } else {
            return $list;
        }
    }

    function get_info($sn) {
        $this->db->where('sn', $sn);
        $info = array();
        foreach ($this->db->get('recruit')->result_array() as $i) {
            $info = array(
                'sn' => $i['sn'],
                'title' => $i['title'],
                'img' => $i['img'],
                'text' => $i['text'],
                'datetime' => date('Y-m-d H:i:s', str_replace('-', '', $i['sn'])),
            );
        }
        if ($info == null) {
            return false;
        } else {
            return $info;
        }
    }

    
    function remove($sn){
        if($this->db->where('sn',$sn)->delete('recruit')){
            return true;
        }else{
            return false;
        }
    }
    
    function insert($data){
        if($this->db->insert('recruit',$data)){
            return true;
        }else{
            return false;
        }
    }
    
    function edit($sn,$title,$text){
        $this->db->where('sn',$sn);
        $this->db->update('recruit',array('title'=>$title,'text'=>$text));
    }
}
