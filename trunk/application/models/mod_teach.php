<?php

class Mod_teach extends CI_Model {

    /**
     * 後台用，教學單位清單
     */
    function teach_list(){
        $t = array();
        $this->db->order_by("slot", "asc");
        foreach($this->db->get('teaching_info')->result_array() as $t){
            $res[] = $t;
        }
        return $res;
    }
    function teach_info($id){
        $t = array();
        foreach($this->db->get_where('teaching_info',array('teaching_id'=>$id))->result_array() as $t){}
        if(count($t) < 1){
            return false;
        }else{
            return $t;
        }
    }
    function edit($id,$info){
        if($this->db->where('teaching_id',$id)->update('teaching_info',array('info_html'=>$info))){
            return true;
        }else{
            return false;
        }
    }
//----查詢教學單位清單總數
    function get_count($id) {
        $this->db->where('upper_id', $id);
        return $this->db->count_all_results('teaching_info');
    }

//----查詢教學單位清單
    function get_list($id) {
        $return_arr = array();
       
        foreach ($this->db->order_by('teaching_id')->get_where('teaching_info', array('upper_id' => $id))->result_array() as $v) {
            $go_id = $v['teaching_id'];
            $return_arr[] = array(
                'teaching_id' => $v['teaching_id'],
                'teaching_name' => $v['teaching_name'],
                'info_url' => site_url("api_teach/teaching_view?id=$go_id"),
            );
        }
        return $return_arr;
    }

//-----
    function college($id) {
        // $return_arr = array();
        $return_arr = array();
        foreach ($this->db->get_where('college', array('college_id' => $id))->result_array() as $arr) {
            $return_arr = array(
                'teaching_id' => $arr['college_id'],
                'teaching_name' => $arr['college_name'],
                'info_url' => site_url("api_teach/teaching_view?id={$arr['college_id']}"),
            );
        }


        return $return_arr;
    }

}
