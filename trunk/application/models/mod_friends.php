<?php

class Mod_friends extends CI_Model {
    /**
     * 確認關係是否為同學
     * @param type $from
     * @param type $to
     * @return boolean
     * 
     */
    function chk_classmate($from,$to){
        $class_a = array();
        $class_b = array();
        foreach($this->db->where('member_id',$from)->get('member_info')->result_array() as $class_a){}
        foreach($this->db->where('member_id',$to)->get('member_info')->result_array() as $class_b){}
        if(count($class_b) == 0 || count($class_a) == 0){
            return FALSE;
        }elseif($class_a['class_code'] != "" AND $class_a['class_code'] == $class_b['class_code']){
            return TRUE;
        }  else {
            return FALSE;
        }
        
    }
    /**return:
        // 0 -> not have data
        // 1 -> is friend
        // 2 -> wait to be confirm
        //        2a   A->B  wait B confirm
        //        2b   A->B  wait A confirm
        // 3 -> block
         * 
         */
    function friends_status($from,$to){
        $f = array();
        $this->db->where('from',$from);
        $this->db->where('to',$to);
        if($this->db->count_all_results('friend_map') == 0){
            return 0;
        }else{
             $f1 = array();
             $this->db->where('from',$from);
             $this->db->where('to',$to);
             foreach($this->db->get('friend_map')->result_array() as $f1){}
             $ft_1 = $f1['status'];
             
             $f2 = array();
             $this->db->where('from',$to);
             $this->db->where('to',$from);
             foreach($this->db->get('friend_map')->result_array() as $f2){}
             $ft_2 = $f2['status'];
             if($ft_1 == 1 AND $ft_2 == 1){
                 return '1';
             }else{
                if($ft_1 == 0  || $ft_1 == 3){
                     return '2b';
                 }elseif($ft_2 == 0 || $ft_2 == 3){
                     return '2a';
                 }else{
                     return '3';
                 }
             }
        }
    }
    
    
    
    
 /**
  * 
  * @param type $from
  * @param type $to
  * 如果資料表中有資料就用更新狀態為 1 如果沒有的話就是直接新增
  */
    
    function add_friend($from,$to){
        $this->db->where('from',$from);
        $this->db->where('to',$to);
        if($this->db->count_all_results('friend_map') == 0 ){
            $this->db->insert('friend_map',array('from'=>$from,'to'=>$to,'status'=>1));
            $this->db->insert('friend_map',array('from'=>$to,'to'=>$from,'status'=>0));
        }else{
            $this->db->where('from',$from);
            $this->db->where('to',$to);
            $this->db->update('friend_map',array('status'=>1,'view'=>1));
            
            $this->db->where('from',$to);
            $this->db->where('to',$from);
            $this->db->update('friend_map',array('view'=>1));
        }
    }
    
    /**
     * 
     * @param type $from
     * @param type $to
     * 打斷 from 和 to 中間的連結，只有單向打斷，所以對方看起來還會是好友狀態
     */
    function  unlink($from,$to){
        $this->db->where('from',$from);
        $this->db->where('to',$to);
        //$this->db->delete('friend_map');
        $this->db->update('friend_map',array('status'=>3,'view'=>0));
        
    }
    
    /**
     * 
     * @param type $from
     * @param type $to
     * 設定成黑名單點一下設定，再點一下刪除
     */
    function block($from,$to,$switch){
        $member = array();
        $this->db->where('from',$from);
        $this->db->where('to',$to);
        if($switch == 'on'){
            $switch = 1;
        }  else {
            $switch = 0;
        }
        foreach($this->db->get('friend_map')->result_array() as $member){}
        if(count($member) < 1){ //如果friends_map沒資料就直接幫他建黑名單
            $this->db->insert('friend_map',array('from'=>$from,'to'=>$to,'status'=>2,'view'=>0,'block'=>1));
            $this->db->insert('friend_map',array('to'=>$from,'from'=>$to,'status'=>2,'view'=>0,'block'=>0));
            return $switch;
        }else{
             $this->db->where('from',$from);
            $this->db->where('to',$to);
            $this->db->update('friend_map',array('block'=>$switch));
             return $switch;
        }
        echo $this->db->last_query();
        
    }
    
    
}