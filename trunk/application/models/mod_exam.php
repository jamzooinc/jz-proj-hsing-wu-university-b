<?php

class Mod_exam extends CI_Model {

//---題庫分類
    function exam_type($type) {
        $type_arr[1] = '初級英文';
        $type_arr[2] = '中級英文';
        return $type_arr["$type"];
    }

//---題庫主表
    function exam_main($q_id) {
        $exam = array();
        foreach ($this->db->get_where('exam_main', array('q_id' => $q_id))->result_array() as $exam) {
            
        }
        if (count($exam) < 1) {
            $exam['q_id'] = $q_id;
            $exam['start'] = '';
            $exam['type'] = '';
            $exam['use_qty'] = '';
        }
        return $exam;
    }

//---所有題庫
    function get_all_exam() {
        $e = array();
        $result = array();
        foreach ($this->db->order_by('start', 'desc')->get('exam_main')->result_array() as $e) {
            $result[] = $e;
        }
        if (count($result) < 1) {
            return false;
        } else {
            return $result;
        }
    }

//---查詢目前題組編號
    function get_now_exam($type) {
        $this->db->where('start <', time());
        $this->db->where('type', $type);
        $this->db->order_by('q_id','desc');
        $n = array();
        foreach ($this->db->get('exam_main', 1, 0)->result_array() as $n) {
            
        }
        if ($n == null) {
            return false;
        } else {
            return $n;
        }
    }

//---get_qty($q_id){
    function get_qty($q_id) {
        $this->db->where('q_id', $q_id);
        foreach ($this->db->get('exam_main')->result_array() as $a) {
            
        }
        return $a['use_qty'];
    }

//---確認題目存在
    function chk_exam($q_id) {
        $qty = $this->db->count_all_results('exam');
        if ($qty == 0) {
            return false;
        } else {
            return true;
        }
    }

//---取回題目內容
    function get_exam($q_id) {
        $exam = array();
        $result = array();
        foreach ($this->db->get_where('exam', array('q_id' => $q_id))->result_array() as $exam) {
            $result[] = array(
                'q_title' => stripslashes($exam['q_title']),
                'a' => stripslashes($exam['a']),
                'b' => stripslashes($exam['b']),
                'c' => stripslashes($exam['c']),
                'd' => stripslashes($exam['d']),
                'correct' => stripslashes($exam['correct']),
            );
        }

        return $result;
    }

//----寫入最高分
    function insert_top($member_id, $q_id, $score, $type) {
        //確認是最高分
        foreach ($this->db->get_where('score_log', array('member_id' => $member_id, 'q_id' => $q_id))->result_array() as $ns) {
            
        }
        if (isset($ns)) {//如果分數存在
            //if ($score > $ns['score']) {
                $this->db->where(array('member_id' => $member_id, 'q_id' => $q_id, 'type' => $type));
                $this->db->update('score_log', array('score' => $score));
                return true;
            //} else {
                // echo '1';
                //return false;
            //}
        } else {//如果分數不存在就新增
            if ($this->db->insert('score_log', array('member_id' => $member_id, 'q_id' => $q_id, 'score' => $score, 'time' => time(), 'type' => $type))) {
                return true;
            } else {
                // echo '2';
                return false;
            }
        }
    }

//===========學習資源=====================================================
//----英文學習資源
    function en_teach() {
        foreach ($this->db->get('en_teach')->result_array() as $r) {
            $result[] = $r;
        }
        return $result;
    }

    function remove_teach($sn) {
        if ($this->db->where('sn', $sn)->delete('en_teach')) {
            return true;
        } else {
            return false;
        }
    }

    function ins_teach($title, $link) {
        if($title == "" || $link == "") {
           return false;
        }else{
             if ($this->db->insert('en_teach', array('title' => $title, 'link' => $link))) {
                return true;
            } else {
                return false;
            }
        }
    }

}
