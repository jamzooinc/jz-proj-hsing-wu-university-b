<?php

class Mod_calendar extends CI_Model {

    /**
     * 取得年度清單以及 pdf 內容
     * @return boolean|array
     */
    function year_list() {

        $year = array();
        $this->db->order_by('calendar_year', 'desc');
        foreach ($this->db->get('calendar')->result_array() as $year) {
            $res[] = $year;
        }

        if (count($year) < 1) {
            return false;
        } else {
            return $res;
        }
    }

    function year_file($year) {
        $file = array();
        $this->db->where('calendar_year', $year);
        foreach ($this->db->get('calendar')->result_array() as $file) {
            
        }
        if (count($file) < 1) {
            return false;
        } else {
            return $file;
        }
    }

    /**
     * 更新 行事曆 pdf
     * @param type $year
     * @param type $col
     * @param type $file_name
     */
    function set_pdf($year, $col, $file_name) {
        $this->db->where('calendar_year', $year);
        $this->db->update('calendar', array($col => $file_name));
    }

    function add_year($year) {
        $this->db->where('calendar_year', $year);
        if ($this->db->count_all_results('calendar') < 1) {
            if ($this->db->insert('calendar', array('calendar_year' => $year))) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

//============公告==========================

    /**
     * 取得符合區間的公告
     * @param type $type
     * @return boolean|array
     */
    function get_billboard_list($type = '') {
        $b = array();
        //$this->db->where('billboard_start_time <=', date("Y-m-d H:i:s"));
        //$this->db->where('billboard_end_time >=', date("Y-m-d H:i:s"));
        if ($type != '') {
            $this->db->where('type', $type);
        }
        $this->db->order_by("billboard_start_time","desc");
        foreach ($this->db->get('billboard')->result_array() as $b) {
            $res[] = $b;
        }
        if (count($b) < 1) {
            return false;
        } else {
            return $res;
        }
    }

    function add_billboard($data) {
        if ($this->db->insert('billboard', $data)) {
            return true;
        } else {
            return FALSE;
        }
    }

    function remove_billboard($key) {
        $this->db->where('billboard_id', $key);
        if ($this->db->delete('billboard')) {
            return true;
        } else {
            return false;
        }
    }

    function get_once_billboard($key) {
        $this->db->where('billboard_id', $key);
        foreach ($this->db->get('billboard')->result_array() as $b) {
            
        }
        return $b;
    }
    function edit_billboard($id,$data){
        $this->db->where('billboard_id',$id);
        if($this->db->update('billboard',$data)){
            return true;
        }else{
            return false;
        }
        
    }

    function remove_year($year) {
        $this->db->where('calendar_year', $year);
        if ($this->db->delete('calendar')) {
            return true;
        } else {
            return false;
        }
    }

}