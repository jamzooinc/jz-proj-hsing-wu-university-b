<?php

class Mod_member extends CI_Model {

//確認會員是否校內
    function chk_inside($member_id) {
        if (strpos($member_id, '@')) {
            return false;
        } else {
            return true;
        }
    }

//檢查帳號是否存在
    function is_member($member_id) {
        $qty = $this->db->where('member_id', $member_id)->count_all_results('member_main');
        if ($qty == 0) {
            return false;
        } else {
            return true;
        }
    }

//送出檢查 LDAP(未完成)
    function chk_ldap($id, $pwd) {
        
        $url = "http://120.102.129.95/auth.php?id=" . $id . ',' . $pwd;
        $code = exec("curl $url");
        $obj = json_decode($code);
        
        if ($obj->sys_code == '200') {
            return true;
        } else {
            if($id!="admin"  and $id!="1001503002" and $id!="102144027" and $id!="102144028" and $id!="077003" and $id!="h039" and $id!="h090"){
                return false;
            }else{
                return TRUE;
            }
        }
        return true;
        
    }

//檢查 DXID 存在
    function chk_dxid($dxid) {
        if ($this->db->where('dxid', $dxid)->count_all_results('member_main') > 0) {
            return true;
        } else {
            return false;
        }
    }

//清除原有DXID
    function clear_dxid($dxid) {
        foreach ($this->db->where('dxid', $dxid)->get('member_main')->result_array() as $m) {
            
        }
        $member_id = $m['member_id'];
        $this->db->where('member_id', $member_id)->update('member_main', array('dxid' => '', 'datetime' => time()));
    }

//更新DXID 和member_id 配對
    function renew_dxid($member_id, $dxid, $email = '') {
        if ($this->chk_dxid($dxid)) {
            $this->clear_dxid($dxid);
        }

        //如果會員表有值就更新沒有就新增
        if ($this->is_member($member_id)) {
            $this->db->where('member_id', $member_id)->update('member_main', array('dxid' => $dxid, 'datetime' => time(), 'email' => $email));
        } else {
            $this->db->insert('member_main', array('member_id' => $member_id, 'dxid' => $dxid, 'datetime' => time(), 'email' => $email,'year'=>date("Y")));
        }
    }

    //取得會員大頭貼
    function avatar($member_id) {
        $ava = array();
        $this->db->where('member_id', $member_id);
        $this->db->where('avatar !=', '');
        foreach ($this->db->get('member_main')->result_array() as $ava) {
            
        }
        if (count($ava) < 1) {
            return '';
        } else {
            return (string) base_url('upload/avatar/' . $ava['avatar']);
        }
    }
    
/**
 * 取得jampush專用dxid
 * 
 * @param type $member_id
 * @return boolean
 */
    function jampush_dxid($member_id) {
        $m = array();
        foreach ($this->db->get_where('member_main', array('member_id' => $member_id))->result_array() as $m) {
            
        }
        if (count($m) < 1) {
            return false;
        } else {

            if (strlen($m['dxid']) > 30) {
                return $m['dxid'];
            } else {
                return false;
            }
        }
    }

    function  jampush_status($member_id){
        $m = array();
        foreach($this->db->get_where('member_main',array('member_id'=>$member_id))->result_array() as $m){}
        if(count($m) < 1){
            return false;
        }else{
           return $m['jampush'];
        }
    }
    //check member type 
    function chk_member_type($member_id) {
        if ($this->is_member($member_id) == true) {
            if ($this->chk_inside($member_id) == true) {
                //inside
                if (strlen($member_id) >= 9) {
                    return 'student';
                } else {
                    //教職員
                    $m = array();
                    foreach ($this->db->get_where('member_info', array('member_id' => $member_id))->result_array() as $m) {
                        
                    }
                    if ($m['class_name'] == "") {
                        return 'staff';
                    } else {
                        return 'teacher';
                    }
                }
            } else {
                //outside;
                if (strpos($member_id, 'uest@')) {
                    return 'guest';
                } else {
                    return 'email_member';
                }
            }
        } else {
            return false;
        }
    }

//------取得班級
    function get_class($member_id) {;
        $this->db->where('member_id', $member_id);
        $query=$this->db->get('member_info');
        if ($query->num_rows() < 1) {
            return false;
        } else {
            $query_data=$query->row_array();
            $class_code = $query_data['class_code'];
            if ($class_code == "") {
                return false;
            } else {
                return $class_code;
            }
        }
    }

    function class_name($member_id) {
        $this->db->where('member_id', $member_id);
        foreach ($this->db->get('member_info')->result_array() as $c) {
            
        }
        if (count($c) < 1) {
            return false;
        } else {
            $class_name = $c['class_name'];
            if ($class_name == "") {
                return false;
            } else {
                return $class_name;
            }
        }
    }

    
    function get_teacher($class_code){
        $t= array();
        $teacher="";
        foreach($this->db->get_where('member_info',array('class_code'=>$class_code))->result_array() as $t){
            if (strlen($t['member_id']) <= 8) {
                $teacher = $t['member_id'];
            }
        }
        if(count($t) < 1){
            return false;
        }else{
            if($teacher != ""){
                return $teacher;
            }else{
                return false;
            }
  