<?php

class Mod_student extends CI_Model {

    function class_member($lv1) {
        if($lv1!="all"){
            $this->db->where('sys_name', $lv1);
            $this->db->or_where('lv_1_name', $lv1);
        }
        $m = array();
        foreach ($this->db->get('member_info')->result_array() as $m) {
            $member[] = $m;
        }

        if (count($m) < 1) {
            return false;
        } else {
            return $member;
        }
    }

    /**
     * 匯入前先刪除檔案
     * @param type $class
     */
    function clear($class) {
        if ($class == "cha") {
            $this->db->where('sys_name', '國際事務組');
            $this->db->or_where('lv_1_name', '國際事務組');
        } elseif ($class == "spr") {
            $this->db->where('sys_name', '進修部學務組');
            $this->db->or_where('lv_1_name', '進修部學務組');
        }



        foreach ($this->db->get('member_info')->result_array() as $m) {

            $member_id = $m['member_id'];
            $this->db->where('member_id', $member_id)->delete('member_main');
            $this->db->where('member_id', $member_id)->delete('member_info');
        }


        return true;
    }

    function insert($data) {
        $query_first=$this->db->query("select * from `member_info` where `member_id`='".$data['member_id']."'");
        if($query_first->num_rows()==0){
            $this->db->insert('member_info', $data);
            if ($this->db->where('member_id', $data['member_id'])->count_all_results('member_main') < 1) {
                $data_info = array(
                    'member_id' => $data['member_id'],
                    'email' => $data['email'],
                    'nickname' => "",
                    'jampush' => 1,
                    'start_time' => '22:00',
                    'end_time' => '09:00',
                );
                $this->db->insert('member_main', $data_info);
            }
        }else{
                $query_data=$this->db->query("update `member_info` set `email`='".$data['email']."',`class_code`='".$data['class_code']."',`lv_2_code`='".$data['lv_2_code']."',`lv_1_code`='".$data['lv_1_code']."',`sys_code`='".$data['sys_code']."',`class_name`='".$data['class_name']."',`lv_2_name`='".$data['lv_2_name']."',`lv_1_name`='".$data['lv_1_name']."',`sys_name`='".$data['sys_name']."',`name`='".$data['name']."' where `member_id`='".$data['member_id']."'");
        }
    }

}
