<?php
class Mod_house extends CI_Model {

	function search($key_arr=array(),$orderby='distance'){
		$res = array();
		foreach($this->db->order_by($orderby,'ASC')->get_where('house',$key_arr)->result_array() as $n=> $v){
			$res[] = array(
				'sn' => $v['sn'],
				'price' => $v['price'],
				'type' => $v['type'],
				'distance' => floor($v['distance']),
				'address' => $v['address'],
				'name' => $v['name'],
				'equipment' => $v['equipment'],
				'phone' => $v['phone'],
			);
		}
		// print_r($res);
		return $res;
	}
	function search_count($key_arr){
		$this->db->where($key_arr);
		return $this->db->count_all_results('house');
	}
	function info($sn){
		$n = array();
		foreach($this->db->get_where('house',array('sn'=>$sn))->result_array() as $b){
			
		}
		if($n == 0){
			return false;
		}else{
                    $n=$b;
                    $n['lat']=(string)floor($b['lat']);
                    $n['lon']=(string)floor($b['lon']);
                    return $n;
		}
	}
	
//----------
	function insert_house($data){
		if($this->db->insert('house',$data)){
			return true;
		}else{
			return false;
		}
		
	}
  
  function update($sn,$data){
      if($this->db->where('sn',$sn)->update('house',$data)){
			return true;
		}else{
			return false;
		}
  }

}
