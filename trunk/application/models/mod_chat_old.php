<?php

class Mod_chat extends CI_Model {

//========= 預設權限 channel =================================================================
    function channel($type, $member) {
        switch ($type) {
            case 'outside':

                break;
            case 'inside':

                break;
        }
        $channel_arr[] = array('key' => 'h050', 'value' => '學務處', 'unread' => $this->unread($member, 'h050'));
        $channel_arr[] = array('key' => 'h039', 'value' => '教務處', 'unread' => $this->unread($member, 'h039'));
        $channel_arr[] = array('key' => 'h034', 'value' => '夜間辦公室', 'unread' => $this->unread($member, 'h034'));
        $channel_arr[] = array('key' => 'h090', 'value' => '研究生院', 'unread' => $this->unread($member, 'h090'));
        $channel_arr[] = array('key' => 'h029', 'value' => '推廣中心', 'unread' => $this->unread($member, 'h029'));
        $channel_arr[] = array('key' => 'admin', 'value' => '學校', 'unread' => $this->unread($member, 'admin'));
        return $channel_arr;
    }

//========= 會員可以使用的 channel====================================================================
    function member_channel($member_id) {
        $member = array();
        foreach ($this->db->get_where('member_info', array('member_id' => $member_id))->result_array() as $member) {
            $channel_arr[] = array('key' => $member['class_code'], 'value' => $member['class_name'] . ' (導師)', 'unread' => $this->unread($member_id, $member['class_code']));
            $channel_arr[] = array('key' => $member['lv_2_code'], 'value' => $member['lv_2_name'], 'unread' => $this->unread($member_id, $member['lv_2_code']));
            $channel_arr[] = array('key' => $member['lv_1_code'], 'value' => $member['lv_1_name'], 'unread' => $this->unread($member_id, $member['lv_1_code']));
            //$channel_arr[]  = array('key'=>$member['sys_code'],'value'=>$member['sys_name'],'unread'=>$this->unread($member_id,$member['sys_code']));
        }
        if (count($member) < 1) {
            $channel_arr = $this->channel('outside', $member_id);
        } else {
            $channel_arr = $this->lib_value->array_push_2($channel_arr, $this->channel('inside', $member_id));
            $channel_arr = $this->lib_value->array_del_null($channel_arr, 'key');
            $channel_arr = $this->lib_value->array_del_repeat($channel_arr, 'key');
        }
        return $channel_arr;
    }

//====== 取得要發送的對象 ======================================================
    function jampush_target($group_id, $self_id = "fet") {
        $member = array();
        $m = array();
        if(strpos($group_id, 'uest_')){
            $g = explode('_', $group_id);
            $group_id = 'guest_';
            $year = $g[1];
        }
        switch ($group_id) {
            case 'all':
                $this->db->where('dxid !=', '');      //有 dxid
                foreach ($this->db->get('member_main')->result_array() as $m) {
                    if (strlen($m['dxid']) > 30) {
                        $member[] = array(
                            'member_id' => $m['member_id'],
                            'dxid' => $m['dxid'],
                            'jampush' => $m['jampush'],
                        );
                    }
                }
                break;
            case 'classmate':
                $this->load->model('mod_member');
                $classmate_arr = $this->mod_member->classmate($self_id);
                //  print_r($classmate_arr);
                foreach ($classmate_arr as $classmate) {
                    $dxid = $this->mod_member->jampush_dxid($classmate['member_id']);
                    $jampush = $this->mod_member->jampush_status($classmate['member_id']);
                    if ($dxid != false) {
                        $member[] = array(
                            'member_id' => $classmate['member_id'],
                            'dxid' => $dxid,
                            'jampush' => $jampush,
                        );
                    }
                }

                break;
            case 'outside':
            case 'all_guest':
                $this->db->not_like('email', 'hwu.edu');   //校外會員
                $this->db->where('dxid !=', '');      //有 dxid

                foreach ($this->db->get('member_main')->result_array() as $m) {
                    if (strlen($m['dxid']) > 30) {
                        $member[] = array(
                            'member_id' => $m['member_id'],
                            'dxid' => $m['dxid'],
                            'jampush' => $m['jampush'],
                        );
                    }
                }
                break;
            case 'guest':
                $this->db->not_like('email', 'uest@');
                $this->db->where('dxid !=', '');      //有 dxid
                $this->db->where('member_id !=', $self_id); //排除自己
                foreach ($this->db->get('member_main')->result_array() as $m) {
                    if (strlen($m['dxid']) > 30) {
                        $member[] = array(
                            'member_id' => $m['member_id'],
                            'dxid' => $m['dxid'],
                            'jampush' => $m['jampush'],
                        );
                    }
                }
                break;
            case 'guest_':
                
                $this->db->not_like('email', 'uest@');
                $this->db->where('dxid !=', '');      //有 dxid
                $this->db->where('year', $year);      //有 dxid
                $this->db->where('member_id !=', $self_id); //排除自己
                foreach ($this->db->get('member_main')->result_array() as $m) {
                    if (strlen($m['dxid']) > 30) {
                        $member[] = array(
                            'member_id' => $m['member_id'],
                            'dxid' => $m['dxid'],
                            'jampush' => $m['jampush'],
                        );
                    }
                }
                break;
            case 'outside0':
            case 'outside1':
            case 'outside2':
            case 'outside3':
                $this->db->not_like('email', 'hwu.edu');   //校外會員
                $this->db->not_like('email', 'uest@');   //校外會員
                $this->db->where('dxid !=', '');      //有 dxid

                $this->db->where('member_id !=', $self_id); //排除自己
                foreach ($this->db->get('member_main')->result_array() as $m) {
                    if (strlen($m['dxid']) > 30) {
                        $member[] = array(
                            'member_id' => $m['member_id'],
                            'dxid' => $m['dxid'],
                            'jampush' => $m['jampush'],
                        );
                    }
                }
            case 'inside':
            case 'h050':
            case 'h051':
            case 'h029':
                $this->db->like('email', 'hwu.edu');      //校內會員
                $this->db->where('dxid !=', '');       //有 dxid

                $this->db->where('member_id !=', $self_id); //排除自己
                foreach ($this->db->get('member_main')->result_array() as $m) {
                    if (strlen($m['dxid']) > 30) {
                        $member[] = array(
                            'member_id' => $m['member_id'],
                            'dxid' => $m['dxid'],
                            'jampush' => $m['jampush'],
                        );
                    }
                }
                break;
            case 'staff':
                $this->db->like('email', 'hwu.edu');      //校內會員
                $this->db->where('dxid !=', '');       //有 dxid
                $this->db->where('member_id !=', $self_id); //排除自己
                foreach ($this->db->get('member_main')->result_array() as $m) {
                    if (strlen($m['member_id']) < 9) {
                        if (strlen($m['dxid']) > 30) {
                            $member[] = array(
                                'member_id' => $m['member_id'],
                                'dxid' => $m['dxid'],
                                'jampush' => $m['jampush'],
                            );
                        }
                    }
                }
                break;
            case 'student':
                 $this->db->like('email', 'hwu.edu');      //校內會員
                $this->db->where('dxid !=', '');       //有 dxid
                $this->db->where('member_id !=', $self_id); //排除自己
                foreach ($this->db->get('member_main')->result_array() as $m) {
                    if (strlen($m['member_id']) >= 9) {
                        if (strlen($m['dxid']) > 30) {
                            $member[] = array(
                                'member_id' => $m['member_id'],
                                'dxid' => $m['dxid'],
                                'jampush' => $m['jampush'],
                            );
                        }
                    }
                }
                break;
            default:
                $this->db->where('class_code', $group_id);
                $this->db->or_where('lv_2_code', $group_id);
                $this->db->or_where('lv_1_code', $group_id);
                $this->db->or_where('sys_code', $group_id);
                $this->db->or_where('member_id', $group_id);
                foreach ($this->db->get('member_info')->result_array() as $m) {
                    if (isset($m['dxid'])) {
                        if (strlen($m['dxid']) > 30) {
                            $member[] = array(
                                'member_id' => $m['member_id'],
                                'dxid' => $m['dxid'],
                                'jampush' => $m['jampush'],
                            );
                        }
                    }
                }
                $this->db->where('member_id', $group_id);
                $this->db->where('dxid !=', '');       //有 dxid

                foreach ($this->db->get('member_main')->result_array() as $m) {
                    if (strlen($m['dxid']) > 30) {
                        $member[] = array(
                            'member_id' => $m['member_id'],
                            'dxid' => $m['dxid'],
                            'jampush' => $m['jampush'],
                        );
                    }
                }
        }
        if (count($member) < 0) {
            return false;
        } else {
            return $member;
        }
    }

//====== 給管理員用的選擇目標對象 ===================
    function manager_target($manager_id) {
        foreach ($this->db->get_where('manager', array('manager_id' => $manager_id))->result_array() as $manager) {
            
        }
        return $manager['chat_key'];
    }

//====== 指定對象陣列 ===============================
    function target_select($chat_key) {
        $res = array();
        if (strlen($chat_key) == 1) {
            $lv = $chat_key;
            $chat_key = 'sys_code';
        } elseif (strlen($chat_key) == 2) {
            $lv = $chat_key;
            $chat_key = 'lv_1_code';
        } elseif (strlen($chat_key) == 4) {
            $lv = $chat_key;
            $chat_key = 'lv_2_code';
        }

        switch ($chat_key) {
            case 'all':
                $res[] = array('value' => 'inside', 'text' => '校內');
                $res[] = array('value' => 'outside', 'text' => '校外');
                break;
            case 'outside':
                $res[] = array('value' => 'graduation', 'text' => '畢業生');
                $res[] = array('value' => 'guest', 'text' => '訪客會員');
                break;
            case 'graduation':
                $this->db->group_by('sys_name');
                foreach ($this->db->get('member_info')->result_array() as $m) {
                    if (strpos($m['sys_name'], '畢')) {
                        $res[] = array('value' => $m['sys_code'], 'text' => $m['sys_name']);
                    }
                }

                break;
            case 'guest':
                $this->db->group_by('year');
                foreach($this->db->get('member_main')->result_array() as $m){
                    $res[] = array('value'=>'guest_'.$m['year'],'text'=>$m['year']);
                }
                break;
            case 'inside':
                $res[] = array('value' => 'staff', 'text' => '教職員');
                $res[] = array('value' => 'student', 'text' => '學生');
                break;
            case 'student':
                $this->db->group_by('sys_code');
                foreach ($this->db->get('member_info')->result_array() as $g) {
                    $res[] = array('value' => $g['sys_code'], 'text' => $g['sys_name']);
                }
                break;
            case 'sys_code':
                $this->db->where('sys_code', $lv);
                $this->db->group_by('lv_1_code');
                foreach ($this->db->get('member_info')->result_array() as $g) {
                    $res[] = array('value' => $g['lv_1_code'], 'text' => $g['lv_1_name']);
                }
                break;
            case 'lv_1_code':
                $this->db->where('lv_1_code', $lv);
                $this->db->group_by('lv_2_code');
                foreach ($this->db->get('member_info')->result_array() as $g) {
                    $res[] = array('value' => $g['lv_2_code'], 'text' => $g['lv_2_name']);
                }
                break;
            case 'lv_2_code':
                $this->db->where('lv_2_code', $lv);
                $this->db->group_by('class_code');
                foreach ($this->db->get('member_info')->result_array() as $g) {
                    $res[] = array('value' => $g['class_code'], 'text' => $g['class_name']);
                }
                break;
        }
        return $res;
    }

//====== 寫入 chat table ============================
    function insert_chat($from, $to_arr, $title, $pid = "") {
        $m = array();
        $datetime = time();
        $readed = '0';
        foreach ($to_arr as $m) {
            $data = array(
                'from' => $from,
                'to' => $m['member_id'],
                'title' => $title,
                'pid' => $pid,
                'datetime' => $datetime,
                'readed' => $readed
            );
            $this->db->insert('chat', $data);
        }
        //登記到免費push表
        $new_data = array(
            'pid' => $pid,
            'subject' => $title,
            'channel' => 'chat',
            'type' => 0,
            'datetime' => $datetime,
        );
        $this->db->insert('free_push_main', $new_data);
    }

//==== SN 取得內容 ===================
    function get_chat($sn) {
        $c = array();
        $return = array();
        foreach ($this->db->get_where('chat', array('sn' => $sn))->result_array() as $c) {
            $return[] = array(
                'sn' => $c['sn'],
                'from' => $c['from'],
                'tp' => $c['to'],
                'pid' => $c['pid'],
                'datetime' => date("Y-m-d H:i:s", $c['datetime']),
                'readed' => $c['readed'],
            );
        }
        // print_r($c);
        if (count($c) < 1) {
            return false;
        } else {

            return $return;
        }
    }

//====取得chat 來源帳號資訊==========
    function manager($id) {
        $manager = array();
        foreach ($this->db->get_where('manager', array('manager_id' => $id))->result_array() as $manager) {
            
        }
        if (count($manager) < 1) {
            return false;
        } else {
            return $manager;
        }
    }

//====讀取 chat ===================
    function read_chat($from, $to, $limit = 10) {
        $this->load->model('mod_member');
        $teach = $this->mod_member->get_teacher($from);
        if ($teach != false) {
            $from = $teach;
        }
        $sql = "SELECT * FROM `chat` WHERE (`from`='$from' AND `to`='$to') OR (`from`='$to' AND `to`='$from')  ORDER BY `datetime` ASC ";
        $c = array();
        $chat = array();
        foreach ($this->db->query($sql)->result_array() as $c) {
            if ($c['to'] == $to) {
                $align_type = 'left';
                //設定為已讀
                $chat[] = array(
                    'sn' => $c['sn'],
                    'title' => $c['title'],
                    'align_type' => $align_type,
                    'datetime' => date("Y-m-d H:i:s", $c['datetime']),
                );
                $this->db->where('sn', $c['sn']);
                $this->db->update('chat', array('readed' => '1'));
            } else {
                $align_type = 'right';
                $chat[] = array(
                    'sn' => $c['sn'],
                    'title' => $c['title'],
                    'align_type' => $align_type,
                    'datetime' => date("Y-m-d H:i:s", $c['datetime']),
                );
            }
        }
        return $chat;
    }

    function teacher_chat($member_id) {
        $t = array();

        $this->db->where('from', $member_id);
        $this->db->group_by('pid');
        $this->db->order_by('datetime', 'asc');
        foreach ($this->db->get('chat')->result_array() as $t) {
            $res[] = array(
                'sn' => $t['sn'],
                'title' => $t['title'],
                'align_type' => 'left',
                'datetime' => date("Y-m-d H:i:s", $t['datetime']),
            );
        }
        if (count($t) < 1) {
            return false;
        } else {
            return $res;
        }
    }

//----取得未讀總數
    function unread($member_id, $from = "") {
        if ($from != '') {
            $this->db->where('from', $from);
        }
        $this->db->where('readed', 0);
        $this->db->where('to', $member_id);
        $count = $this->db->count_all_results('chat');
        return $count;
    }

////-----------------------後台----------------------------------------------------------------------------
//----對方回覆的 chat 清單
    function chat_self($manager_id) {
        $this->db->where('to', $manager_id);
        $c = array();
        $return = array();
        foreach ($this->db->order_by('datetime', 'DESC')->get_where('chat', array('to' => $manager_id))->result_array() as $c) {
            $return[] = array(
                'from' => $c['from'],
                'title' => $c['title'],
                'datetime' => $c['datetime'],
                'readed' => $c['readed'],
            );
            if ($c['readed'] == 0) {
                $this->db->where('sn', $c['sn'])->update('chat', array('readed' => 1));
            }
        }
        return $return;
    }

}
