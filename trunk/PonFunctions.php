<?php




	function pre($obj, $title = 'Debug') {
		static $call_count = 1;
		echo '<pre class="ui-widget-content" style="border: 1px solid #F00; padding: 10px; text-align: left;">';
		echo '<div style="font-weight: bold;">'.$title.'</div>';
		echo '<div style="font-weight: bold;">Call:<span style="color: #F00;">'.$call_count.'</span></div>';
		print_r($obj);
		echo '</pre>';
		$call_count++;
	}
	function Hughes($obj, $title = '') {
		$title = $title != '' ? $title : 'Hughes Debugging';
		pre($obj, $title);
	}
	function Pon($obj, $title = '') {
		$title = $title != '' ? $title : 'PonPon Debugging';
		pre($obj, $title);
	}
?>